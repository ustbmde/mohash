package edu.ustb.sei.mde.mohash.evaluation.comparators;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class GenFileEditStat {
	
	public GenFileEditStat(List<FileEditData> modelDataSumList) {
		super();
		this.modelDataSumList = modelDataSumList;
	}

	static final String[] paths = {
		"ecore_top", "ecore_large", "ecore_medium", "ecore_small",
		"uml_top", "uml_large", "uml_medium", "uml_small"
	};

	public static void main(String[] args) {
		List<FileEditData> modelDataSumList = new ArrayList<>();
		for(String path : paths) {
			System.out.println(path);
			GenFileEditStat gen = new GenFileEditStat(modelDataSumList);
			gen.generate(new File("C:\\JavaProjects\\git\\mohash\\edu.ustb.sei.mde.mohash.evaluation\\output\\"+path), path);
		}
		
		generateTotal(System.out, modelDataSumList);
	}
	
	private List<FileEditData> modelDataList = new ArrayList<>();
	private Pattern pattern1 = Pattern.compile("[a-zA-Z0-9_]+\\s\\[numOfEdits=([0-9]+),.+\\sdiffs_mohash=([0-9]+),\\sdiffs_mohashext=([0-9]+),\\stotal=([0-9]+),\\scriticalMiss=[0-9]+\\]");
	final private List<FileEditData> modelDataSumList;
	
	public void generate(File folder, String cat) {
		try {
			extractFromFolder(folder);
			
			printSummary(System.out, cat);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static public void generateTotal(PrintStream out, List<FileEditData> list) {
		list.forEach(d->{
			out.println(d.toString());
		});
	}
	
	private void printSummary(PrintStream out, String cat) {
		modelDataSumList.add(new FileEditData(cat, 
				modelDataList.stream().map(it->it.minEdits()).min((l,r)->l-r).get(), 
				modelDataList.stream().map(it->it.maxEdits()).max((l,r)->l-r).get(), 
				modelDataList.stream().collect(Collectors.averagingDouble(it->it.minDA())), 
				modelDataList.stream().collect(Collectors.averagingDouble(it->it.maxDA())), 
				modelDataList.stream().collect(Collectors.averagingDouble(it->it.midDA())), 
				modelDataList.stream().collect(Collectors.averagingDouble(it->it.minExtDA())), 
				modelDataList.stream().collect(Collectors.averagingDouble(it->it.maxExtDA())), 
				modelDataList.stream().collect(Collectors.averagingDouble(it->it.midExtDA())),
				modelDataList.stream().collect(Collectors.averagingInt(it->it.total())).intValue()
				));
		
		modelDataList.forEach(d->{
			out.println(d.toString());
		});
	}

	protected void extractFromFolder(File folder) throws IOException {
		for(File file : folder.listFiles((p,n)->{
			return n.endsWith(".log");
		})) {
			extractFromLog(file);
		}
	}
	
	protected void extractFromLog(File logFile) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(logFile));
		String line = null;
		
		String modelName = logFile.getName().substring(0, logFile.getName().indexOf('.'));
		
		int maxEdits = 0;
		int minEdits = Integer.MAX_VALUE;
		double maxDA = 0;
		double minDA = Double.MAX_VALUE;
		double maxExtDA = 0;
		double minExtDA = Double.MAX_VALUE;
		int total = 0;
		double midDA = 0;
		double midExtDA = 0;
		int count = 0;
		
		while((line=in.readLine())!=null) {
			Matcher m1 = pattern1.matcher(line);
			if(m1.matches()) {
				count ++;
				int edits = Integer.parseInt(m1.group(1));
				int diffs = Integer.parseInt(m1.group(2));
				int diffsExt = Integer.parseInt(m1.group(3));
				total = Integer.parseInt(m1.group(4));
				
				double diffRate = ((double)diffs) / total * 100.0;
				double diffExtRate = ((double)diffsExt) / total * 100.0;
				midDA += diffRate;
				midExtDA += diffExtRate;
				
				if(edits > maxEdits) maxEdits = edits;
				if(edits < minEdits) minEdits = edits;
				if(diffRate > maxDA) maxDA = diffRate;
				if(diffRate < minDA) minDA = diffRate;
				if(diffExtRate > maxExtDA) maxExtDA = diffExtRate;
				if(diffExtRate < minExtDA) minExtDA = diffExtRate;
			}
		}
		
		midDA = midDA / count;
		midExtDA = midExtDA / count;
		
		modelDataList.add(new FileEditData(modelName, minEdits, maxEdits, minDA, maxDA, midDA, minExtDA, maxExtDA, midExtDA, total));
		
		in.close();
	}
}

record FileEditData(String modelName, int minEdits, int maxEdits, double minDA, double maxDA, double midDA, double minExtDA, double maxExtDA, double midExtDA, int total) {
	public String toString() {
		return String.format("%s & %d--%d & %.2f%%--%.2f%% & %.2f%% & %.2f%%--%.2f%% & %.2f%% & %d\\\\", modelName, minEdits, maxEdits, minDA, maxDA, midDA, minExtDA, maxExtDA, midExtDA, total);
	}
}	
	