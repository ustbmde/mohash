package edu.ustb.sei.mde.mohash.evaluation.comparators

import edu.ustb.sei.mde.mohash.EObjectSimHasher
import edu.ustb.sei.mde.mohash.HashValue64
import edu.ustb.sei.mde.mohash.ObjectIndex
import edu.ustb.sei.mde.mohash.TypeMap
import edu.ustb.sei.mde.mohash.emfcompare.MatcherKind
import edu.ustb.sei.mde.mohash.factory.MoHashMatchEngineFactory
import edu.ustb.sei.mde.mohash.functions.Hash64
import edu.ustb.sei.mde.mumodel.ModelMutator
import edu.ustb.sei.mde.mumodel.RandomUtils
import edu.ustb.sei.mde.mumodel.ResourceSetExtension
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.PrintStream
import java.lang.reflect.Field
import java.util.Collections
import java.util.HashSet
import java.util.List
import java.util.Map
import java.util.Set
import java.util.regex.Pattern
import org.eclipse.emf.common.util.BasicMonitor
import org.eclipse.emf.compare.Comparison
import org.eclipse.emf.compare.match.IMatchEngine
import org.eclipse.emf.compare.match.eobject.EObjectIndex
import org.eclipse.emf.compare.match.eobject.EcoreWeightProvider
import org.eclipse.emf.compare.match.eobject.WeightProvider
import org.eclipse.emf.compare.match.eobject.internal.ProximityMatchStats
import org.eclipse.emf.compare.scope.DefaultComparisonScope
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EPackage
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.uml2.uml.UMLPackage
import edu.ustb.sei.mde.mohash.emfcompare.SimHashProximityEObjectMatcher
import edu.ustb.sei.mde.mohash.EHasherTable
import java.util.Arrays
import edu.ustb.sei.mde.mohash.emfcompare.HashBasedEObjectIndex
import edu.ustb.sei.mde.mohash.ByTypeIndex
import org.eclipse.emf.compare.match.eobject.ProximityEObjectMatcher.DistanceFunction
import edu.ustb.sei.mde.mohash.emfcompare.EditionDistanceEx

class EvaluateComparator {
	static public var debug = false // when debug is true, there is no warm up and there is only one test
	static public val matcherKind = MatcherKind.dualHash
	
	private def int checkMohashMatch(EObject object, ComparisonResult result) {
		val m1 = result.mohash.getMatch(object)
		val m2 = result.emfcomp.getMatch(object)
		
		if(m1===null && m2===null) return 0
		else if(m1===null || m2===null) return {
			result.recordDiff(object)
			1
		}
		else return if(m1.left===m2.left && m1.right===m2.right) {
			0
		} else {
			result.recordDiff(object)
			1
		}
	}
	
	private def int checkMohashExtMatch(EObject object, ComparisonResult result) {
		val m1 = result.mohashext.getMatch(object)
		val m2 = result.emfcomp.getMatch(object)
		
		if(m1===null && m2===null) return 0
		else if(m1===null || m2===null) return {
//			result.recordDiff(object)
			1
		}
		else return if(m1.left===m2.left && m1.right===m2.right) {
			0
		} else {
//			result.recordDiff(object)
			1
		}
	}
	
	static val needDump = false
	static val dumpDir = 'C:/JavaProjects/git/mohash/edu.ustb.sei.mde.mohash.evaluation/output/dump';
	static var dumpcount = 0;
	
	private def void dump(ComparisonResult result) {
		val dir = new File(dumpDir);
		dir.mkdirs
		
		val oRes = createResource(dumpDir+'/original_'+dumpcount+'.xmi')
		oRes.contents+=original.contents
		
		val mRes = createResource(dumpDir+'/mutant_'+dumpcount+'.xmi')
		mRes.contents+=mutant.contents
		
		val emfcRes = createResource(dumpDir+'/emfc_'+dumpcount+'.xmi')
		emfcRes.contents += result.emfcomp
		
		val oursRes = createResource(dumpDir+'/ours_'+dumpcount+'.xmi')
		oursRes.contents += result.mohash
		
		try {			
			oRes.save(null);
			mRes.save(null);
			emfcRes.save(null);
			oursRes.save(null);
		} catch(Exception e) {}
		
		dumpcount ++;
	}
	
	def void checkComparisonResults(Resource original, ComparisonResult result, Set<EClass> neglectedTypes) {
		var total = 0
		var diffs_mohash = 0
		var diffs_mohashext = 0
		
		val iter = original.allContents
		while (iter.hasNext) {
			val next = iter.next
			if(neglectedTypes.contains(next.eClass)===false) {
				total++
				diffs_mohash += checkMohashMatch(next, result)
				diffs_mohashext += checkMohashExtMatch(next, result)
			}
		}
		
		result.total = total
		result.diffs_mohash = diffs_mohash
		result.diffs_mohashext = diffs_mohashext
		
		if(diffs_mohash!==0 && needDump) dump(result)
	}
	
	static extension ResourceSetExtension = new ResourceSetExtension
	
	val Resource original
	val Resource mutant
	
	val IMatchEngine mohash
	val IMatchEngine mohashext
	val IMatchEngine emfcomp
	val ComparisonMetric metric
	
	new(EObject root, MoHashMatchEngineFactory factory, ComparisonMetric metric) {
		this(Collections.singletonList(root), factory, metric)
	}
	
	new(Resource model, MoHashMatchEngineFactory factory, ComparisonMetric metric) {
		this(model.contents, factory, metric)
	}
	
	new(List<EObject> roots, MoHashMatchEngineFactory factory, ComparisonMetric metric) {
		factory.reset
		factory.matcherKind = MatcherKind.simpleHash
		mohash = factory.matchEngine
		
		factory.reset
		factory.matcherKind = MatcherKind.dualHash
		mohashext = factory.matchEngine
		
		emfcomp = if(metric===ComparisonMetric.execCount) MoHashMatchEngineFactory.createEMFCompareFactoryForProfiling.matchEngine else MoHashMatchEngineFactory.createEMFCompareFactory.matchEngine
		
		original = createResource('/Mohash/pseudo_original.xmi')
		mutant = createResource('/Mohash/pseudo_mutant.xmi')
		
		original.contents += EcoreUtil.copyAll(roots)
		
		this.metric = metric
	}
	
	def long usedMemory() {
//		try { Thread.sleep(1000) } catch (Exception e) {}
		val r = Runtime.runtime;
		return r.totalMemory - r.freeMemory;
	}
	
	def ComparisonResult evaluate(List<EClass> ignoredClasses, List<EStructuralFeature> ignoredFeatures, Set<EClass> neglectedTypes, ComparisonMetric metric) {
		val mutator = new ModelMutator
		mutator.ignoredClasses = ignoredClasses
		mutator.ignoredFeatures = ignoredFeatures
		
		mutant.contents.clear
		val result = new ComparisonResult
		
		
		result.numOfEdits = mutator.mutateResource(original, mutant)
		
		val tasks = newArrayList(
			[r | evaluateEMFC(r)], [r | evaluateMohashExt(r)], [r | evaluateMohash(r)]
		)
		
		Collections.shuffle(tasks)
		
		for(t : tasks) {
			t.apply(result)
		}
		
		if(metric===ComparisonMetric.normal) {			
			checkComparisonResults(original, result, neglectedTypes)
		}
		
		result
	}
	
	protected def void evaluateMohash(ComparisonResult result) {
		EHasherTable.reset
		val mohashScope = new DefaultComparisonScope(original, mutant, null)
		
		if(metric===ComparisonMetric.normal) {
			val mohashStart = System.nanoTime
			result.mohash = mohash.match(mohashScope, new BasicMonitor)
			val mohashEnd = System.nanoTime
			result.mohashTime = (mohashEnd - mohashStart)
		} else {
			mohash.match(mohashScope, new BasicMonitor)
			result.mohashStats = mohash.distStats
		}
		
		if(debug) {
			val matcher = mohash.getFieldValue('eObjectMatcher') as SimHashProximityEObjectMatcher
			matcher.printStats(System.out)
		}
	}
	
	protected def void evaluateMohashExt(ComparisonResult result) {
		EHasherTable.reset
		val mohashScope = new DefaultComparisonScope(original, mutant, null)
		if(metric===ComparisonMetric.normal) {
			val mohashStart = System.nanoTime
			result.mohashext = mohashext.match(mohashScope, new BasicMonitor)
			val mohashEnd = System.nanoTime
			result.mohashextTime = (mohashEnd - mohashStart)
		} else {
			mohashext.match(mohashScope, new BasicMonitor)
			result.mohashExtStats = mohashext.distStats
		}
	}
	
	protected def int[] getDistStats(IMatchEngine engine) {
		val meter = engine.getFieldValue('eObjectMatcher')?.getFieldValue('index')?.getFieldValue("meter")?.getFieldValue("wrapped") as EditionDistanceEx
		
		if (meter === null) {
			println('Error in getDistStats')
			null
		} else {
			val res = meter.exeCount
			meter.resetStats
			return res
		}
	}
	
	protected def void evaluateEMFC(ComparisonResult result) {
//		try {
//			System.gc();
////			Thread.sleep(3000)
//		} catch(Exception e) {}
		val emfcScope = new DefaultComparisonScope(original, mutant, null)
		if(metric===ComparisonMetric.normal) {
			val emfcStart = System.nanoTime
			result.emfcomp = emfcomp.match(emfcScope, new BasicMonitor)
			val emfcEnd = System.nanoTime
			result.emfcompTime = (emfcEnd - emfcStart)
		} else {
			emfcomp.match(emfcScope, new BasicMonitor)
			result.emfcStats = emfcomp.distStats
		}
		
		if(debug) {
			val indexes = emfcomp.getFieldValue('eObjectMatcher')?.getFieldValue('index')?.getFieldValue('allIndexes') as Map<String, EObjectIndex>
			if(indexes===null) {
				println('Cannot print stats of EMFC')
			} else {				
				indexes.entrySet.forEach[
					val substats = it.value.getFieldValue('stats') as ProximityMatchStats
					println("["+it.key.substring(it.key.lastIndexOf(':') + 1)+"]\t"+substats)
				]
			}
		}
	}
	
	def static void main(String[] args) {
		val cat = args.get(0)
		val set = args.get(1)
		val metric = ComparisonMetric.valueOf(args.get(3));
		
		println("matcher: "+matcherKind);
		
		val modelFolder = new File('C:/JavaProjects/git/mohash/edu.ustb.sei.mde.mohash.evaluation/model/'+cat+'/data_'+set+'_20')
		
		val count = if(debug) 1 else if(metric===ComparisonMetric.execCount) 10 else 100
		val outputset = if(debug) 'debug_'+set else set
		
		if(cat=='ecore') {
			val models = modelFolder.listFiles.filter[it.name.endsWith(".ecore")].map[
				// for ecore
				it.absolutePath.loadResource.contents.get(0) as EPackage
			].toList
			evaluateEcoreModels(models, count, new File('C:/JavaProjects/git/mohash/edu.ustb.sei.mde.mohash.evaluation/output/ecore_'+outputset), metric)
		} else {
			val models = modelFolder.listFiles.filter[it.name.endsWith(".xmi")].map[
				// for uml
				it.absolutePath.loadUMLResource
			].toList
			evaluateUMLModels(models, count, new File('C:/JavaProjects/git/mohash/edu.ustb.sei.mde.mohash.evaluation/output/uml_'+outputset), metric)
		}
	}
	
	
	def static void evaluateEcoreModels(List<EPackage> models, int count, File outputFolder, ComparisonMetric metric) {
		outputFolder.mkdirs
		
		generateGlobalLog(outputFolder)
		
		models.forEach[model, i|
			System.gc
			println("Model "+(i+1))
			model.evaluateEcoreModel(count, outputFolder, metric)
		]
	}
	
	protected def static void generateGlobalLog(File outputFolder) {
		val out = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(outputFolder, 'global.txt'))))
		out.println('''global seed: «RandomUtils.seed»''');
		out.flush
		out.close
	}
	
	def static void evaluateUMLModels(List<Resource> models, int count, File outputFolder, ComparisonMetric metric) {
		outputFolder.mkdirs
		
		generateGlobalLog(outputFolder)
		
		models.forEach[model, i|
			System.gc
			println("Model "+(i+1))
			model.evaluateUMLModel(count, outputFolder, metric)
		]
	}
	
	protected def static getLogFileName(Object object) {
		val resource = if(object instanceof Resource) object as Resource else (object as EObject).eResource;
		val name = if(resource!==null) {
			val pattern = Pattern.compile('([0-9]+).+')
			val matcher = pattern.matcher(resource.URI.lastSegment)
			if(matcher.matches) {
				(if(object instanceof Resource) 'UML@' else 'Ecore@') + matcher.group(1)
			} else {
				resource.URI.trimFileExtension.lastSegment
			}
		} else {
			(object as EPackage).name
		}
		
		name
	}
	
	protected def static void evaluateEcoreModel(EPackage model, int count, File outputFolder, ComparisonMetric metric) {
		println('Begin '+model.eResource.URI.toFileString)
		val file = new File(outputFolder, model.getLogFileName+'.log')
		val out = new PrintStream(new BufferedOutputStream(new FileOutputStream(file)))
		try { evaluateEcoreModel(model, count, out, metric) } catch(Exception e) {
			e.printStackTrace
		}
		out.flush
		out.close
		println('End '+model.eResource.URI.toFileString)
	}
	
	protected def static void evaluateUMLModel(Resource umlModel, int count, File outputFolder, ComparisonMetric metric) {
//		val modelName = umlModel.URI.trimFileExtension.lastSegment
		println('Begin '+umlModel.URI.toFileString)
		val file = new File(outputFolder, umlModel.getLogFileName+'.log')
		val out = new PrintStream(new BufferedOutputStream(new FileOutputStream(file)))
		try { evaluateUMLModel(umlModel, count, out, metric) } catch(Exception e) {
			e.printStackTrace
		}
		out.flush
		out.close
		println('End '+umlModel.URI.toFileString)
	}
	
	protected def static void evaluateUMLModel(Resource umlModel, int count, PrintStream out, ComparisonMetric metric) {
		val factory = new MoHashMatchEngineFactory
		val weight = factory.weightProviderRegistry
		
		val thresholds = new TypeMap<Double>(0.5)
		thresholds.put(EcorePackage.eINSTANCE.EPackage, 0.15)
		thresholds.put(EcorePackage.eINSTANCE.EClass, 0.55)
		thresholds.put(EcorePackage.eINSTANCE.EReference, 0.65)
		thresholds.put(EcorePackage.eINSTANCE.EOperation, 0.55)
		thresholds.put(EcorePackage.eINSTANCE.EAttribute, 0.68)
		thresholds.put(EcorePackage.eINSTANCE.EStringToStringMapEntry, 0.4)
		thresholds.put(EcorePackage.eINSTANCE.EEnum, 0.5)
		thresholds.put(EcorePackage.eINSTANCE.EEnumLiteral, 0.45)
		thresholds.put(EcorePackage.eINSTANCE.EParameter, 0.5)
		
		// for UML ...
//		thresholds.put(UMLPackage.eINSTANCE.activity, 0.60);
//		thresholds.put(UMLPackage.eINSTANCE.association, 0.62);
//		thresholds.put(UMLPackage.eINSTANCE.actor, 0.55);
//		thresholds.put(UMLPackage.eINSTANCE.behaviorExecutionSpecification, 0.65);
//		thresholds.put(UMLPackage.eINSTANCE.class_, 0.60);
//		thresholds.put(UMLPackage.eINSTANCE.collaboration, 0.60);
//		thresholds.put(UMLPackage.eINSTANCE.component, 0.50);
//		thresholds.put(UMLPackage.eINSTANCE.dataType, 0.55);
//		thresholds.put(UMLPackage.eINSTANCE.dependency, 0.60);
//		thresholds.put(UMLPackage.eINSTANCE.elementImport, 0.65);
//		thresholds.put(UMLPackage.eINSTANCE.enumeration, 0.55);
//		thresholds.put(UMLPackage.eINSTANCE.enumerationLiteral, 0.35);
//		thresholds.put(UMLPackage.eINSTANCE.executionOccurrenceSpecification, 0.65);
//		thresholds.put(UMLPackage.eINSTANCE.generalOrdering, 0.68);
//		thresholds.put(UMLPackage.eINSTANCE.interaction, 0.60);
//		thresholds.put(UMLPackage.eINSTANCE.instanceSpecification, 0.35);
//		thresholds.put(UMLPackage.eINSTANCE.lifeline, 0.45);
//		thresholds.put(UMLPackage.eINSTANCE.literalInteger, 0.50);
//		thresholds.put(UMLPackage.eINSTANCE.message, 0.55);
//		thresholds.put(UMLPackage.eINSTANCE.messageOccurrenceSpecification, 0.70);
//		thresholds.put(UMLPackage.eINSTANCE.model, 0.40);
//		thresholds.put(UMLPackage.eINSTANCE.occurrenceSpecification, 0.70);
//		thresholds.put(UMLPackage.eINSTANCE.operation, 0.55);
//		thresholds.put(UMLPackage.eINSTANCE.package, 0.50);
//		thresholds.put(UMLPackage.eINSTANCE.parameter, 0.65);
//		thresholds.put(UMLPackage.eINSTANCE.property, 0.65);
//		thresholds.put(UMLPackage.eINSTANCE.stateMachine, 0.55);
//		thresholds.put(UMLPackage.eINSTANCE.useCase, 0.60);
//		thresholds.put(UMLPackage.eINSTANCE.usage, 0.45);
//		thresholds.put(UMLPackage.eINSTANCE.literalUnlimitedNatural, 0.45);
		
		thresholds.put(UMLPackage.eINSTANCE.activity, 0.76);
		thresholds.put(UMLPackage.eINSTANCE.actor, 0.63);
		thresholds.put(UMLPackage.eINSTANCE.class_, 0.67);
		thresholds.put(UMLPackage.eINSTANCE.component, 0.77);
		thresholds.put(UMLPackage.eINSTANCE.association, 0.5);
		thresholds.put(UMLPackage.eINSTANCE.behaviorExecutionSpecification, 0.77);
		thresholds.put(UMLPackage.eINSTANCE.collaboration, 0.67);
		thresholds.put(UMLPackage.eINSTANCE.dataType, 0.61);
		thresholds.put(UMLPackage.eINSTANCE.dependency, 0.60);
		thresholds.put(UMLPackage.eINSTANCE.elementImport, 0.65);
		thresholds.put(UMLPackage.eINSTANCE.enumeration, 0.59);
		thresholds.put(UMLPackage.eINSTANCE.enumerationLiteral, 0.51);
		thresholds.put(UMLPackage.eINSTANCE.executionOccurrenceSpecification, 0.71);
		thresholds.put(UMLPackage.eINSTANCE.generalOrdering, 0.78);
		thresholds.put(UMLPackage.eINSTANCE.interaction, 0.74);
		thresholds.put(UMLPackage.eINSTANCE.instanceSpecification, 0.5);
		thresholds.put(UMLPackage.eINSTANCE.lifeline, 0.57);
		thresholds.put(UMLPackage.eINSTANCE.message, 0.67);
		thresholds.put(UMLPackage.eINSTANCE.messageOccurrenceSpecification, 0.73);
		thresholds.put(UMLPackage.eINSTANCE.model, 0.44);
		thresholds.put(UMLPackage.eINSTANCE.occurrenceSpecification, 0.72);
		thresholds.put(UMLPackage.eINSTANCE.operation, 0.66);
		thresholds.put(UMLPackage.eINSTANCE.package, 0.41);
		thresholds.put(UMLPackage.eINSTANCE.parameter, 0.67);
		thresholds.put(UMLPackage.eINSTANCE.property, 0.76);
		thresholds.put(UMLPackage.eINSTANCE.stateMachine, 0.55);
		thresholds.put(UMLPackage.eINSTANCE.useCase, 0.72);
		thresholds.put(UMLPackage.eINSTANCE.usage, 0.47);
		thresholds.put(UMLPackage.eINSTANCE.abstraction, 0.67);
		thresholds.put(UMLPackage.eINSTANCE.activityFinalNode, 0.73);
		thresholds.put(UMLPackage.eINSTANCE.decisionNode, 0.68);
		thresholds.put(UMLPackage.eINSTANCE.flowFinalNode, 0.68);
		thresholds.put(UMLPackage.eINSTANCE.forkNode, 0.67);
		thresholds.put(UMLPackage.eINSTANCE.joinNode, 0.72);
		thresholds.put(UMLPackage.eINSTANCE.include, 0.74);
		thresholds.put(UMLPackage.eINSTANCE.initialNode, 0.69);
		thresholds.put(UMLPackage.eINSTANCE.interface, 0.67);
		thresholds.put(UMLPackage.eINSTANCE.literalUnlimitedNatural, 0.50);
		thresholds.put(UMLPackage.eINSTANCE.literalInteger, 0.50);
		thresholds.put(UMLPackage.eINSTANCE.literalString, 0.20);
		
		factory.setThresholdMap(thresholds)
		factory.matcherKind = matcherKind
		
		val nohashTypes = #{
			EcorePackage.eINSTANCE.EAnnotation, EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, 
			EcorePackage.eINSTANCE.EStringToStringMapEntry,
			UMLPackage.eINSTANCE.packageImport, UMLPackage.eINSTANCE.literalBoolean
		}
		
		factory.ignoredClasses = nohashTypes
		
		if(metric===ComparisonMetric.normal) {
			var mohashTotal = 0L;
			var missMohashTotal = 0L;
			var critialMissMohashTotal = 0L;
	
			var mohashextTotal = 0L;
			var missMohashExtTotal = 0L;
			var critialMissMohashExtTotal = 0L;
	
			var emfcomTotal = 0L;
			var allTotal = 0L
			
			if(!debug) {
				for(var i=0; i<2; i++) {
					val evaluator = new EvaluateComparator(umlModel.contents,  factory, metric)
					evaluator.evaluate(#[EcorePackage.eINSTANCE.EAnnotation, EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, EcorePackage.eINSTANCE.EStringToStringMapEntry,
						UMLPackage.eINSTANCE.literalBoolean //, UMLPackage.eINSTANCE.literalInteger,  UMLPackage.eINSTANCE.literalUnlimitedNatural
					], #[], #{}, metric)
				}
			}
			
			val modelName = umlModel.URI.trimFileExtension.lastSegment
			
			for(var i=0; i<count; i++) {
				print("  No. "+(i + 1)+"  ")
				val evaluator = new EvaluateComparator(umlModel.contents,  factory, metric)
				val result = evaluator.evaluate(#[EcorePackage.eINSTANCE.EAnnotation, EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, EcorePackage.eINSTANCE.EStringToStringMapEntry //,
					//UMLPackage.eINSTANCE.literalInteger, UMLPackage.eINSTANCE.literalBoolean, UMLPackage.eINSTANCE.literalUnlimitedNatural
				], #[], #{EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, EcorePackage.eINSTANCE.EStringToStringMapEntry,
					UMLPackage.eINSTANCE.literalBoolean, UMLPackage.eINSTANCE.literalInteger, UMLPackage.eINSTANCE.literalUnlimitedNatural
					}, metric) 
				val hasher = factory.hasher
				result.print(modelName, out, hasher, weight)
				mohashTotal += result.mohashTime
				mohashextTotal += result.mohashextTime
				emfcomTotal += result.emfcompTime
				critialMissMohashTotal += result.critialMisses_mohash
				critialMissMohashExtTotal += result.critialMisses_mohashext
				missMohashTotal += result.diffs_mohash
				missMohashExtTotal += result.diffs_mohashext
				allTotal += result.total
			}
			
			out.println("==================================================")
			out.println("====================[SUMMARY]=====================")
			out.println("==================================================")
			out.println('''Time:    Avg(MoHash)=«(mohashTotal as double)/count/1000000.0»    Avg(MoHashExt)=«(mohashextTotal as double)/count/1000000.0»    Avg(EMFComp)=«(emfcomTotal as double)/count/1000000.0»''')
			out.println(String.format('Diff Rate (MoHash):    Total=%.4f%%    Critical=%.4f%%', (missMohashTotal*100 as double)/allTotal, (missMohashTotal*100 as double)/allTotal))
			out.println(String.format('Diff Rate (MoHashExt):    Total=%.4f%%    Critical=%.4f%%', (missMohashExtTotal*100 as double)/allTotal, (missMohashExtTotal*100 as double)/allTotal))
		} else if(metric===ComparisonMetric.execCount) {
			val modelName = umlModel.URI.trimFileExtension.lastSegment
			
			val emfcStats = newIntArrayOfSize(2)
			val mohashStats = newIntArrayOfSize(2)
			val mohashExtStats = newIntArrayOfSize(2)
			
			for(var i=0; i<count; i++) {
				println("  No. "+(i + 1)+"  "+modelName)
				val evaluator = new EvaluateComparator(umlModel.contents,  factory, metric)
				val result = evaluator.evaluate(#[EcorePackage.eINSTANCE.EAnnotation, EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, EcorePackage.eINSTANCE.EStringToStringMapEntry //,
					//UMLPackage.eINSTANCE.literalInteger, UMLPackage.eINSTANCE.literalBoolean, UMLPackage.eINSTANCE.literalUnlimitedNatural
				], #[], #{EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, EcorePackage.eINSTANCE.EStringToStringMapEntry,
					UMLPackage.eINSTANCE.literalBoolean, UMLPackage.eINSTANCE.literalInteger, UMLPackage.eINSTANCE.literalUnlimitedNatural
					}, metric)
				printStats(result.emfcStats, "EMFC", out)
				emfcStats.set(0, emfcStats.get(0) + result.emfcStats.get(0))
				emfcStats.set(1, emfcStats.get(1) + result.emfcStats.get(1))
				printStats(result.mohashStats, "MOHASH", out)
				mohashStats.set(0, mohashStats.get(0) + result.mohashStats.get(0))
				mohashStats.set(1, mohashStats.get(1) + result.mohashStats.get(1))
				printStats(result.mohashExtStats, "MOHASHEXT", out)
				mohashExtStats.set(0, mohashExtStats.get(0) + result.mohashExtStats.get(0))
				mohashExtStats.set(1, mohashExtStats.get(1) + result.mohashExtStats.get(1))
			}
			
			
			out.println("==================================================")
			out.println("====================[SUMMARY]=====================")
			out.println("==================================================")
			out.println('''EMFC:         Avg(#idcheck)=«(emfcStats.get(0) as double)/count»    Avg(#distfun)=«(emfcStats.get(1) as double)/count»''')
			out.println('''MOHASH:       Avg(#idcheck)=«(mohashStats.get(0) as double)/count»    Avg(#distfun)=«(mohashStats.get(1) as double)/count»''')
			out.println('''MOHASHEXT:    Avg(#idcheck)=«(mohashExtStats.get(0) as double)/count»    Avg(#distfun)=«(mohashExtStats.get(1) as double)/count»''')
		}
	}
	
	protected def static void printStats(int[] stats, String header, PrintStream out) {
		out.println('''«header»    #idcheck=«stats.get(0)»    #distfun=«stats.get(1)»''')
	} 
	
	protected def static void evaluateEcoreModel(EPackage model, int count, PrintStream out, ComparisonMetric metric) {
		val factory = new MoHashMatchEngineFactory
		val weight = factory.weightProviderRegistry
		
		val thresholds = new TypeMap<Double>(0.5)
//		thresholds.put(EcorePackage.eINSTANCE.EPackage, 0.15)
//		thresholds.put(EcorePackage.eINSTANCE.EClass, 0.55)
//		thresholds.put(EcorePackage.eINSTANCE.EReference, 0.65)
//		thresholds.put(EcorePackage.eINSTANCE.EOperation, 0.55)
//		thresholds.put(EcorePackage.eINSTANCE.EAttribute, 0.68)
//		thresholds.put(EcorePackage.eINSTANCE.EStringToStringMapEntry, 0.4)
//		thresholds.put(EcorePackage.eINSTANCE.EEnum, 0.5)
//		thresholds.put(EcorePackage.eINSTANCE.EEnumLiteral, 0.45)
//		thresholds.put(EcorePackage.eINSTANCE.EParameter, 0.5)

		thresholds.put(EcorePackage.eINSTANCE.EPackage, 0.46)
		thresholds.put(EcorePackage.eINSTANCE.EClass, 0.59)
		thresholds.put(EcorePackage.eINSTANCE.EReference, 0.74)
		thresholds.put(EcorePackage.eINSTANCE.EAttribute, 0.72)
		thresholds.put(EcorePackage.eINSTANCE.EEnum, 0.52)
		thresholds.put(EcorePackage.eINSTANCE.EEnumLiteral, 0.53)
		thresholds.put(EcorePackage.eINSTANCE.EOperation, 0.61)
		thresholds.put(EcorePackage.eINSTANCE.EParameter, 0.66)
		thresholds.put(EcorePackage.eINSTANCE.EStringToStringMapEntry, 0.4)
		
		factory.setThresholdMap(thresholds)
		factory.matcherKind = matcherKind
		
		val nohashTypes = newHashSet(EcorePackage.eINSTANCE.EAnnotation, EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, EcorePackage.eINSTANCE.EStringToStringMapEntry);
		
		factory.ignoredClasses = nohashTypes
		
		if(metric===ComparisonMetric.normal) {			
			var mohashTotal = 0L;
			var missMohashTotal = 0L;
			var critialMissMohashTotal = 0L;
	
			var mohashextTotal = 0L;
			var missMohashExtTotal = 0L;
			var critialMissMohashExtTotal = 0L;
	
			var emfcomTotal = 0L;
			var allTotal = 0L
		
			if(!debug) {
				for(var i=0; i<2; i++) {
					val evaluator = new EvaluateComparator(model,  factory, metric)
					evaluator.evaluate(#[EcorePackage.eINSTANCE.EAnnotation, EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, EcorePackage.eINSTANCE.EStringToStringMapEntry], #[], #{}, metric)
				}
			}
			val modelName = model.name
			for(var i=0; i<count; i++) {
				print("  No. "+(i + 1)+"  ")
				val evaluator = new EvaluateComparator(model,  factory, metric)
				val result = evaluator.evaluate(#[EcorePackage.eINSTANCE.EAnnotation, EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, EcorePackage.eINSTANCE.EStringToStringMapEntry], #[], #{EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, EcorePackage.eINSTANCE.EStringToStringMapEntry}, metric) 
				val hasher = factory.hasher
				result.print(modelName, out, hasher, weight)
				mohashTotal += result.mohashTime
				mohashextTotal += result.mohashextTime
				emfcomTotal += result.emfcompTime
				critialMissMohashTotal += result.critialMisses_mohash
				critialMissMohashExtTotal += result.critialMisses_mohashext
				missMohashTotal += result.diffs_mohash
				missMohashExtTotal += result.diffs_mohashext
				allTotal += result.total
			}
			
			out.println("==================================================")
			out.println("====================[SUMMARY]=====================")
			out.println("==================================================")
			out.println('''Time:    Avg(MoHash)=«(mohashTotal as double)/count/1000000.0»    Avg(MoHashExt)=«(mohashextTotal as double)/count/1000000.0»    Avg(EMFComp)=«(emfcomTotal as double)/count/1000000.0»''')
			out.println(String.format('Diff Rate (MoHash):    Total=%.4f%%    Critical=%.4f%%', (missMohashTotal*100 as double)/allTotal, (missMohashTotal*100 as double)/allTotal))
			out.println(String.format('Diff Rate (MoHashExt):    Total=%.4f%%    Critical=%.4f%%', (missMohashExtTotal*100 as double)/allTotal, (missMohashExtTotal*100 as double)/allTotal))
			out.println(String.format('Details:    AllTotalElement=%d    Count=%d', allTotal, count))
		} else {
			val emfcStats = newIntArrayOfSize(2)
			val mohashStats = newIntArrayOfSize(2)
			val mohashExtStats = newIntArrayOfSize(2)
			
			val modelName = model.name
			for(var i=0; i<count; i++) {
				print("  No. "+(i + 1)+"  "+modelName)
				val evaluator = new EvaluateComparator(model,  factory, metric)
				val result = evaluator.evaluate(#[EcorePackage.eINSTANCE.EAnnotation, EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, EcorePackage.eINSTANCE.EStringToStringMapEntry], #[], #{EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, EcorePackage.eINSTANCE.EStringToStringMapEntry}, metric)
				printStats(result.emfcStats, "EMFC", out)
				emfcStats.set(0, emfcStats.get(0) + result.emfcStats.get(0))
				emfcStats.set(1, emfcStats.get(1) + result.emfcStats.get(1))
				printStats(result.mohashStats, "MOHASH", out)
				mohashStats.set(0, mohashStats.get(0) + result.mohashStats.get(0))
				mohashStats.set(1, mohashStats.get(1) + result.mohashStats.get(1))
				printStats(result.mohashExtStats, "MOHASHEXT", out)
				mohashExtStats.set(0, mohashExtStats.get(0) + result.mohashExtStats.get(0))
				mohashExtStats.set(1, mohashExtStats.get(1) + result.mohashExtStats.get(1))
			}
			
			out.println("==================================================")
			out.println("====================[SUMMARY]=====================")
			out.println("==================================================")
			out.println('''EMFC:         Avg(#idcheck)=«(emfcStats.get(0) as double)/count»    Avg(#distfun)=«(emfcStats.get(1) as double)/count»''')
			out.println('''MOHASH:       Avg(#idcheck)=«(mohashStats.get(0) as double)/count»    Avg(#distfun)=«(mohashStats.get(1) as double)/count»''')
			out.println('''MOHASHEXT:    Avg(#idcheck)=«(mohashExtStats.get(0) as double)/count»    Avg(#distfun)=«(mohashExtStats.get(1) as double)/count»''')
		}
		
	}
	
	
		
	static def Field getFieldDef(Class<?> clazz, String name) {
		val field = clazz.declaredFields.findFirst[it.name==name]
		return if(field===null) {
			if(clazz.superclass===null) null
			else clazz.superclass.getFieldDef(name)
		} else field
	}
	
	static def <T> T getFieldValue(Object object, String name) {
		val clazz = object.class
		val field = clazz.getFieldDef(name)
		return if(field===null) null
			   else {
			   	field.accessible = true
			   	val value = field.get(object)
			   	return value as T
			   }
	}
	
	static def void addFieldValue(Object object, String name, int inc) {
		val clazz = object.class
		val field = clazz.getFieldDef(name)
		if (field === null) {}
		else {
			field.accessible = true
			val value = field.get(object) as Integer
			field.set(object, value + inc)
		}
	}
}

class ComparisonResult {
	public var int[] emfcStats = null;
	public var int[] mohashStats = null;
	public var int[] mohashExtStats = null;
	
	public var Comparison mohash
	public var Comparison mohashext
	public var Comparison emfcomp
	
	public var long mohashTime
	public var long mohashextTime
	public var long emfcompTime
	
	public var int diffs_mohash
	public var int diffs_mohashext
	
	public var int total
	public var int critialMisses_mohash = 0
	public var int critialMisses_mohashext = 0
	
	public var int numOfEdits
	
	
	override toString() {
		return '''
		ComparisonResult [numOfEdits=«numOfEdits», mohashTime=«mohashTime», mohashExtTime=«mohashextTime», emfcompTime=«emfcompTime», diffs_mohash=«diffs_mohash», diffs_mohashext=«diffs_mohashext», total=«total»]
		''' 
	}
	
	
	val Set<EObject> differences = new HashSet
	
	def void recordDiff(EObject source) {
		differences += source
	}
	
	def void print(String header, PrintStream out, EObjectSimHasher hasher, WeightProvider.Descriptor.Registry weight) {
		out.print('''«header» [numOfEdits=«numOfEdits», mohashTime=«mohashTime», mohashExtTime=«mohashextTime», emfcompTime=«emfcompTime», diffs_mohash=«diffs_mohash», diffs_mohashext=«diffs_mohashext», total=«total», ''')
		
		val critical = new HashSet<EObject>()
		for(o : differences) {
//			val m1 = mohash.getMatch(o)
//			val m2 = emfcomp.getMatch(o)
//			
//			val r1 = m1.right
//			val r2 = m2.right
//			val h0 = hasher.hash(o)
//			val h1 = r1!==null ? hasher.hash(r1) : 0
//			val h2 = r2!==null ? hasher.hash(r2) : 0
			
			val pw = weight.getHighestRankingWeightProvider(o.eClass.EPackage).getParentWeight(o);
			
			if(pw<=EcoreWeightProvider.SIGNIFICANT) {
				critical += o
			} else {
				if(!differences.contains(o.eContainer)) {
					critical += o
				}
			}
		}
		
		critialMisses_mohash = critical.size
		
		out.println('''criticalMiss=«critical.size»]''')
		
		critical.forEach[o|
			val m1 = mohash.getMatch(o)
			val m2 = emfcomp.getMatch(o)
			
			val r1 = m1.right
			val r2 = m2.right
			val h0 = hasher.hash(o)
			val h1 = r1!==null ? hasher.hash(r1) : 0
			val h2 = r2!==null ? hasher.hash(r2) : 0
			
			out.println("Content")
			out.print("Common left\t") out.print(o) out.print("\t") out.print(o.eContainer) out.print("\t") out.println(o.eContainer?.eContainer)
			out.print("Mohash.right\t") out.print(r1) out.print("\t") out.print(r1?.eContainer) out.print("\t") out.println(r1?.eContainer?.eContainer)
			out.print("EMFcom.right\t") out.print(r2) out.print("\t") out.print(r2?.eContainer) out.print("\t") out.println(r2?.eContainer?.eContainer)
			out.println("Hash value")
			out.print("Common left\t") 
			out.println(Hash64.toString(h0))
			out.print("Mohash.right\t") 
			out.print(Hash64.toString(h1))
			if(h1!==0) {
				out.print("\t")
				out.println(ObjectIndex.similarity(new HashValue64(h0), new HashValue64(h1)))
			}
			else out.println()
			out.print("EMFcom.right\t") 
			out.print(Hash64.toString(h2))
			if(h2!==0) {
				out.print("\t")
				out.println(ObjectIndex.similarity(new HashValue64(h0), new HashValue64(h2)))
			}
			else out.println()
			out.println
		]
//		differences.forEach[o|
//			val m1 = mohash.getMatch(o)
//			val m2 = emfcomp.getMatch(o)
//			
//			val r1 = m1.right
//			val r2 = m2.right
//			val h0 = hasher.hash(o)
//			val h1 = r1!==null ? hasher.hash(r1) : 0
//			val h2 = r2!==null ? hasher.hash(r2) : 0
//			
//			out.println("Content")
//			out.print("Common left\t") out.print(o) out.print("\t") out.println(o.eContainer)
//			out.print("Mohash.right\t") out.println(r1)
//			out.print("EMFcom.right\t") out.println(r2)
//			out.println("Hash value")
//			out.print("Common left\t") 
//			out.println(Hash64.toString(h0))
//			out.print("Mohash.right\t") 
//			out.print(Hash64.toString(h1))
//			if(h1!==0) {
//				out.print("\t")
//				out.println(ObjectIndex.similarity(new HashValue64(h0), new HashValue64(h1)))
//			}
//			else out.println()
//			out.print("EMFcom.right\t") 
//			out.print(Hash64.toString(h2))
//			if(h2!==0) {
//				out.print("\t")
//				out.println(ObjectIndex.similarity(new HashValue64(h0), new HashValue64(h2)))
//			}
//			else out.println()
//			out.println
//		]
	}

}

enum ComparisonMetric {
	normal, execCount
}