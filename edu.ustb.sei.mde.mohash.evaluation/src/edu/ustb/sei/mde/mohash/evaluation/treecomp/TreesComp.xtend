package edu.ustb.sei.mde.mohash.evaluation.treecomp

import edu.ustb.sei.mde.mumodel.ResourceSetExtension
import org.eclipse.emf.ecore.resource.Resource
import java.util.List
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil
import edu.ustb.sei.mde.mumodel.ModelMutator
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EStructuralFeature
import java.util.Set
import java.util.HashMap
import com.google.common.collect.Multimap
import com.google.common.collect.Multimaps
import com.google.common.collect.MultimapBuilder
import com.google.common.collect.ArrayListMultimap
import java.util.Map
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.uml2.uml.UMLPackage
import java.io.File

class TreesComp {
	static extension ResourceSetExtension = new ResourceSetExtension
	val Resource original
	val Resource mutant
	
	new(List<EObject> roots) {
		original = createResource('/Mohash/pseudo_original.xmi')
		mutant = createResource('/Mohash/pseudo_mutant.xmi')
		original.contents += EcoreUtil.copyAll(roots)
		
		original.contents.forEach[it.computeStringIndex(originalIndexMap, originalInvIndexMap)]
	}
	
	protected def String genAttributeValue(Object value) {
		if(value instanceof List) {
			'''[«FOR v : value SEPARATOR ','»«v.genAttributeValue»«ENDFOR»]'''
		} else {
			switch(value) {
				String : value
				Integer : value.toString
				Boolean : value.toString
				case null: '__NILL__'
				default : value.toString
			}
		}
	}
	
	val originalIndexMap = new HashMap<EObject, String>
	val originalInvIndexMap = ArrayListMultimap.<String, EObject>create
	
	val mutantIndexMap = new HashMap<EObject, String>
	val mutantInvIndexMap = ArrayListMultimap.<String, EObject>create
	
	protected def String computeStringIndex(EObject object, Map<EObject, String> indexMap, Multimap<String, EObject> invIndexMap) {
		val allAttributes = object.eClass.EAllAttributes
		val localString = allAttributes.map[it.name + ":" + genAttributeValue(object.eGet(it))].reduce[l, r | l + ";" + r]
		val childrenString = (object.eContents.map[it.computeStringIndex(indexMap, invIndexMap)].reduce[l, r | l + ";" + r])
		val index = object.eClass.name + ": {" + localString + '} {' + childrenString + '}'
		
		indexMap.put(object, index)
		invIndexMap.put(index, object)
		
		return index
	}
	
	def void evaluate(List<EClass> ignoredClasses, List<EStructuralFeature> ignoredFeatures, Set<EClass> neglectedTypes) {
		val mutator = new ModelMutator
		mutator.ignoredClasses = ignoredClasses
		mutator.ignoredFeatures = ignoredFeatures
		
		mutant.contents.clear
		
		mutator.mutateResource(original, mutant)
		mutantIndexMap.clear
		mutantInvIndexMap.clear
		
		mutant.contents.forEach[it.computeStringIndex(mutantIndexMap, mutantInvIndexMap)]
		
		
		var totalElements = 0;
		var coveredElements = 0;
		var critical = 0;
		var criticalSize = 0;
		var ambi = 0
		var ambiCriticalSize = 0
		
		val iter = original.allContents
		while(iter.hasNext) {
			val cur = iter.next
			val hash = originalIndexMap.get(cur)
			var isAmbi = false
			val hashMatch = 
				if(hash!==null) {
					val list = mutantInvIndexMap.get(hash)
					
					if(list.size > 1) 
						isAmbi = true
					
					if(list.empty) false
					else true
				} else false
			
			val size = cur.treeSize
			if(hashMatch) {
				totalElements += size
				coveredElements += size
				if(size > 1) {
					critical ++
					criticalSize += size
				}
				iter.prune
			} else {
				totalElements ++
			}
			
			if(isAmbi) {
				ambi ++
				ambiCriticalSize += size
			}
		}
		
		println('''Total = «totalElements», Covered = «coveredElements», critical = «critical» («criticalSize»), ambi = «ambi» («ambiCriticalSize»)''')
	}
	
	protected def int treeSize(EObject element) {
		1 + (element.eContents.map[it.treeSize].reduce[l,r| l + r] ?: 0)
	}
	
	def static void main(String[] args) {
		val modelFolder = new File('C:/JavaProjects/git/mohash/edu.ustb.sei.mde.mohash.evaluation/model/uml/data_top_20')
		val models = modelFolder.listFiles.filter[it.name.endsWith(".xmi")].map [
			// for uml
			it.absolutePath.loadUMLResource
		].toList
		
		models.forEach[umlModel|
			println(umlModel.URI)
			for(var i=0; i<10; i++) {
				val evaluator = new TreesComp(umlModel.contents)
					evaluator.evaluate(#[EcorePackage.eINSTANCE.EAnnotation, EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, EcorePackage.eINSTANCE.EStringToStringMapEntry //,
						//UMLPackage.eINSTANCE.literalInteger, UMLPackage.eINSTANCE.literalBoolean, UMLPackage.eINSTANCE.literalUnlimitedNatural
					], #[], #{EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, EcorePackage.eINSTANCE.EStringToStringMapEntry,
						UMLPackage.eINSTANCE.literalBoolean, UMLPackage.eINSTANCE.literalInteger, UMLPackage.eINSTANCE.literalUnlimitedNatural
						})
			}
			println
		]
	}
}