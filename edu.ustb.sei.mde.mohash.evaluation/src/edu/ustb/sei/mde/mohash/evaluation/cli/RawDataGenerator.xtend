package edu.ustb.sei.mde.mohash.evaluation.cli

import edu.ustb.sei.mde.mohash.EObjectSimHasher
import edu.ustb.sei.mde.mumodel.ElementMutator
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.PrintStream
import java.util.ArrayList
import java.util.Arrays
import java.util.List
import java.util.Map
import java.util.function.Function
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.compare.ComparePackage
import org.eclipse.emf.compare.Comparison
import org.eclipse.emf.compare.Match
import org.eclipse.emf.compare.match.eobject.EditionDistance
import org.eclipse.emf.compare.match.eobject.ProximityEObjectMatcher.DistanceFunction
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.uml2.uml.UMLPackage
import edu.ustb.sei.mde.mohash.factory.MoHashMatchEngineFactory

class RawDataGenerator {
	val EditionDistance distance
	val EObjectSimHasher hasher
	val objectsOfType = new ArrayList<EObject>
	val EClass focusedType
	val Function<EObject, String> objectPrinter
	val ModelMutationSimMatrix matrix = new ModelMutationSimMatrix()
	val List<EObject> roots
	
	protected def void hackDistFun() {
		val clazz = distance.class
		val thresholdField = clazz.declaredFields.findFirst[it.name == 'thresholds']
		thresholdField.accessible = true
		
		val thresholds = newDoubleArrayOfSize(100);
		Arrays.fill(thresholds, 1.0);
		thresholdField.set(distance, thresholds);
		thresholdField.accessible = false
	}
	
	new(String path, EClass type, DistanceFunction distance, EObjectSimHasher hasher, List<EObject> roots, Function<EObject, String> objectPrinter) {
		this.focusedType = type
		this.distance = distance as EditionDistance
		this.objectPrinter = objectPrinter
		this.hasher = hasher
		this.pathname = new File(path)
		this.roots = roots
		
		
	}
	
	def printObjects() {
		println('''«FOR o : objectsOfType SEPARATOR ','»«objectPrinter.apply(o)»«ENDFOR»''')
	}
	
	def constructEnhancedMatrix() {
		for(root : roots) {
			objectsOfType.clear
			if(focusedType===root.eClass) objectsOfType += root
			root.eAllContents.filter[focusedType===it.eClass].forEach[objectsOfType += it]
			this.buildEnhancedMatrix
		}
		this.printEnhancedObjectsCode
		this.printEnhancedOracleLogMatrix
		this.printEnhancedMohash
	}
	
	protected def buildEnhancedMatrix() {
		val mutator = new ElementMutator(this.focusedType)
		mutator.prepare(objectsOfType)
		val allObjects = mutator.selectAll
		
		// add original objects and original matrix
		allObjects.forEach[
			matrix.addObject(it)
		]
		
		for(var i = 0; i< allObjects.size; i++) {
			val ro = allObjects.get(i)
			for(var j = i; j < allObjects.size; j++) {
				val co = allObjects.get(j)
				calcSimLogMatCell(ro, co)
			}
		}
		
		val mutationSize = Math.min(10, allObjects.size)
		
		// for each rate, for each object, mutate
		
		for(var mr = 0.2; mr <= 0.9; mr += 0.1) {
			mutator.featureChangeRate = mr
			for(i : 0..<mutationSize) {
				mutator.push
				for(o : allObjects) {
					mutator.mutate(o)
					val mo = mutator.getMutant(o)
					matrix.addObject(mo)
					calcSimLogMatCell(o, mo)
				}				
			}
		}
		
		println('Total size: ' + matrix.size)
	}
	
	protected def printEnhancedObjectsCode() {
		val out = makeFileOutputStream('rawcode.txt')
		matrix.printRawCode(hasher, this.focusedType, out)
		out.flush
		out.close
	}
	
	protected def printEnhancedOracleLogMatrix() {
		val out = makeFileOutputStream('oracleLogSP.txt')
		matrix.printLogMatrix(out)
		out.flush
		out.close
	}
	
	protected def void calcSimLogMatCell(EObject ro, EObject co) {
		val comp = EcoreUtil.create(ComparePackage.eINSTANCE.comparison) as Comparison
		val match_par = EcoreUtil.create(ComparePackage.eINSTANCE.match) as Match
		match_par.left = ro.eContainer
		match_par.right = co.eContainer
		
		comp.matches += match_par
		val rawDist = distance.distance(comp, ro, co)
		
		val dist = if(rawDist===Double.MAX_VALUE) -1 else 1
		matrix.setSimLog(ro, co, dist)
	}
	
	def printObjectSimilarityLogicalMatrix() {
		val out = makeFileOutputStream('oracleLog.txt')
		
		val results = newArrayOfSize(objectsOfType.size, objectsOfType.size)
		
		objectsOfType.forEach[l, li|
			objectsOfType.forEach[r, ri|
				val comp = EcoreUtil.create(ComparePackage.eINSTANCE.comparison) as Comparison
				val match_par = EcoreUtil.create(ComparePackage.eINSTANCE.match) as Match
				match_par.left = l.eContainer
				match_par.right = r.eContainer
				
				comp.matches += match_par
				val rawDist = distance.distance(comp, l, r)
				
				val dist = if(rawDist===Double.MAX_VALUE) -1 else 1
				
				results.set(li, ri, dist)
			]
		]
		
		for(i : 0..<objectsOfType.size) {
			for(j : 0..<objectsOfType.size) {
				out.print(results.get(i, j))
				if(j!==objectsOfType.size - 1) out.print(' ')
			}
			out.println
		}
		
		out.flush
		out.close
	}
	
	def printObjectSimilarityLogicalSparseMatrix() {
		val out = makeFileOutputStream('oracleLog.txt')
		
		val results = newArrayOfSize(objectsOfType.size, objectsOfType.size)
		
		objectsOfType.forEach[l, li|
			objectsOfType.forEach[r, ri|
				val comp = EcoreUtil.create(ComparePackage.eINSTANCE.comparison) as Comparison
				val match_par = EcoreUtil.create(ComparePackage.eINSTANCE.match) as Match
				match_par.left = l.eContainer
				match_par.right = r.eContainer
				
				comp.matches += match_par
				val rawDist = distance.distance(comp, l, r)
				
				val dist = if(rawDist===Double.MAX_VALUE) -1 else 1
				
				results.set(li, ri, dist)
			]
		]
		
		for(i : 0..<objectsOfType.size) {
			for(j : i..<objectsOfType.size) {
				if(results.get(i, j)!==0)
					out.println('''«i» «j» «results.get(i, j)»''')
			}
		}
		
		out.flush
		out.close
	}
	
	def printObjectSimilarityMatrix() {
		val out = makeFileOutputStream('oracle.txt')
		hackDistFun
		val results = newArrayOfSize(objectsOfType.size, objectsOfType.size)
		
		objectsOfType.forEach[l, li|
			objectsOfType.forEach[r, ri|
				val comp = EcoreUtil.create(ComparePackage.eINSTANCE.comparison) as Comparison
				val match_par = EcoreUtil.create(ComparePackage.eINSTANCE.match) as Match
				match_par.left = l.eContainer
				match_par.right = r.eContainer
				
				comp.matches += match_par
				val rawDist = distance.distance(comp, l, r)
				val thresh = Math.max(distance.getThresholdAmount(l), distance.getThresholdAmount(r)) + 1
				
				val dist = if(rawDist===Double.MAX_VALUE) thresh else rawDist
				
				val sim = 1.0 - dist / thresh
				results.set(li, ri, sim)
			]
		]
		
		for(i : 0..<objectsOfType.size) {
			for(j : 0..<objectsOfType.size) {
				out.print(results.get(i, j))
				if(j!==objectsOfType.size - 1) out.print(' ')
			}
			out.println
		}
		
		out.flush
		out.close
	}
	
	def printFeatures() {
		val list = hasher.getFeatureHasherTuples(focusedType)
		println('''«FOR f : list SEPARATOR ','»«f.feature.name»«ENDFOR»''')
	}
	
	def printRawCode() {
		val out = makeFileOutputStream('rawcode.txt')

		objectsOfType.forEach[
			val code = hasher.computeRawCode(it, focusedType)
			out.println('''«FOR c : code SEPARATOR ' '»«c»«ENDFOR»''')
		]
		
		out.flush
		out.close
	}
	
	def printMohash() {
		val out = makeFileOutputStream('mohash.txt')
		
		objectsOfType.forEach[
			val code = hasher.hash(it)
			out.println(code)
		]
		
		out.flush
		out.close
	}
	
	def printEnhancedMohash() {
		val out = makeFileOutputStream('mohash.txt')
		
		matrix.allObjects.forEach[
			val code = hasher.hash(it)
			out.println(code)
		]
		
		out.flush
		out.close
	}
	
	
	val File pathname;
	
	def makeFileOutputStream(String filename) {
		val file = new File(pathname, filename)
		new PrintStream(new BufferedOutputStream(new FileOutputStream(file)))
	}
	
	def static void main(String[] args) {
		val modelPath = 'C:\\JavaProjects\\git\\mohash\\edu.ustb.sei.mde.mohash.evaluation\\model\\ecore\\data_medium_20'
		
		val modelRoots = loadModels(modelPath)
		
		val factory = new MoHashMatchEngineFactory()
		factory.matchEngine
		val generator = new RawDataGenerator('C:\\Users\\dr-he\\OneDrive\\Documents\\MATLAB',
			EcorePackage.Literals.ECLASS, factory.distance, factory.hasher, modelRoots //#[UMLPackage.eINSTANCE]
		)[
			it.eGet(EcorePackage.Literals.ENAMED_ELEMENT__NAME) as String
		]
		
		generator.constructEnhancedMatrix
		
//		generator.printObjects
//		println
//		generator.printObjectSimilarityLogicalMatrix
//		println
//		generator.printObjectSimilarityMatrix
//		println
//		generator.printFeatures
//		println
//		generator.printRawCode
//		println
//		generator.printMohash
	}
	
	protected def static List<EObject> loadModels(String modelPath) {
		val pathFile = new File(modelPath)
		val models = <EObject>newArrayList
		
		val resourceSet = new ResourceSetImpl()
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put('ecore', new XMIResourceFactoryImpl());
		resourceSet.packageRegistry.put(EcorePackage.eINSTANCE.nsURI, EcorePackage.eINSTANCE)
		
		pathFile.listFiles.filter[it.name.endsWith('.ecore')].forEach[
			val uri = URI.createFileURI(it.absolutePath)
			try {
				val resource = resourceSet.getResource(uri, true)
				models += resource.contents.get(0)
			} catch(Exception e) {
			}
		]
		
		return models
	}
}

class ModelMutationSimMatrix {
	val Map<EObject, Integer> objectIdMap = newLinkedHashMap
	val Map<Integer, Map<Integer, Integer>> simLogMatrix = newHashMap
	
	def void setSimLog(EObject ro, EObject co, Integer sim) {
		val r = objectIdMap.get(ro)
		val c = objectIdMap.get(co)
		
		val m = simLogMatrix.computeIfAbsent(r)[newHashMap]
		m.put(c, sim)
	}
	
	def allObjects() {
		objectIdMap.entrySet().map[it.key]
	}
	
	def Integer getSimLog(Integer r, Integer c) {
		val m = simLogMatrix.get(r)
		if(m === null) 0
		else {
			val s = m.get(c)
			if(s===null) 0 else s
		}
	}
	
	def addObject(EObject obj) {
		val id = objectIdMap.size
		objectIdMap.put(obj, id)
		return id
	}
	
	def printRawCode(EObjectSimHasher hasher,EClass focusedType, PrintStream out) {
		for(o : objectIdMap.entrySet) {
			val code = hasher.computeRawCode(o.key, focusedType)
			out.println('''«FOR c : code SEPARATOR ' '»«c»«ENDFOR»''')
		}
	}
	
	def printLogMatrix(PrintStream out) {
		val size = objectIdMap.size
		
		for(r : 0..<size) {
			for(c : r..<size) {
				val l = if(r===c) 1 else getSimLog(r,c)
				if(l!==0)
					out.println('''«r» «c» «l»''')
			}
		}
	}
	
	def getSize() {
		objectIdMap.size
	}
	
}