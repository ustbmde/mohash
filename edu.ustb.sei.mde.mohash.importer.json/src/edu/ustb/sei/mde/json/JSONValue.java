/**
 */
package edu.ustb.sei.mde.json;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JSON Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONValue()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface JSONValue extends EObject {
} // JSONValue
