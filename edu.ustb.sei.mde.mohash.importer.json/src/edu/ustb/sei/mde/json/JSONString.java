/**
 */
package edu.ustb.sei.mde.json;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JSON String</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.json.JSONString#getLiteral <em>Literal</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONString()
 * @model
 * @generated
 */
public interface JSONString extends JSONNode, JSONValue {
	/**
	 * Returns the value of the '<em><b>Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literal</em>' attribute.
	 * @see #setLiteral(String)
	 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONString_Literal()
	 * @model
	 * @generated
	 */
	String getLiteral();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.json.JSONString#getLiteral <em>Literal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Literal</em>' attribute.
	 * @see #getLiteral()
	 * @generated
	 */
	void setLiteral(String value);

} // JSONString
