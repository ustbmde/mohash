/**
 */
package edu.ustb.sei.mde.json.impl;

import edu.ustb.sei.mde.json.JSONBool;
import edu.ustb.sei.mde.json.JSONField;
import edu.ustb.sei.mde.json.JSONNumber;
import edu.ustb.sei.mde.json.JSONObject;
import edu.ustb.sei.mde.json.JSONString;
import edu.ustb.sei.mde.json.JSONValue;
import edu.ustb.sei.mde.json.JsonFactory;
import edu.ustb.sei.mde.json.JsonPackage;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>JSON Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.json.impl.JSONObjectImpl#getFields <em>Fields</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.json.impl.JSONObjectImpl#getId <em>Id</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.json.impl.JSONObjectImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JSONObjectImpl extends JSONNodeImpl implements JSONObject {
	/**
	 * The cached value of the '{@link #getFields() <em>Fields</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFields()
	 * @generated
	 * @ordered
	 */
	protected EList<JSONField> fields;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JSONObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JsonPackage.Literals.JSON_OBJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JSONField> getFields() {
		if (fields == null) {
			fields = new EObjectContainmentEList<JSONField>(JSONField.class, this, JsonPackage.JSON_OBJECT__FIELDS);
		}
		return fields;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JsonPackage.JSON_OBJECT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JsonPackage.JSON_OBJECT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Object get(String key) {
		for(JSONField f : this.getFields()) {
			if(Objects.equals(f.getKey(), key)) {
				return unwrap(f.getValue());
			}
		}
		return null;
	}

	private Object unwrap(JSONValue value) {
		if(value instanceof JSONString) {
			return ((JSONString) value).getLiteral();
		} else if(value instanceof JSONNumber) {
			return ((JSONNumber) value).getLiteral();
		} else if(value instanceof JSONBool) {
			return ((JSONBool)value).isLiteral();
		} else if(value == null) return null;
		else return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void set(String key, Object value) {
		JSONValue jValue = wrap(value);
		
		for(JSONField f : this.getFields()) {
			if(Objects.equals(f.getKey(), key)) {
				f.setValue(jValue);
				return;
			}
		}
		
		JSONField f = JsonFactory.eINSTANCE.createJSONField();
		f.setKey(key);
		f.setValue(jValue);
		this.getFields().add(f);
	}

	private JSONValue wrap(Object value) {
		if(value instanceof JSONValue) return (JSONValue) value;
		else if(value == null) return null;
		else if(value instanceof String) {
			JSONString  v = JsonFactory.eINSTANCE.createJSONString();
			v.setLiteral((String) value);
			return v;
		} else if(value instanceof BigDecimal) {
			JSONNumber  v = JsonFactory.eINSTANCE.createJSONNumber();
			v.setLiteral((BigDecimal) value);
			return v;
		} else if(value instanceof Boolean) {
			JSONBool  v = JsonFactory.eINSTANCE.createJSONBool();
			v.setLiteral((Boolean) value);
			return v;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JsonPackage.JSON_OBJECT__FIELDS:
				return ((InternalEList<?>)getFields()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JsonPackage.JSON_OBJECT__FIELDS:
				return getFields();
			case JsonPackage.JSON_OBJECT__ID:
				return getId();
			case JsonPackage.JSON_OBJECT__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JsonPackage.JSON_OBJECT__FIELDS:
				getFields().clear();
				getFields().addAll((Collection<? extends JSONField>)newValue);
				return;
			case JsonPackage.JSON_OBJECT__ID:
				setId((String)newValue);
				return;
			case JsonPackage.JSON_OBJECT__TYPE:
				setType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JsonPackage.JSON_OBJECT__FIELDS:
				getFields().clear();
				return;
			case JsonPackage.JSON_OBJECT__ID:
				setId(ID_EDEFAULT);
				return;
			case JsonPackage.JSON_OBJECT__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JsonPackage.JSON_OBJECT__FIELDS:
				return fields != null && !fields.isEmpty();
			case JsonPackage.JSON_OBJECT__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case JsonPackage.JSON_OBJECT__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case JsonPackage.JSON_OBJECT___GET__STRING:
				return get((String)arguments.get(0));
			case JsonPackage.JSON_OBJECT___SET__STRING_OBJECT:
				set((String)arguments.get(0), arguments.get(1));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}
	
	

} //JSONObjectImpl
