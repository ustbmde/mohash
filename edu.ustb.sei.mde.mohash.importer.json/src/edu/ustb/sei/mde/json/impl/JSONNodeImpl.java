/**
 */
package edu.ustb.sei.mde.json.impl;

import edu.ustb.sei.mde.json.JSONNode;
import edu.ustb.sei.mde.json.JsonPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>JSON Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.json.impl.JSONNodeImpl#getStartingLine <em>Starting Line</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.json.impl.JSONNodeImpl#getOffsetInStartingLine <em>Offset In Starting Line</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.json.impl.JSONNodeImpl#getEndingLine <em>Ending Line</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.json.impl.JSONNodeImpl#getOffsetInEndingLine <em>Offset In Ending Line</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class JSONNodeImpl extends MinimalEObjectImpl.Container implements JSONNode {
	/**
	 * The default value of the '{@link #getStartingLine() <em>Starting Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartingLine()
	 * @generated
	 * @ordered
	 */
	protected static final int STARTING_LINE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStartingLine() <em>Starting Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartingLine()
	 * @generated
	 * @ordered
	 */
	protected int startingLine = STARTING_LINE_EDEFAULT;

	/**
	 * The default value of the '{@link #getOffsetInStartingLine() <em>Offset In Starting Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffsetInStartingLine()
	 * @generated
	 * @ordered
	 */
	protected static final int OFFSET_IN_STARTING_LINE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOffsetInStartingLine() <em>Offset In Starting Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffsetInStartingLine()
	 * @generated
	 * @ordered
	 */
	protected int offsetInStartingLine = OFFSET_IN_STARTING_LINE_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndingLine() <em>Ending Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndingLine()
	 * @generated
	 * @ordered
	 */
	protected static final int ENDING_LINE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getEndingLine() <em>Ending Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndingLine()
	 * @generated
	 * @ordered
	 */
	protected int endingLine = ENDING_LINE_EDEFAULT;

	/**
	 * The default value of the '{@link #getOffsetInEndingLine() <em>Offset In Ending Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffsetInEndingLine()
	 * @generated
	 * @ordered
	 */
	protected static final int OFFSET_IN_ENDING_LINE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOffsetInEndingLine() <em>Offset In Ending Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffsetInEndingLine()
	 * @generated
	 * @ordered
	 */
	protected int offsetInEndingLine = OFFSET_IN_ENDING_LINE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JSONNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JsonPackage.Literals.JSON_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getStartingLine() {
		return startingLine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartingLine(int newStartingLine) {
		int oldStartingLine = startingLine;
		startingLine = newStartingLine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JsonPackage.JSON_NODE__STARTING_LINE, oldStartingLine, startingLine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOffsetInStartingLine() {
		return offsetInStartingLine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOffsetInStartingLine(int newOffsetInStartingLine) {
		int oldOffsetInStartingLine = offsetInStartingLine;
		offsetInStartingLine = newOffsetInStartingLine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JsonPackage.JSON_NODE__OFFSET_IN_STARTING_LINE, oldOffsetInStartingLine, offsetInStartingLine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getEndingLine() {
		return endingLine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndingLine(int newEndingLine) {
		int oldEndingLine = endingLine;
		endingLine = newEndingLine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JsonPackage.JSON_NODE__ENDING_LINE, oldEndingLine, endingLine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOffsetInEndingLine() {
		return offsetInEndingLine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOffsetInEndingLine(int newOffsetInEndingLine) {
		int oldOffsetInEndingLine = offsetInEndingLine;
		offsetInEndingLine = newOffsetInEndingLine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JsonPackage.JSON_NODE__OFFSET_IN_ENDING_LINE, oldOffsetInEndingLine, offsetInEndingLine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JsonPackage.JSON_NODE__STARTING_LINE:
				return getStartingLine();
			case JsonPackage.JSON_NODE__OFFSET_IN_STARTING_LINE:
				return getOffsetInStartingLine();
			case JsonPackage.JSON_NODE__ENDING_LINE:
				return getEndingLine();
			case JsonPackage.JSON_NODE__OFFSET_IN_ENDING_LINE:
				return getOffsetInEndingLine();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JsonPackage.JSON_NODE__STARTING_LINE:
				setStartingLine((Integer)newValue);
				return;
			case JsonPackage.JSON_NODE__OFFSET_IN_STARTING_LINE:
				setOffsetInStartingLine((Integer)newValue);
				return;
			case JsonPackage.JSON_NODE__ENDING_LINE:
				setEndingLine((Integer)newValue);
				return;
			case JsonPackage.JSON_NODE__OFFSET_IN_ENDING_LINE:
				setOffsetInEndingLine((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JsonPackage.JSON_NODE__STARTING_LINE:
				setStartingLine(STARTING_LINE_EDEFAULT);
				return;
			case JsonPackage.JSON_NODE__OFFSET_IN_STARTING_LINE:
				setOffsetInStartingLine(OFFSET_IN_STARTING_LINE_EDEFAULT);
				return;
			case JsonPackage.JSON_NODE__ENDING_LINE:
				setEndingLine(ENDING_LINE_EDEFAULT);
				return;
			case JsonPackage.JSON_NODE__OFFSET_IN_ENDING_LINE:
				setOffsetInEndingLine(OFFSET_IN_ENDING_LINE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JsonPackage.JSON_NODE__STARTING_LINE:
				return startingLine != STARTING_LINE_EDEFAULT;
			case JsonPackage.JSON_NODE__OFFSET_IN_STARTING_LINE:
				return offsetInStartingLine != OFFSET_IN_STARTING_LINE_EDEFAULT;
			case JsonPackage.JSON_NODE__ENDING_LINE:
				return endingLine != ENDING_LINE_EDEFAULT;
			case JsonPackage.JSON_NODE__OFFSET_IN_ENDING_LINE:
				return offsetInEndingLine != OFFSET_IN_ENDING_LINE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (startingLine: ");
		result.append(startingLine);
		result.append(", offsetInStartingLine: ");
		result.append(offsetInStartingLine);
		result.append(", endingLine: ");
		result.append(endingLine);
		result.append(", offsetInEndingLine: ");
		result.append(offsetInEndingLine);
		result.append(')');
		return result.toString();
	}

} //JSONNodeImpl
