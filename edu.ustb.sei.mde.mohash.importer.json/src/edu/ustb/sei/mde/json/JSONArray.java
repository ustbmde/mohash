/**
 */
package edu.ustb.sei.mde.json;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JSON Array</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.json.JSONArray#getValues <em>Values</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONArray()
 * @model
 * @generated
 */
public interface JSONArray extends JSONNode, JSONValue {
	/**
	 * Returns the value of the '<em><b>Values</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.json.JSONValue}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Values</em>' containment reference list.
	 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONArray_Values()
	 * @model containment="true"
	 * @generated
	 */
	EList<JSONValue> getValues();

} // JSONArray
