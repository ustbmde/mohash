/**
 */
package edu.ustb.sei.mde.json;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.json.JsonPackage
 * @generated
 */
public interface JsonFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	JsonFactory eINSTANCE = edu.ustb.sei.mde.json.impl.JsonFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>JSON String</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JSON String</em>'.
	 * @generated
	 */
	JSONString createJSONString();

	/**
	 * Returns a new object of class '<em>JSON Number</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JSON Number</em>'.
	 * @generated
	 */
	JSONNumber createJSONNumber();

	/**
	 * Returns a new object of class '<em>JSON Bool</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JSON Bool</em>'.
	 * @generated
	 */
	JSONBool createJSONBool();

	/**
	 * Returns a new object of class '<em>JSON Object</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JSON Object</em>'.
	 * @generated
	 */
	JSONObject createJSONObject();

	/**
	 * Returns a new object of class '<em>JSON Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JSON Field</em>'.
	 * @generated
	 */
	JSONField createJSONField();

	/**
	 * Returns a new object of class '<em>JSON Array</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JSON Array</em>'.
	 * @generated
	 */
	JSONArray createJSONArray();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	JsonPackage getJsonPackage();

} //JsonFactory
