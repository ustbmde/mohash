/**
 */
package edu.ustb.sei.mde.json;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JSON Field</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.json.JSONField#getKey <em>Key</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.json.JSONField#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONField()
 * @model
 * @generated
 */
public interface JSONField extends JSONNode {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' attribute.
	 * @see #setKey(String)
	 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONField_Key()
	 * @model
	 * @generated
	 */
	String getKey();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.json.JSONField#getKey <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' attribute.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(String value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(JSONValue)
	 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONField_Value()
	 * @model containment="true"
	 * @generated
	 */
	JSONValue getValue();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.json.JSONField#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(JSONValue value);

} // JSONField
