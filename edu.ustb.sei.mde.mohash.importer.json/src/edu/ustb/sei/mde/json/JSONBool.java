/**
 */
package edu.ustb.sei.mde.json;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JSON Bool</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.json.JSONBool#isLiteral <em>Literal</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONBool()
 * @model
 * @generated
 */
public interface JSONBool extends JSONNode, JSONValue {
	/**
	 * Returns the value of the '<em><b>Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literal</em>' attribute.
	 * @see #setLiteral(boolean)
	 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONBool_Literal()
	 * @model
	 * @generated
	 */
	boolean isLiteral();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.json.JSONBool#isLiteral <em>Literal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Literal</em>' attribute.
	 * @see #isLiteral()
	 * @generated
	 */
	void setLiteral(boolean value);

} // JSONBool
