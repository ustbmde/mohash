/**
 */
package edu.ustb.sei.mde.json;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.json.JsonFactory
 * @model kind="package"
 * @generated
 */
public interface JsonPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "json";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ustb.edu.cn/sei/mde/json";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "json";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	JsonPackage eINSTANCE = edu.ustb.sei.mde.json.impl.JsonPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.json.impl.JSONNodeImpl <em>JSON Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.json.impl.JSONNodeImpl
	 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONNode()
	 * @generated
	 */
	int JSON_NODE = 0;

	/**
	 * The feature id for the '<em><b>Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_NODE__STARTING_LINE = 0;

	/**
	 * The feature id for the '<em><b>Offset In Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_NODE__OFFSET_IN_STARTING_LINE = 1;

	/**
	 * The feature id for the '<em><b>Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_NODE__ENDING_LINE = 2;

	/**
	 * The feature id for the '<em><b>Offset In Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_NODE__OFFSET_IN_ENDING_LINE = 3;

	/**
	 * The number of structural features of the '<em>JSON Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_NODE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>JSON Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_NODE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.json.JSONValue <em>JSON Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.json.JSONValue
	 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONValue()
	 * @generated
	 */
	int JSON_VALUE = 1;

	/**
	 * The number of structural features of the '<em>JSON Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_VALUE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>JSON Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_VALUE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.json.impl.JSONStringImpl <em>JSON String</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.json.impl.JSONStringImpl
	 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONString()
	 * @generated
	 */
	int JSON_STRING = 2;

	/**
	 * The feature id for the '<em><b>Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_STRING__STARTING_LINE = JSON_NODE__STARTING_LINE;

	/**
	 * The feature id for the '<em><b>Offset In Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_STRING__OFFSET_IN_STARTING_LINE = JSON_NODE__OFFSET_IN_STARTING_LINE;

	/**
	 * The feature id for the '<em><b>Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_STRING__ENDING_LINE = JSON_NODE__ENDING_LINE;

	/**
	 * The feature id for the '<em><b>Offset In Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_STRING__OFFSET_IN_ENDING_LINE = JSON_NODE__OFFSET_IN_ENDING_LINE;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_STRING__LITERAL = JSON_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>JSON String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_STRING_FEATURE_COUNT = JSON_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>JSON String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_STRING_OPERATION_COUNT = JSON_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.json.impl.JSONNumberImpl <em>JSON Number</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.json.impl.JSONNumberImpl
	 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONNumber()
	 * @generated
	 */
	int JSON_NUMBER = 3;

	/**
	 * The feature id for the '<em><b>Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_NUMBER__STARTING_LINE = JSON_NODE__STARTING_LINE;

	/**
	 * The feature id for the '<em><b>Offset In Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_NUMBER__OFFSET_IN_STARTING_LINE = JSON_NODE__OFFSET_IN_STARTING_LINE;

	/**
	 * The feature id for the '<em><b>Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_NUMBER__ENDING_LINE = JSON_NODE__ENDING_LINE;

	/**
	 * The feature id for the '<em><b>Offset In Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_NUMBER__OFFSET_IN_ENDING_LINE = JSON_NODE__OFFSET_IN_ENDING_LINE;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_NUMBER__LITERAL = JSON_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>JSON Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_NUMBER_FEATURE_COUNT = JSON_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>JSON Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_NUMBER_OPERATION_COUNT = JSON_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.json.impl.JSONBoolImpl <em>JSON Bool</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.json.impl.JSONBoolImpl
	 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONBool()
	 * @generated
	 */
	int JSON_BOOL = 4;

	/**
	 * The feature id for the '<em><b>Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_BOOL__STARTING_LINE = JSON_NODE__STARTING_LINE;

	/**
	 * The feature id for the '<em><b>Offset In Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_BOOL__OFFSET_IN_STARTING_LINE = JSON_NODE__OFFSET_IN_STARTING_LINE;

	/**
	 * The feature id for the '<em><b>Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_BOOL__ENDING_LINE = JSON_NODE__ENDING_LINE;

	/**
	 * The feature id for the '<em><b>Offset In Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_BOOL__OFFSET_IN_ENDING_LINE = JSON_NODE__OFFSET_IN_ENDING_LINE;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_BOOL__LITERAL = JSON_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>JSON Bool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_BOOL_FEATURE_COUNT = JSON_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>JSON Bool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_BOOL_OPERATION_COUNT = JSON_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.json.impl.JSONObjectImpl <em>JSON Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.json.impl.JSONObjectImpl
	 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONObject()
	 * @generated
	 */
	int JSON_OBJECT = 5;

	/**
	 * The feature id for the '<em><b>Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_OBJECT__STARTING_LINE = JSON_NODE__STARTING_LINE;

	/**
	 * The feature id for the '<em><b>Offset In Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_OBJECT__OFFSET_IN_STARTING_LINE = JSON_NODE__OFFSET_IN_STARTING_LINE;

	/**
	 * The feature id for the '<em><b>Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_OBJECT__ENDING_LINE = JSON_NODE__ENDING_LINE;

	/**
	 * The feature id for the '<em><b>Offset In Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_OBJECT__OFFSET_IN_ENDING_LINE = JSON_NODE__OFFSET_IN_ENDING_LINE;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_OBJECT__FIELDS = JSON_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_OBJECT__ID = JSON_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_OBJECT__TYPE = JSON_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>JSON Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_OBJECT_FEATURE_COUNT = JSON_NODE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_OBJECT___GET__STRING = JSON_NODE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Set</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_OBJECT___SET__STRING_OBJECT = JSON_NODE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>JSON Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_OBJECT_OPERATION_COUNT = JSON_NODE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.json.impl.JSONFieldImpl <em>JSON Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.json.impl.JSONFieldImpl
	 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONField()
	 * @generated
	 */
	int JSON_FIELD = 6;

	/**
	 * The feature id for the '<em><b>Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_FIELD__STARTING_LINE = JSON_NODE__STARTING_LINE;

	/**
	 * The feature id for the '<em><b>Offset In Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_FIELD__OFFSET_IN_STARTING_LINE = JSON_NODE__OFFSET_IN_STARTING_LINE;

	/**
	 * The feature id for the '<em><b>Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_FIELD__ENDING_LINE = JSON_NODE__ENDING_LINE;

	/**
	 * The feature id for the '<em><b>Offset In Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_FIELD__OFFSET_IN_ENDING_LINE = JSON_NODE__OFFSET_IN_ENDING_LINE;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_FIELD__KEY = JSON_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_FIELD__VALUE = JSON_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>JSON Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_FIELD_FEATURE_COUNT = JSON_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>JSON Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_FIELD_OPERATION_COUNT = JSON_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.json.impl.JSONArrayImpl <em>JSON Array</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.json.impl.JSONArrayImpl
	 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONArray()
	 * @generated
	 */
	int JSON_ARRAY = 7;

	/**
	 * The feature id for the '<em><b>Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_ARRAY__STARTING_LINE = JSON_NODE__STARTING_LINE;

	/**
	 * The feature id for the '<em><b>Offset In Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_ARRAY__OFFSET_IN_STARTING_LINE = JSON_NODE__OFFSET_IN_STARTING_LINE;

	/**
	 * The feature id for the '<em><b>Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_ARRAY__ENDING_LINE = JSON_NODE__ENDING_LINE;

	/**
	 * The feature id for the '<em><b>Offset In Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_ARRAY__OFFSET_IN_ENDING_LINE = JSON_NODE__OFFSET_IN_ENDING_LINE;

	/**
	 * The feature id for the '<em><b>Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_ARRAY__VALUES = JSON_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>JSON Array</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_ARRAY_FEATURE_COUNT = JSON_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>JSON Array</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JSON_ARRAY_OPERATION_COUNT = JSON_NODE_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.json.JSONNode <em>JSON Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JSON Node</em>'.
	 * @see edu.ustb.sei.mde.json.JSONNode
	 * @generated
	 */
	EClass getJSONNode();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.json.JSONNode#getStartingLine <em>Starting Line</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Starting Line</em>'.
	 * @see edu.ustb.sei.mde.json.JSONNode#getStartingLine()
	 * @see #getJSONNode()
	 * @generated
	 */
	EAttribute getJSONNode_StartingLine();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.json.JSONNode#getOffsetInStartingLine <em>Offset In Starting Line</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset In Starting Line</em>'.
	 * @see edu.ustb.sei.mde.json.JSONNode#getOffsetInStartingLine()
	 * @see #getJSONNode()
	 * @generated
	 */
	EAttribute getJSONNode_OffsetInStartingLine();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.json.JSONNode#getEndingLine <em>Ending Line</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ending Line</em>'.
	 * @see edu.ustb.sei.mde.json.JSONNode#getEndingLine()
	 * @see #getJSONNode()
	 * @generated
	 */
	EAttribute getJSONNode_EndingLine();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.json.JSONNode#getOffsetInEndingLine <em>Offset In Ending Line</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset In Ending Line</em>'.
	 * @see edu.ustb.sei.mde.json.JSONNode#getOffsetInEndingLine()
	 * @see #getJSONNode()
	 * @generated
	 */
	EAttribute getJSONNode_OffsetInEndingLine();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.json.JSONValue <em>JSON Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JSON Value</em>'.
	 * @see edu.ustb.sei.mde.json.JSONValue
	 * @generated
	 */
	EClass getJSONValue();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.json.JSONString <em>JSON String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JSON String</em>'.
	 * @see edu.ustb.sei.mde.json.JSONString
	 * @generated
	 */
	EClass getJSONString();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.json.JSONString#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Literal</em>'.
	 * @see edu.ustb.sei.mde.json.JSONString#getLiteral()
	 * @see #getJSONString()
	 * @generated
	 */
	EAttribute getJSONString_Literal();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.json.JSONNumber <em>JSON Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JSON Number</em>'.
	 * @see edu.ustb.sei.mde.json.JSONNumber
	 * @generated
	 */
	EClass getJSONNumber();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.json.JSONNumber#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Literal</em>'.
	 * @see edu.ustb.sei.mde.json.JSONNumber#getLiteral()
	 * @see #getJSONNumber()
	 * @generated
	 */
	EAttribute getJSONNumber_Literal();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.json.JSONBool <em>JSON Bool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JSON Bool</em>'.
	 * @see edu.ustb.sei.mde.json.JSONBool
	 * @generated
	 */
	EClass getJSONBool();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.json.JSONBool#isLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Literal</em>'.
	 * @see edu.ustb.sei.mde.json.JSONBool#isLiteral()
	 * @see #getJSONBool()
	 * @generated
	 */
	EAttribute getJSONBool_Literal();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.json.JSONObject <em>JSON Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JSON Object</em>'.
	 * @see edu.ustb.sei.mde.json.JSONObject
	 * @generated
	 */
	EClass getJSONObject();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.json.JSONObject#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fields</em>'.
	 * @see edu.ustb.sei.mde.json.JSONObject#getFields()
	 * @see #getJSONObject()
	 * @generated
	 */
	EReference getJSONObject_Fields();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.json.JSONObject#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see edu.ustb.sei.mde.json.JSONObject#getId()
	 * @see #getJSONObject()
	 * @generated
	 */
	EAttribute getJSONObject_Id();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.json.JSONObject#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see edu.ustb.sei.mde.json.JSONObject#getType()
	 * @see #getJSONObject()
	 * @generated
	 */
	EAttribute getJSONObject_Type();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.mde.json.JSONObject#get(java.lang.String) <em>Get</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get</em>' operation.
	 * @see edu.ustb.sei.mde.json.JSONObject#get(java.lang.String)
	 * @generated
	 */
	EOperation getJSONObject__Get__String();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.mde.json.JSONObject#set(java.lang.String, java.lang.Object) <em>Set</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set</em>' operation.
	 * @see edu.ustb.sei.mde.json.JSONObject#set(java.lang.String, java.lang.Object)
	 * @generated
	 */
	EOperation getJSONObject__Set__String_Object();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.json.JSONField <em>JSON Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JSON Field</em>'.
	 * @see edu.ustb.sei.mde.json.JSONField
	 * @generated
	 */
	EClass getJSONField();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.json.JSONField#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see edu.ustb.sei.mde.json.JSONField#getKey()
	 * @see #getJSONField()
	 * @generated
	 */
	EAttribute getJSONField_Key();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.json.JSONField#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see edu.ustb.sei.mde.json.JSONField#getValue()
	 * @see #getJSONField()
	 * @generated
	 */
	EReference getJSONField_Value();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.json.JSONArray <em>JSON Array</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JSON Array</em>'.
	 * @see edu.ustb.sei.mde.json.JSONArray
	 * @generated
	 */
	EClass getJSONArray();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.json.JSONArray#getValues <em>Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Values</em>'.
	 * @see edu.ustb.sei.mde.json.JSONArray#getValues()
	 * @see #getJSONArray()
	 * @generated
	 */
	EReference getJSONArray_Values();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	JsonFactory getJsonFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.json.impl.JSONNodeImpl <em>JSON Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.json.impl.JSONNodeImpl
		 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONNode()
		 * @generated
		 */
		EClass JSON_NODE = eINSTANCE.getJSONNode();

		/**
		 * The meta object literal for the '<em><b>Starting Line</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JSON_NODE__STARTING_LINE = eINSTANCE.getJSONNode_StartingLine();

		/**
		 * The meta object literal for the '<em><b>Offset In Starting Line</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JSON_NODE__OFFSET_IN_STARTING_LINE = eINSTANCE.getJSONNode_OffsetInStartingLine();

		/**
		 * The meta object literal for the '<em><b>Ending Line</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JSON_NODE__ENDING_LINE = eINSTANCE.getJSONNode_EndingLine();

		/**
		 * The meta object literal for the '<em><b>Offset In Ending Line</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JSON_NODE__OFFSET_IN_ENDING_LINE = eINSTANCE.getJSONNode_OffsetInEndingLine();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.json.JSONValue <em>JSON Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.json.JSONValue
		 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONValue()
		 * @generated
		 */
		EClass JSON_VALUE = eINSTANCE.getJSONValue();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.json.impl.JSONStringImpl <em>JSON String</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.json.impl.JSONStringImpl
		 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONString()
		 * @generated
		 */
		EClass JSON_STRING = eINSTANCE.getJSONString();

		/**
		 * The meta object literal for the '<em><b>Literal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JSON_STRING__LITERAL = eINSTANCE.getJSONString_Literal();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.json.impl.JSONNumberImpl <em>JSON Number</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.json.impl.JSONNumberImpl
		 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONNumber()
		 * @generated
		 */
		EClass JSON_NUMBER = eINSTANCE.getJSONNumber();

		/**
		 * The meta object literal for the '<em><b>Literal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JSON_NUMBER__LITERAL = eINSTANCE.getJSONNumber_Literal();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.json.impl.JSONBoolImpl <em>JSON Bool</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.json.impl.JSONBoolImpl
		 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONBool()
		 * @generated
		 */
		EClass JSON_BOOL = eINSTANCE.getJSONBool();

		/**
		 * The meta object literal for the '<em><b>Literal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JSON_BOOL__LITERAL = eINSTANCE.getJSONBool_Literal();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.json.impl.JSONObjectImpl <em>JSON Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.json.impl.JSONObjectImpl
		 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONObject()
		 * @generated
		 */
		EClass JSON_OBJECT = eINSTANCE.getJSONObject();

		/**
		 * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JSON_OBJECT__FIELDS = eINSTANCE.getJSONObject_Fields();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JSON_OBJECT__ID = eINSTANCE.getJSONObject_Id();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JSON_OBJECT__TYPE = eINSTANCE.getJSONObject_Type();

		/**
		 * The meta object literal for the '<em><b>Get</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation JSON_OBJECT___GET__STRING = eINSTANCE.getJSONObject__Get__String();

		/**
		 * The meta object literal for the '<em><b>Set</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation JSON_OBJECT___SET__STRING_OBJECT = eINSTANCE.getJSONObject__Set__String_Object();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.json.impl.JSONFieldImpl <em>JSON Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.json.impl.JSONFieldImpl
		 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONField()
		 * @generated
		 */
		EClass JSON_FIELD = eINSTANCE.getJSONField();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JSON_FIELD__KEY = eINSTANCE.getJSONField_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JSON_FIELD__VALUE = eINSTANCE.getJSONField_Value();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.json.impl.JSONArrayImpl <em>JSON Array</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.json.impl.JSONArrayImpl
		 * @see edu.ustb.sei.mde.json.impl.JsonPackageImpl#getJSONArray()
		 * @generated
		 */
		EClass JSON_ARRAY = eINSTANCE.getJSONArray();

		/**
		 * The meta object literal for the '<em><b>Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JSON_ARRAY__VALUES = eINSTANCE.getJSONArray_Values();

	}

} //JsonPackage
