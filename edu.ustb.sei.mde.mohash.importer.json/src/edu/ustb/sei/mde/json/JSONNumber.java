/**
 */
package edu.ustb.sei.mde.json;

import java.math.BigDecimal;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JSON Number</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.json.JSONNumber#getLiteral <em>Literal</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONNumber()
 * @model
 * @generated
 */
public interface JSONNumber extends JSONNode, JSONValue {
	/**
	 * Returns the value of the '<em><b>Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literal</em>' attribute.
	 * @see #setLiteral(BigDecimal)
	 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONNumber_Literal()
	 * @model
	 * @generated
	 */
	BigDecimal getLiteral();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.json.JSONNumber#getLiteral <em>Literal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Literal</em>' attribute.
	 * @see #getLiteral()
	 * @generated
	 */
	void setLiteral(BigDecimal value);

} // JSONNumber
