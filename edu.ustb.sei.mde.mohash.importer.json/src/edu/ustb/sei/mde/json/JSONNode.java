/**
 */
package edu.ustb.sei.mde.json;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JSON Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.json.JSONNode#getStartingLine <em>Starting Line</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.json.JSONNode#getOffsetInStartingLine <em>Offset In Starting Line</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.json.JSONNode#getEndingLine <em>Ending Line</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.json.JSONNode#getOffsetInEndingLine <em>Offset In Ending Line</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONNode()
 * @model abstract="true"
 * @generated
 */
public interface JSONNode extends EObject {
	/**
	 * Returns the value of the '<em><b>Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Starting Line</em>' attribute.
	 * @see #setStartingLine(int)
	 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONNode_StartingLine()
	 * @model
	 * @generated
	 */
	int getStartingLine();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.json.JSONNode#getStartingLine <em>Starting Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Starting Line</em>' attribute.
	 * @see #getStartingLine()
	 * @generated
	 */
	void setStartingLine(int value);

	/**
	 * Returns the value of the '<em><b>Offset In Starting Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset In Starting Line</em>' attribute.
	 * @see #setOffsetInStartingLine(int)
	 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONNode_OffsetInStartingLine()
	 * @model
	 * @generated
	 */
	int getOffsetInStartingLine();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.json.JSONNode#getOffsetInStartingLine <em>Offset In Starting Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset In Starting Line</em>' attribute.
	 * @see #getOffsetInStartingLine()
	 * @generated
	 */
	void setOffsetInStartingLine(int value);

	/**
	 * Returns the value of the '<em><b>Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ending Line</em>' attribute.
	 * @see #setEndingLine(int)
	 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONNode_EndingLine()
	 * @model
	 * @generated
	 */
	int getEndingLine();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.json.JSONNode#getEndingLine <em>Ending Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ending Line</em>' attribute.
	 * @see #getEndingLine()
	 * @generated
	 */
	void setEndingLine(int value);

	/**
	 * Returns the value of the '<em><b>Offset In Ending Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset In Ending Line</em>' attribute.
	 * @see #setOffsetInEndingLine(int)
	 * @see edu.ustb.sei.mde.json.JsonPackage#getJSONNode_OffsetInEndingLine()
	 * @model
	 * @generated
	 */
	int getOffsetInEndingLine();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.json.JSONNode#getOffsetInEndingLine <em>Offset In Ending Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset In Ending Line</em>' attribute.
	 * @see #getOffsetInEndingLine()
	 * @generated
	 */
	void setOffsetInEndingLine(int value);

} // JSONNode
