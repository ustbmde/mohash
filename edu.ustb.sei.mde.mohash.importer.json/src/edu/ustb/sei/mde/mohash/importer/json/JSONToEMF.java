package edu.ustb.sei.mde.mohash.importer.json;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CodePointBuffer;
import org.antlr.v4.runtime.CodePointCharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.json.JSONArray;
import edu.ustb.sei.mde.json.JSONBool;
import edu.ustb.sei.mde.json.JSONField;
import edu.ustb.sei.mde.json.JSONNode;
import edu.ustb.sei.mde.json.JSONNumber;
import edu.ustb.sei.mde.json.JSONObject;
import edu.ustb.sei.mde.json.JSONString;
import edu.ustb.sei.mde.json.JSONValue;
import edu.ustb.sei.mde.json.parser.JSONLexer;
import edu.ustb.sei.mde.json.parser.JSONParser;
import edu.ustb.sei.mde.json.parser.JSONParser.ArrayContext;
import edu.ustb.sei.mde.json.parser.JSONParser.Bound;
import edu.ustb.sei.mde.json.parser.JSONParser.ObjectContext;
import edu.ustb.sei.mde.json.parser.JSONParser.PairContext;
import edu.ustb.sei.mde.json.parser.JSONParser.ValueContext;

public class JSONToEMF {
    private JSONCreationFactory factory;
    private String idField;
    private String typeField;

    public JSONToEMF(String idField, String typeField) {
        this.idField = idField;
        this.typeField = typeField;
        
        factory = new JSONCreationFactory();
    }

    public static void main(String[] args) {
        JSONToEMF c = new JSONToEMF("id","type");
        String s = "[\n {\"id\":\"111\", \"a\":1},\n {\"id\":\"222\", \"b\":true}]";
        
        

        EObject o;
		try {
			File f = new File("/Volumes/Macintosh HD Data/Eclipse Projects/git/mohash/edu.ustb.sei.mde.mohash.importer.json/test.json");
			if(f.exists()) {
				o = c.parseArray(f.getAbsolutePath());
				c.factory.export(o);				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public EObject parseObject(String filePath) throws IOException {
    	Path path = Paths.get(filePath);
    	byte[] bytes = java.nio.file.Files.readAllBytes(path);
    	return parseObject(bytes);
    }
    
    public EObject parseArray(String filePath) throws IOException {
    	Path path = Paths.get(filePath);
    	byte[] bytes = java.nio.file.Files.readAllBytes(path);
    	return parseArray(bytes);
    }
    
    public EObject parseObject(byte[] jsonBytes) {
    	CharStream input = CodePointCharStream.fromBuffer(CodePointBuffer.withBytes(ByteBuffer.wrap(jsonBytes)));
        JSONLexer lexer = new JSONLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        JSONParser parser = new JSONParser(tokens);
        
        ObjectContext object = parser.object();

        return convertObject(object);
    }

    public EObject parseArray(byte[] jsonBytes) {
        CharStream input = CodePointCharStream.fromBuffer(CodePointBuffer.withBytes(ByteBuffer.wrap(jsonBytes)));
        JSONLexer lexer = new JSONLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        JSONParser parser = new JSONParser(tokens);
        
        ArrayContext array = parser.array();

        return convertArray(array);
    }

    @SuppressWarnings("all")
    private JSONArray convertArray(ArrayContext j) {
        JSONArray o = factory.createArray();
        List<JSONValue> values = j.value().stream().map(p->convertValue(p)).collect(Collectors.toList());
        o.getValues().addAll(values);
        
        initBound(j, o);
        
        return o;
    }

	protected void initBound(ParserRuleContext j, JSONNode o) {
		Bound b = Bound.make(j);
        o.setStartingLine(b.start.line);
        o.setOffsetInStartingLine(b.start.offsetInLine);
        o.setEndingLine(b.end.line);
        o.setOffsetInEndingLine(b.end.offsetInLine);
	}


    @SuppressWarnings("all")
    private JSONObject convertObject(ObjectContext j) {
        List<JSONField> fields = j.pair().stream().map(p->convertField(p)).collect(Collectors.toList());
        Optional<JSONField> id = fields.stream().filter(p->idField.equals(p.getKey())).findFirst();
        Optional<JSONField> type = fields.stream().filter(p->typeField.equals(p.getKey())).findFirst();
        
        JSONObject o = factory.createObject(
            id.isPresent() ? ((JSONString) id.get().getValue()).getLiteral() : null, 
            type.isPresent() ? ((JSONString) type.get().getValue()).getLiteral() : null);
        
        o.getFields().addAll(fields);
        
        initBound(j, o);
        return o;
    }

    private String extractString(String STRING) {
        return STRING.substring(1, STRING.length() -1);
    }

    private JSONField convertField(PairContext j) {
    	JSONField o = factory.createField(extractString(j.STRING().getText()), convertValue(j.value()));
    	initBound(j, o);
    	return o;
    }

    private JSONValue convertValue(ValueContext j) {
        if(j.STRING() != null) {
            JSONString o = factory.createString(extractString(j.STRING().getText()));
            initBound(j, o);
            return o;
        } else if(j.NUMBER() != null) {
            JSONNumber o = factory.createNumber(j.NUMBER().getText());
            initBound(j, o);
            return o;
        } else if(j.array() != null) {
            return convertArray(j.array());
        } else if(j.object() != null) {
            return convertObject(j.object());
        } else {
            String text = j.getText();
            if("true".equals(text)) {
                JSONBool o = factory.createBool(true);
                initBound(j, o);
                return o;
            } else if("false".equals(text)) {
                JSONBool o = factory.createBool(false);
                initBound(j, o);
                return o;
            } else {
                return null;
            }
        }
    }
}
