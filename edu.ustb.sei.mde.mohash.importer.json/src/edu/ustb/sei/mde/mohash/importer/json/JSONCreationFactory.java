package edu.ustb.sei.mde.mohash.importer.json;

import java.io.IOException;
import java.math.BigDecimal;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import edu.ustb.sei.mde.json.JSONArray;
import edu.ustb.sei.mde.json.JSONBool;
import edu.ustb.sei.mde.json.JSONField;
import edu.ustb.sei.mde.json.JSONNumber;
import edu.ustb.sei.mde.json.JSONObject;
import edu.ustb.sei.mde.json.JSONString;
import edu.ustb.sei.mde.json.JSONValue;
import edu.ustb.sei.mde.json.JsonFactory;
import edu.ustb.sei.mde.json.JsonPackage;

public class JSONCreationFactory {

    private ResourceSet set;
    
    public JSONCreationFactory() {
        set = new ResourceSetImpl();
        set.getPackageRegistry().put(EcorePackage.eINSTANCE.getNsURI(), EcorePackage.eINSTANCE);
        set.getPackageRegistry().put(JsonPackage.eINSTANCE.getNsURI(), JsonPackage.eINSTANCE);
        set.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new XMIResourceFactoryImpl());
    }

    public void export(EObject root) {
        Resource res = set.createResource(URI.createURI("temp://temp.xmi"));
        res.getContents().add(root);
        try {
            res.save(System.out, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public JSONObject createObject(String id, String type) {
        JSONObject create = JsonFactory.eINSTANCE.createJSONObject();
        create.setId(id);
        create.setType(type);
        return create;
    }

    public JSONArray createArray() {
        return JsonFactory.eINSTANCE.createJSONArray();
    }

    public JSONString createString(String value) {
        JSONString o = JsonFactory.eINSTANCE.createJSONString();
        o.setLiteral(value);
        return o;
    }

    public JSONNumber createNumber(String value) {
    	JSONNumber o = JsonFactory.eINSTANCE.createJSONNumber();
        o.setLiteral(new BigDecimal(value));
        return o;
    }

    public JSONBool createBool(boolean value) {
        JSONBool o = JsonFactory.eINSTANCE.createJSONBool();
        o.setLiteral(value);
        return o;
    }
    
    public JSONField createField(String key, JSONValue value) {
    	JSONField f = JsonFactory.eINSTANCE.createJSONField();
    	f.setKey(key);
    	f.setValue(value);
    	return f;
    }
}
