package edu.ustb.sei.mde.mohash.importer.json

import edu.ustb.sei.mde.json.JSONNode
import edu.ustb.sei.mde.json.JSONValue
import edu.ustb.sei.mde.json.JsonPackage
import edu.ustb.sei.mde.mohash.TypeMap
import edu.ustb.sei.mde.mohash.interfaces.IImporter
import java.util.regex.Pattern
import org.eclipse.emf.compare.match.eobject.DefaultWeightProvider
import org.eclipse.emf.compare.match.eobject.WeightProviderDescriptorRegistryImpl
import org.eclipse.emf.compare.match.eobject.internal.WeightProviderDescriptorImpl
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.emf.common.util.URI

class JsonImporter implements IImporter {
	
	override getThresholds() {
		val thresholds = new TypeMap<Double>(0.5)
		thresholds.put(EcorePackage.eINSTANCE.EPackage, 0.15)
		thresholds.put(EcorePackage.eINSTANCE.EClass, 0.55)
		thresholds.put(EcorePackage.eINSTANCE.EReference, 0.65)
		thresholds.put(EcorePackage.eINSTANCE.EOperation, 0.55)
		thresholds.put(EcorePackage.eINSTANCE.EAttribute, 0.68)
		thresholds.put(EcorePackage.eINSTANCE.EStringToStringMapEntry, 0.4)
		thresholds.put(EcorePackage.eINSTANCE.EEnum, 0.5)
		thresholds.put(EcorePackage.eINSTANCE.EEnumLiteral, 0.45)
		thresholds.put(EcorePackage.eINSTANCE.EParameter, 0.5)
		
		thresholds.put(JsonPackage.eINSTANCE.JSONObject, 0.7)
		thresholds
	}
	
	override initResourceSet(ResourceSet set) {
		set.packageRegistry.put(EcorePackage.eINSTANCE.nsURI, EcorePackage.eINSTANCE)
		set.packageRegistry.put(JsonPackage.eINSTANCE.nsURI, JsonPackage.eINSTANCE)
		set.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
			Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
	}
	
	@SuppressWarnings("all")
	override getWeightRegistry() {
		val JsonWeightProvider jsonWeightProvider = new JsonWeightProvider();
		val jwpDescriptor = new WeightProviderDescriptorImpl(
				jsonWeightProvider, 101, Pattern.compile("http://www.ustb.edu.cn/sei/mde/json"));
		
		val reg = WeightProviderDescriptorRegistryImpl.createStandaloneInstance();
		reg.put(jsonWeightProvider.getClass().name, jwpDescriptor)
		
		return reg
	}
	
	override unhashableTypes() {
		#[EcorePackage.eINSTANCE.EAnnotation, 
			EcorePackage.eINSTANCE.EGenericType, 
			EcorePackage.eINSTANCE.EFactory, 
			EcorePackage.eINSTANCE.EStringToStringMapEntry,
			// we only try to hash json objects
			JsonPackage.eINSTANCE.JSONArray,
			JsonPackage.eINSTANCE.JSONField,
			JsonPackage.eINSTANCE.JSONString,
			JsonPackage.eINSTANCE.JSONNumber,
			JsonPackage.eINSTANCE.JSONBool]
	}
	
	override loadFromFile(ResourceSet resourceSet, String filePath) {
		val j2e = new JSONToEMF("id", "type")
		
		val arr = j2e.parseArray(filePath)
		
		val res = resourceSet.createResource(URI.createFileURI(filePath+'.xmi'))
		
		res.contents += arr
		
		res
	}
	
}

class JsonWeightProvider extends DefaultWeightProvider {
	/**
	 * {@inheritDoc}
	 */
	override getWeight(EStructuralFeature feature) {
		if (irrelevant(feature)) {
			return 0;
		}
		
		var Integer found = weights.get(feature)
		
		if(found === null) {			
			if(feature === JsonPackage.Literals.JSON_OBJECT__ID 
				|| feature === JsonPackage.Literals.JSON_OBJECT__TYPE
				|| feature === JsonPackage.Literals.JSON_FIELD__KEY
			)
				found = Integer.valueOf(UNLIKELY_TO_MATCH)
			else if(feature === JsonPackage.Literals.JSON_FIELD__VALUE
				|| feature === JsonPackage.Literals.JSON_OBJECT__FIELDS
				|| feature === JsonPackage.Literals.JSON_NODE__STARTING_LINE
				|| feature === JsonPackage.Literals.JSON_NODE__ENDING_LINE
				|| feature === JsonPackage.Literals.JSON_NODE__OFFSET_IN_STARTING_LINE
				|| feature === JsonPackage.Literals.JSON_NODE__OFFSET_IN_ENDING_LINE
				
			)
				found = 0
			else {
				found = super.getWeight(feature)
			}
	
			weights.put(feature, found);
		}
		
		return found.intValue();
	}

	/**
	 * {@inheritDoc}
	 */
	override getParentWeight(EObject a) {
		var int parentWeight = 0;
		if (a instanceof JSONValue || a instanceof JSONNode) {
			parentWeight = UNLIKELY_TO_MATCH;
		} else {
			parentWeight = SIGNIFICANT;
		}
		return parentWeight;
	}

	/**
	 * {@inheritDoc}
	 */
	override getContainingFeatureWeight(EObject a) {
		return SIGNIFICANT;
	}
}