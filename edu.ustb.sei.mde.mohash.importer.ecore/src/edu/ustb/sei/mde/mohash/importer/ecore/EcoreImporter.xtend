package edu.ustb.sei.mde.mohash.importer.ecore

import edu.ustb.sei.mde.mohash.interfaces.IImporter
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.emf.compare.match.eobject.WeightProviderDescriptorRegistryImpl
import edu.ustb.sei.mde.mohash.TypeMap
import org.eclipse.emf.ecore.EcorePackage

class EcoreImporter implements IImporter {
	
	override getThresholds() {
		val thresholds = new TypeMap<Double>(0.5)
		thresholds.put(EcorePackage.eINSTANCE.EPackage, 0.15)
		thresholds.put(EcorePackage.eINSTANCE.EClass, 0.55)
		thresholds.put(EcorePackage.eINSTANCE.EReference, 0.65)
		thresholds.put(EcorePackage.eINSTANCE.EOperation, 0.55)
		thresholds.put(EcorePackage.eINSTANCE.EAttribute, 0.68)
		thresholds.put(EcorePackage.eINSTANCE.EStringToStringMapEntry, 0.4)
		thresholds.put(EcorePackage.eINSTANCE.EEnum, 0.5)
		thresholds.put(EcorePackage.eINSTANCE.EEnumLiteral, 0.45)
		thresholds.put(EcorePackage.eINSTANCE.EParameter, 0.5)
		thresholds
	}
	
	override initResourceSet(ResourceSet set) {
		set.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
			Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
	}
	
	override getWeightRegistry() {
		WeightProviderDescriptorRegistryImpl.createStandaloneInstance()
	}
	
	override unhashableTypes() {
		#[EcorePackage.eINSTANCE.EAnnotation, 
			EcorePackage.eINSTANCE.EGenericType, 
			EcorePackage.eINSTANCE.EFactory, 
			EcorePackage.eINSTANCE.EStringToStringMapEntry]
	}
	
}