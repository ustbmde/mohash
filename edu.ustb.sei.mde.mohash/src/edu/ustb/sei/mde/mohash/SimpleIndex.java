package edu.ustb.sei.mde.mohash;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;

import org.eclipse.emf.compare.match.eobject.EObjectIndex.Side;
import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.ImmutableSet;

/**
 * A simple index that ignores hash codes.
 * @author dr-he
 *
 */
public class SimpleIndex implements ObjectIndex {
	private Set<EObject> allObjects = new LinkedHashSet<>();
	private List<EObject> shadowCopy = null;
	
	// For debugging
	@Override
	public int getPosition(long hash, EObject selected, Iterable<EObject> candidates) {
		return allObjects.size();
	}
	// End

	@Override
	final public Iterable<EObject> query(EObject target, EObject containerMatch, long hashCode, double minSim,
			double containerDiff) {
		if(containerMatch!=null) return filterByContainer(allObjects, containerMatch);
		else return allObjects;
	}

	@Override
	final public void index(EObject object, long hashCode) {
		shadowCopy = null;
		allObjects.add(object);
	}

	@Override
	final public Long remove(EObject object) {
		allObjects.remove(object);
		return 0L;
	}

	@Override
	final public Long getHash(EObject object) {
		return 0L;
	}

	@Override
	final public Iterable<EObject> getRemainingObjects() {
		if(shadowCopy==null || shadowCopy.size() != allObjects.size())
			shadowCopy = new ArrayList<>(allObjects);
		return shadowCopy;
	}

	@Override
	public void printHashCodes(BiFunction<EObject, Long, String> function) {
		allObjects.forEach(o->{
			System.out.println(function.apply(o, 0L)); 
		});
	}

	@Override
	public Iterable<EObject> getValuesToMatchAhead(Side side) {
		if(allObjects.size() > SEARCH_WINDOW) {
			return getRemainingObjects();
		}
		return Collections.emptyList();
	}

}
