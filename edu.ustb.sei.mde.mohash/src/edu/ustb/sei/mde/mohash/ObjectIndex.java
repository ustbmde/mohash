package edu.ustb.sei.mde.mohash;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;

import org.eclipse.emf.compare.match.DefaultMatchEngine;
import org.eclipse.emf.compare.match.eobject.internal.MatchAheadOfTime;
import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.Sets;

import edu.ustb.sei.mde.mohash.emfcompare.HashBasedEObjectIndex;
import edu.ustb.sei.mde.mohash.emfcompare.SimHashProximityEObjectMatcher;
import edu.ustb.sei.mde.mohash.factory.MoHashMatchEngineFactory;
import edu.ustb.sei.mde.mohash.functions.Hash64;

/**
 * ObjectIndex is the internal index interface used by HashBasedEObjectIndex.
 * HashBasedEObjectIndex will delegate some functionalities to ObjectIndex.
 * @author dr-he
 *
 */
@SuppressWarnings("restriction")
public interface ObjectIndex extends MatchAheadOfTime {
	
	// For debugging
	default public int getPosition(long hash, EObject selected, Iterable<EObject> candidates) {return -1;}
	// End
	
	/**
	 * This constant is defined according to {@link org.eclipse.emf.compare.match.eobject.internal.ProximityIndex}
	 */
	int SEARCH_WINDOW = 1000;
	
	/**
	 * The method is used to retrieve elements by hash code.
	 * @param target is the element to be matched.
	 * @param containerMatch is the container match. It can be null for identical match.
	 * @param hashCode is the hash code of {@code target}.
	 * @param minSim is the threshold for similarity.
	 * @param containerDiff is the penalty for the mismatch of the containers.
	 * @return a collection of candidates
	 */
	Iterable<EObject> query(EObject target, EObject containerMatch, long hashCode, double minSim, double containerDiff);

	/**
	 * Add {@code object} to the index.
	 * @param object
	 * @param hashCode
	 */
	void index(EObject object, long hashCode);

	/**
	 * Remove {@code object} from the index. Ideally, when {@code} is useless,
	 * it is removed from the index. But we do not ensure this.
	 */
	Long remove(EObject object);
	
	/**
	 * Get the hash code of {@code object}.
	 * @param object
	 * @return the hash code, or null if {@code object} is not in the index.
	 */
	Long getHash(EObject object);
	
	/**
	 * Get all the objects in the index.
	 * @return
	 */
	Iterable<EObject> getRemainingObjects();

	/**
	 * Print all hash codes in the index.
	 * @param function
	 */
	void printHashCodes(BiFunction<EObject, Long,String> function);
	
	/**
	 * This is a helper method that obtains the HashBasedEObjectIndex of an MatchEngine.
	 * @param factory
	 * @return
	 */
	static public HashBasedEObjectIndex getObjectIndex(MoHashMatchEngineFactory factory) {
		try {
			DefaultMatchEngine engine = (DefaultMatchEngine) factory.getMatchEngine();
			Method method = engine.getClass().getDeclaredMethod("getEObjectMatcher");
			method.setAccessible(true);
			SimHashProximityEObjectMatcher matcher = (SimHashProximityEObjectMatcher) method.invoke(engine);
			HashBasedEObjectIndex index = (HashBasedEObjectIndex) matcher.getIndex();
			return index;
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Compute the similarity of two hash codes.
	 * @param left
	 * @param right
	 * @return
	 */
	static public double similarity(HashValue64 left, HashValue64 right) {
		return Hash64.cosineSimilarity(left, right);
	}
	
	default Iterable<EObject> filterByContainer(Set<EObject> candidates, EObject container) {
		if(candidates.size() < 5) return candidates;
		
		Set<EObject> children = new LinkedHashSet<>(container.eContents());
		
		if(candidates.size() <= children.size()) {
			return Sets.filter(candidates, e -> children.contains(e));
		} else {
			children.retainAll(candidates);
			return children;
		}
	}
}