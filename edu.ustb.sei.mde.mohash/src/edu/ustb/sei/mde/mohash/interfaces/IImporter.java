package edu.ustb.sei.mde.mohash.interfaces;

import java.io.File;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.match.eobject.WeightProvider.Descriptor.Registry;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import edu.ustb.sei.mde.mohash.TypeMap;

public interface IImporter {
	void initResourceSet(ResourceSet set);
	
	Registry getWeightRegistry();
	
	TypeMap<Double> getThresholds();
	
	List<EClass> unhashableTypes();
	
	default Resource loadFromFile(ResourceSet resourceSet, String filePath) {
		URI uri = URI.createFileURI((new File(filePath)).getAbsolutePath());
		return resourceSet.getResource(uri, true);
	}
}
