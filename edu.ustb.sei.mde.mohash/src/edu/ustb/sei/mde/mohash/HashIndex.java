package edu.ustb.sei.mde.mohash;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiFunction;

import org.eclipse.emf.compare.match.eobject.EObjectIndex.Side;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mohash.functions.Hash64;

/**
 * HashIndex maps elements and their hashes bidirectionally. It uses an helper 
 * structure, namely HashValue64, to preserve more information.
 * @author dr-he
 *
 */
public class HashIndex implements ObjectIndex {
	protected Map<EObject, HashValue64> obj2codeMap = new LinkedHashMap<>();
	protected Map<Long, Set<EObject>> code2objMap = new LinkedHashMap<>();
	
	// For debugging
	public int getPosition(long anchor, EObject selected, Iterable<EObject> candidates) {
		List<Double> ahead = new ArrayList<>();
		HashValue64 anchorHash = new HashValue64(anchor);
		HashValue64 selectedHash = obj2codeMap.get(selected);

		double baseSim = Hash64.cosineSimilarity(anchorHash, selectedHash);

		for (EObject cand : candidates) {
			HashValue64 candHash = obj2codeMap.get(cand);
			double candSim = Hash64.cosineSimilarity(anchorHash, candHash);
			if (baseSim <= candSim)
				ahead.add(candSim);
		}

		return ahead.size();
	}
	// End
	
	@Override
	public Iterable<EObject> query(EObject target, EObject containerMatch, long hashCode, double minSim, double containerDiff) {
		if(minSim==1 || hashCode==0) {
			Set<EObject> o = code2objMap.getOrDefault(hashCode, Collections.emptySet());
			if(containerMatch!=null) return filterByContainer(o, containerMatch);
			else return o;
		}
		
		HashValue64 hv = new HashValue64(hashCode);
		
		// ==================VARIATION POINT BEGIN===========================
		// VARIANT 1: the following code ensures the order of comparison
		// If we want to be consistent with EMF Compare, we should execute it		
		LinkedList<EObject> result = new LinkedList<>();
		for(Entry<EObject, HashValue64> entry : obj2codeMap.entrySet()) {
			HashValue64 value = entry.getValue();
			double containerSim = containerDiff;
			if(containerMatch==entry.getKey().eContainer()) {
				containerSim = 0;
			}
			double sim = value.code==hashCode ? 1.0 : ObjectIndex.similarity(value, hv);
			if(sim >= (minSim+containerSim)) { // Similarity propagation
				result.add(entry.getKey());
			}
		}
		/*
		 * A potential improvement here is to add more elements (top-K) when the size 
		 * of result is less than a certain threshold. The reason is as follows.
		 * For some cases, a mutant of an element is significantly changed but 
		 * the mutant cannot match other elements. To improve the matching result,
		 * we should enlarge the search scope. However, this will be inconsistent
		 * with EMF Compare.
		 */
		return result;
		// ===============================================================
		// VARIANT 2: the following code may change the order of comparison
		// However, the following code may actually improve the result of the match
		// If we want to be consistent with EMF Compare, we should not execute it
//		SortedSet<DiffPair> result = new TreeSet<>(DiffPair::compare);
//		for(Entry<EObject, Long> entry : obj2codeMap.entrySet()) {
//			Long value = entry.getValue();
//			if(value==hashCode) {
//				result.add(new DiffPair(1.0, entry.getKey()));
//			} else {
//				double jaccardSimilarity = Hash64.jaccardSimilarity(value, hashCode);
//				if(jaccardSimilarity>=minSim) {
//					result.add(new DiffPair(jaccardSimilarity, entry.getKey()));
//				}
//			}
//		}
//		return Iterables.transform(result, DiffPair::getEObject);
		// ==================VARIATION POINT END==========================
		
		
		// ====================UNSUCCESSFUL ATTEMPT==========================
		/*
		 *  NOTE 2023/1/28: the following code does not work as expected.
		 *  In most cases, elements of the same type are very similar in terms of
		 *  the numbers of 1-bits. It would be very ineffective to filter elements
		 *  by using bit counts. Any other filtering mechanisms related to bit 
		 *  counts are also ineffective.
		 */
//		int minBits = Math.max(0, (int) Math.round(hv.bitCount * minSim - 0.5));
//		int maxBits = Math.min(64, (int) Math.round(hv.bitCount / minSim + 0.5));
//		LinkedList<EObject> result = new LinkedList<>();
//		for(int i = minBits ; i<=maxBits; i++) {
//			Map<EObject, HashValue64> map = bitCountIndex[i];
//			for(Entry<EObject, HashValue64> entry : map.entrySet()) {
//				HashValue64 value = entry.getValue();
//				if(value.code==hashCode || ObjectIndex.similarity(value, hv)>=minSim) {
//					result.add(entry.getKey());
//				}
//			}
//		}
//		return result;
		// ===============================================================
	}
	
	@Override
	public void index(EObject object, long hashCode) {
		HashValue64 hv = new HashValue64(hashCode);
		obj2codeMap.put(object, hv);
		code2objMap.computeIfAbsent(hashCode,HashIndex::orderedSetCreator).add(object);
	}
	
	static private Set<EObject> orderedSetCreator(Long key) {
		return new LinkedHashSet<>();
	}
	
	@Override
	public Long remove(EObject object) {
		HashValue64 hv = obj2codeMap.remove(object);
		if(hv!=null) {
			Set<EObject> set = code2objMap.get(hv.code);
			if(set!=null) {
				set.remove(object);
				if(set.isEmpty()) {
					code2objMap.remove(hv.code);
				}
			}
			return hv.code;
		}
		else return null;
	}
	
	@Override
	public Long getHash(EObject object) {
		HashValue64 hv = obj2codeMap.get(object);
		if(hv!=null) return hv.code;
		else return null;
	}
	
	@Override
	public Iterable<EObject> getRemainingObjects() {
		return new ArrayList<>(obj2codeMap.keySet());
	}

	@Override
	public void printHashCodes(BiFunction<EObject, Long, String> function) {
		code2objMap.forEach((h,list)->{
			Object hashString = Hash64.toString(h)+":"+Long.bitCount(h);
			list.forEach(e->{				
				System.out.println(String.format("%s\t%s", hashString, function.apply(e, h)));
			});
		});
	}
	
	static class DiffPair {
		public DiffPair(double sim, EObject eObj) {
			super();
			this.diff = 1.0 - sim;
			this.eObj = eObj;
		}

		public double diff;
		public EObject eObj;
		
		public EObject getEObject() {
			return eObj;
		}
		
		static public int compare(DiffPair a, DiffPair b) {
			double delta = a.diff - b.diff;
			if(delta<0) return -1;
			else if(delta==0) return a.hashCode() - b.hashCode();
			else return 1;
		}
	}
	
	@Override
	public Iterable<EObject> getValuesToMatchAhead(Side side) {
		if(obj2codeMap.size() > SEARCH_WINDOW) {
			return getRemainingObjects();
		}
		return Collections.emptyList();
	}
}
