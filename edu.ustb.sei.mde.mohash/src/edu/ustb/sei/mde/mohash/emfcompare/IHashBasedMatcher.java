package edu.ustb.sei.mde.mohash.emfcompare;

import java.util.Set;

import org.eclipse.emf.compare.match.eobject.IEObjectMatcher;
import org.eclipse.emf.compare.match.eobject.ScopeQuery;
import org.eclipse.emf.ecore.EClass;

import edu.ustb.sei.mde.mohash.EObjectSimHasher;

public interface IHashBasedMatcher extends IEObjectMatcher, ScopeQuery{
	EObjectSimHasher getHasher();
	void setIgnoredClasses(Set<EClass> ignoredClasses);
}
