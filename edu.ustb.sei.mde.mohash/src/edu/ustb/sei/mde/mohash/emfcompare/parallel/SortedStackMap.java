package edu.ustb.sei.mde.mohash.emfcompare.parallel;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import org.eclipse.emf.ecore.EObject;

public class SortedStackMap implements Iterable<EObject> {
	private int hostID;
	private SortedMap<Double, RoundedStack> internalStackMap = new TreeMap<>();
	
	public void put(Double key, EObject val) {
		RoundedStack list = internalStackMap.computeIfAbsent(key, k->new RoundedStack(board.getNumberOfThreads()));
		list.add(val);
		board.addToWaitingList(val, hostID);
	}
	
	private Scoreboard board;
	public SortedStackMap(Scoreboard board, int hostID) {
		this.board = board;
		this.hostID = hostID;
	}

	@Override
	public Iterator<EObject> iterator() {
		Iterator<Entry<Double, RoundedStack>> iter = internalStackMap.entrySet().iterator();
		
		return new Iterator<EObject>() {
			private Iterator<EObject> listIter = null;
			@Override
			public boolean hasNext() {
				while(true) {
					if(listIter==null) {
						if(iter.hasNext()) {
							listIter = iter.next().getValue().iterator();
						}
						else return false;
					} else {
						if(listIter.hasNext()) return true;
						else listIter = null;
					}
				}
			}

			@Override
			public EObject next() {
				if(!hasNext()) return null;
				else {
					EObject next = listIter.next();
					int repeated = 0;
					while(true) {		
						repeated++;
						if(repeated > 10000000) throw new Error("Max iteration!");
						int waitingStatus = board.getWaitingStatus(next);
						if(waitingStatus < 0) {
							return next();
						} else {
							if(waitingStatus >= hostID) {
								if(board.isPreviousSearched(hostID)) {
									listIter = null;
									return next;
								}
							} 
							try {
								Thread.sleep(1);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							
						}
					}
				}
			}
		};
	}
	
	static class RoundedStack implements Iterable<EObject> {
		private EObject[] array;
		private int top = 0;
		private int bottom = 0;
		public RoundedStack(int threads) {
			array = new EObject[threads + 1];
		}
		
		public boolean isEmpty() {
			return top == bottom;
		}
		
		public void add(EObject object) {
			top = (top + 1) % array.length;
			array[top] = object;
			if(bottom == top) {
				bottom = (bottom + 1) % array.length;
			}
		}

		@Override
		public Iterator<EObject> iterator() {
			return new Iterator<EObject>() {
				private int cur = top;
				@Override
				public boolean hasNext() {
					return cur != bottom;
				}

				@Override
				public EObject next() {
					EObject o = array[cur];
					cur = (cur + array.length - 1) % array.length;
					return o;
				}
			};
		}
		
	}
}
