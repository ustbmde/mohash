package edu.ustb.sei.mde.mohash.emfcompare;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.match.eobject.EObjectIndex;
import org.eclipse.emf.compare.match.eobject.ProximityEObjectMatcher;
import org.eclipse.emf.compare.match.eobject.ScopeQuery;
import org.eclipse.emf.compare.match.eobject.WeightProvider;
import org.eclipse.emf.compare.match.eobject.internal.MatchAheadOfTime;
import org.eclipse.emf.compare.match.eobject.internal.ProximityMatchStats;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import com.google.common.collect.Maps;

import edu.ustb.sei.mde.mohash.ByTypeIndex;
import edu.ustb.sei.mde.mohash.EHasherTable;
import edu.ustb.sei.mde.mohash.EObjectSimHasher;
import edu.ustb.sei.mde.mohash.EObjectSimHasherEx;
import edu.ustb.sei.mde.mohash.EObjectSimHasherWithJIT;
import edu.ustb.sei.mde.mohash.HWTreeBasedIndex;
import edu.ustb.sei.mde.mohash.ObjectIndex;
import edu.ustb.sei.mde.mohash.StructureOnlyEHasherTable;
import edu.ustb.sei.mde.mohash.TypeMap;
import edu.ustb.sei.mde.mohash.WeightedEHasherTable;
import edu.ustb.sei.mde.mohash.functions.AccessBasedLRUCache;
import edu.ustb.sei.mde.mohash.profiling.Timer;

@SuppressWarnings("restriction")
public class HashBasedEObjectIndex implements EObjectIndex, MatchAheadOfTime {
	private ProximityMatchStats stats = new ProximityMatchStats();
	
	public ProximityMatchStats getStats() {
		return stats;
	}
	
	public Timer timer = new Timer();
	
	/**
	 * The distance function used to compare the Objects.
	 */
	protected ReasonableCachingDistance meter;
	
	private AccessBasedLRUCache<EObject, Boolean> skipIdenticCheck = new AccessBasedLRUCache<>(10000, 1000, 0.75f);

	protected Set<EClass> ignoredClasses = Collections.emptySet();
	
//	protected boolean isReasonableCachingDistanceFunction;
	
	protected boolean needHashing(EClass cls) {
		return ignoredClasses.contains(cls)==false;
	}

	public void setIgnoredClasses(Set<EClass> ignoredClasses) {
		this.ignoredClasses = ignoredClasses;
	}
	
	/**
	 * An object able to tell us whether an object is in the scope or not.
	 */
	protected ScopeQuery scope;
	
	protected ObjectIndex lefts;
	protected ObjectIndex rights;
	protected ObjectIndex origins;
	
	private EObjectSimHasher hasher;
	
	private Function<EClass, ObjectIndex> objectIndexBuilder = (t)->new HWTreeBasedIndex();
	
	public EObjectSimHasher getEObjectHasher() {
		return hasher;
	}
	
	public Iterable<ObjectIndex> getIndices() {
		return Arrays.asList(lefts, rights, origins);
	}
	
//	public HashBasedEObjectIndex(ReasonableCachingDistance meter, ScopeQuery matcher) {
//		this(meter, matcher, null);
//	}
	
//	public HashBasedEObjectIndex(ReasonableCachingDistance meter, ScopeQuery matcher, WeightProvider.Descriptor.Registry weightProviderRegistry) {
//		this(meter, matcher, weightProviderRegistry, null);
//	}
	
	public HashBasedEObjectIndex(ReasonableCachingDistance meter, ScopeQuery matcher, WeightProvider.Descriptor.Registry weightProviderRegistry, Double threshold, boolean extendedHasher) {
		this(meter, matcher, weightProviderRegistry, new TypeMap<Double>(threshold), extendedHasher, null);
	}
	
	private WeightProvider.Descriptor.Registry weightProviderRegistry;
	private Map<EClass, Double> containerSimilarityRatioMap;
	
	public double getContainerSimilarityRatio(EObject object) {
		EClass clazz = object.eClass();
		Double ratio = containerSimilarityRatioMap.get(clazz);
		if(ratio==null) {
			int max = 0;
			WeightProvider highestRankingWeightProvider = weightProviderRegistry.getHighestRankingWeightProvider(clazz.getEPackage());
			for (EStructuralFeature feat : clazz.getEAllStructuralFeatures()) {
				EClassifier eType = feat.getEType();
				if (eType != null) { // Do not update amount in case of untyped feature
					int featureWeight = highestRankingWeightProvider.getWeight(feat);
					if (featureWeight != 0) {
						max += featureWeight;
					}
				}
			}
			max = max + highestRankingWeightProvider.getContainingFeatureWeight(object);
			int containerWeight = highestRankingWeightProvider.getParentWeight(object);
			ratio = ((double) containerWeight) / max;
			containerSimilarityRatioMap.put(clazz, ratio);
		}
		
		return ratio;
	}
	
	public HashBasedEObjectIndex(ReasonableCachingDistance meter, ScopeQuery matcher, WeightProvider.Descriptor.Registry weightProviderRegistry, TypeMap<Double> threshold, boolean extendedHasher, Function<EClass, ObjectIndex> objectIndexBuilder) {
		this.meter = meter;
		this.scope = matcher;
		if(objectIndexBuilder!=null)
			this.objectIndexBuilder = objectIndexBuilder;
		
		if(threshold==null) this.thresholdMap = new TypeMap<Double>(0.5);
		else this.thresholdMap = threshold;

		// we do not use bucket index by default
		initIndex();
		
		this.weightProviderRegistry = weightProviderRegistry;
		this.containerSimilarityRatioMap = new HashMap<>();
		
		if(weightProviderRegistry==null) {
			if(extendedHasher) this.hasher = new EObjectSimHasherEx();
			else this.hasher = new EObjectSimHasher();
		}
		else {
			EHasherTable table;
			
			switch(EObjectSimHasher.TABLE_KIND) {
			case DEFAULT: table = new EHasherTable(); break;
			case WEIGHTED: table = new WeightedEHasherTable(weightProviderRegistry); break;
			case STRUCTURAL: table = new StructureOnlyEHasherTable(); break;
			default: table = new EHasherTable();
			}
			
			if(EObjectSimHasher.ENABLE_JIT) this.hasher = new EObjectSimHasherWithJIT(table);
			else {
				if(extendedHasher) this.hasher = new EObjectSimHasherEx(table);
				else this.hasher = new EObjectSimHasher(table);
			}
		}
	}

	protected void initIndex() {
		this.lefts = new ByTypeIndex(objectIndexBuilder);
		this.rights = new ByTypeIndex(objectIndexBuilder);
		this.origins = new ByTypeIndex(objectIndexBuilder);
	}

	@Override
	public Iterable<EObject> getValuesStillThere(Side side) {
		switch(side) {
		case LEFT: return lefts.getRemainingObjects();
		case RIGHT: return rights.getRemainingObjects();
		case ORIGIN: return origins.getRemainingObjects();
		}
		return Collections.emptySet();
	}

	public Map<Side, EObject> findClosests(Comparison inProgress, EObject eObj, Side passedObjectSide) {
		if (!readyForThisTest(inProgress, eObj)) {
			return null;
		}
		
//		timer.tic("findClosests");
		
		Map<Side, EObject> result = new HashMap<EObjectIndex.Side, EObject>(3);
		result.put(passedObjectSide, eObj);
		if (passedObjectSide == Side.LEFT) {
			EObject closestRight = findTheClosest(inProgress, eObj, Side.LEFT, Side.RIGHT, true);
			EObject closestOrigin = findTheClosest(inProgress, eObj, Side.LEFT, Side.ORIGIN, true);
			result.put(Side.RIGHT, closestRight);
			result.put(Side.ORIGIN, closestOrigin);
		} else if (passedObjectSide == Side.RIGHT) {
			EObject closestLeft = findTheClosest(inProgress, eObj, Side.RIGHT, Side.LEFT, true);
			EObject closestOrigin = findTheClosest(inProgress, eObj, Side.RIGHT, Side.ORIGIN, true);
			result.put(Side.LEFT, closestLeft);
			result.put(Side.ORIGIN, closestOrigin);

		} else if (passedObjectSide == Side.ORIGIN) {
			EObject closestLeft = findTheClosest(inProgress, eObj, Side.ORIGIN, Side.LEFT, true);
			EObject closestRight = findTheClosest(inProgress, eObj, Side.ORIGIN, Side.RIGHT, true);
			result.put(Side.LEFT, closestLeft);
			result.put(Side.RIGHT, closestRight);
		}
		
//		timer.toc("findClosests");
		
		return result;

	}
	
	private TypeMap<Double> thresholdMap;
	
	
//	HashMap<EClass, Map<P, Integer>> countMap = new HashMap<EClass, Map<P,Integer>>(Collections.emptyMap());
//	void count(EObject a, ObjectIndex sa, EObject b, ObjectIndex sb) {
//		long ha = sa.getHash(a);
//		long hb = sb.getHash(b);
//		P p = new P(ha,hb);
//		EClass cls = a.eClass();
//		
//		Map<P,Integer> map = countMap.computeIfAbsent(cls, k -> new HashMap<>());
//		map.compute(p, (k,v)->{
//			if(v==null) return 0;
//			else return v + 1;
//		});
//	}
//	
//	class P {
//		long first;
//		long second;
//		
//		P(long first, long second) {
//			this.first = first;
//			this.second = second;
//		}
//		
//		@Override
//		public int hashCode() {
//			long a = Math.max(first, second);
//			long b = Math.min(first, second);
//			return (int) ((a * 31 + b) % Integer.MAX_VALUE);
//		}
//		
//		@Override
//		public boolean equals(Object obj) {
//			if(obj instanceof P) {
//				return (((P) obj).first == first && ((P) obj).second == second) 
//						|| (((P) obj).second == first && ((P) obj).first == second);
//			}
//			else return false;
//		}
//	}
	
	private EObject findTheClosest(Comparison inProgress, final EObject eObj, final Side originalSide,
			final Side sideToFind, boolean shouldDoubleCheck) {
		ObjectIndex originStorage = lefts;
		switch (originalSide) {
			case RIGHT:
				originStorage = rights;
				break;
			case LEFT:
				originStorage = lefts;
				break;
			case ORIGIN:
				originStorage = origins;
				break;

			default:
				break;
		}
		
		ObjectIndex storageToSearchFor = lefts;
		switch (sideToFind) {
			case RIGHT:
				storageToSearchFor = rights;
				break;
			case LEFT:
				storageToSearchFor = lefts;
				break;
			case ORIGIN:
				storageToSearchFor = origins;
				break;

			default:
				break;
		}
		
		// find identical by hash code
		Long hash = originStorage.getHash(eObj);
		
		if(!skipIdenticCheck.getOrDefault(eObj, Boolean.FALSE)) {
//			timer.tic("id-check");
			Iterable<EObject> cand = storageToSearchFor.query(eObj, null, hash, 1.0, 0.0);
			for(EObject fastCheck : cand) {
				if (!readyForThisTest(inProgress, fastCheck)) {
					stats.backtrack();
				} else {
					stats.identicCompare();
					if (meter.areIdentic(inProgress, eObj, fastCheck)) {
						stats.identicSuccess();
						// For debugging
//						this.avoidable += unmatched.get(eObj);
//						this.avoidable += unmatched.get(fastCheck);
						// End
//						timer.toc("id-check");
						return fastCheck;
					}
				}
			}
//			timer.toc("id-check");
		} // skip if eObj has been checked

		Match containerMatch = getPreviousMatch(eObj.eContainer(), inProgress);
		SortedMap<Double, EObject> candidates = Maps.newTreeMap();
		double minSim = getMinSim(eObj);
		double containerDiff = getContainerSimilarityRatio(eObj);
		
		boolean canCache = true;
		
		EObject matchedContainer = null;
		if(containerMatch!=null) {			
			switch (sideToFind) {
			case RIGHT:
				matchedContainer = containerMatch.getRight();
				break;
			case LEFT:
				matchedContainer = containerMatch.getLeft();
				break;
			case ORIGIN:
				matchedContainer = containerMatch.getOrigin();
				break;
			default:
				break;
			}
		} else {
			canCache = false;
		}
		
		
		/*
		 * We could not find an EObject which is identical, let's search again and find the closest EObject.
		 */
		Iterable<EObject> cand2 = storageToSearchFor.query(eObj, matchedContainer, hash, minSim, containerDiff);
		
		/*
		 * A potential improvement is to parallelize the following loop when cand2.size is greater than a certain threshold
		 */
		
		double bestDistance = Double.MAX_VALUE;
		EObject bestObject = null;
		
		if(shouldDoubleCheck) {
			// For debugging
//			int size = 0;
			// End
			
//			String tag = "dist-check";
//			timer.tic(tag);
			for(EObject potentialClosest : cand2) {
				// For debugging
//				size ++;
				// End
				double dist = meter.distance(inProgress, eObj, potentialClosest, matchedContainer == potentialClosest.eContainer());
//				count(eObj, originStorage, potentialClosest, storageToSearchFor);
				stats.similarityCompare();
				if(dist < bestDistance) {
					candidates.put(Double.valueOf(dist), potentialClosest);
				}
				// For debugging
//				unmatched.add(potentialClosest);
				// End
				
				// FIXME: the following code should not be executed if we want to be consistent with EMF Compare
				// However, the following code may actually improve the result of the match
//				else if(dist<Double.MAX_VALUE && dist != bestDistance && candidates.size() < 3) {
//					candidates.put(Double.valueOf(dist), potentialClosest);
//				}
			}
//			timer.toc(tag);
			
			// For debugging
//			if (size != 0)
//				firstCandSetSizes.add(size);
			//			for(EObject c : candidates.values()) unmatched.decrese(c);
			// End
			
			// double check
			for (Entry<Double, EObject> entry : candidates.entrySet()) {
				EObject doubleCheck = findTheClosest(inProgress, entry.getValue(), sideToFind, originalSide, false);
				stats.doubleCheck();
				if (doubleCheck == eObj) {
					stats.similaritySuccess();
					// For debugging
//					firstSetPositions.add(storageToSearchFor.getPosition(hash, entry.getValue(), cand2));
					// End
					return entry.getValue();
				} else {
					stats.failedDoubleCheck();
				}
			}
		} else {
			// For debugging
//			int size = 0;
			// End
//			String tag = "dist-check-db";
//			timer.tic(tag);
			for(EObject potentialClosest : cand2) {
				// For debugging
//				size ++;
				// End
				double dist;
				dist = meter.distance(inProgress, eObj, potentialClosest, matchedContainer == potentialClosest.eContainer(), canCache);
//					count(eObj, originStorage, potentialClosest, storageToSearchFor);
				stats.similarityCompare();
				
				if (dist < bestDistance) {
					bestDistance = dist;
					bestObject = potentialClosest;
				}
				// For debugging
//				unmatched.add(potentialClosest);
				// End
			}
//			timer.toc(tag);
			// For debugging
//			if (size != 0)
//				secondCandSetSizes.add(size);
//			if (bestObject != null)
//				unmatched.decrese(bestObject);
//			secondSetPositions.add(storageToSearchFor.getPosition(hash, bestObject, cand2));
			// End
		} 
		
		if(bestObject==null) stats.noMatch();
		
		return bestObject;
	}

	public Match getPreviousMatch(final EObject eObj, Comparison inProgress) {
		if(eObj==null) return null;
		else return inProgress.getMatch(eObj);
	}

	protected double getMinSim(final EObject eObj) {
		EClass clazz = eObj.eClass();
		return thresholdMap.get(clazz);
	}

	@Override
	public void remove(EObject eObj, Side side) {
		switch(side) {
		case LEFT: lefts.remove(eObj); break;
		case RIGHT: rights.remove(eObj); break;
		case ORIGIN: origins.remove(eObj); break;
		}
	}
	
	// For debugging
//	private List<Integer> firstSetPositions = new ArrayList<>(1024);
//	private List<Integer> secondSetPositions = new ArrayList<>(1024);
//	private List<Integer> firstCandSetSizes = new ArrayList<>(1024);
//	private List<Integer> secondCandSetSizes = new ArrayList<>(1024);
//	private Bag<EObject> unmatched = new Bag<>();
//	private int avoidable = 0;
	// End
	
	@Override
	public void index(EObject eObj, Side side) {
		long hashcode = needHashing(eObj.eClass()) ? hasher.hash(eObj) : hasher.pseudoHash(eObj);
		switch(side) {
		case LEFT: lefts.index(eObj, hashcode); break;
		case RIGHT: rights.index(eObj, hashcode); break;
		case ORIGIN: origins.index(eObj, hashcode); break;
		}
		meter.recordDigest(eObj);
	}
	
	public void index(EObject eObj, long hashcode, Side side) {
		switch(side) {
		case LEFT: lefts.index(eObj, hashcode); break;
		case RIGHT: rights.index(eObj, hashcode); break;
		case ORIGIN: origins.index(eObj, hashcode); break;
		}
	}
	
	protected boolean readyForThisTest(Comparison inProgress, EObject fastCheck) {
		EObject eContainer = fastCheck.eContainer();
		if (eContainer != null && scope.isInScope(eContainer)) {
			return inProgress.getMatch(eContainer) != null;
		}
		return true;
	}

	@Override
	public Iterable<EObject> getValuesToMatchAhead(Side side) {
		switch(side) {
		case LEFT: return lefts.getValuesToMatchAhead(side);
		case RIGHT: return rights.getValuesToMatchAhead(side);
		case ORIGIN: origins.getValuesToMatchAhead(side);
		}
		return Collections.emptyList();
	}

	// For debugging
	public void printStats(String label, PrintStream out) {
		out.print("[" + label + "]\t");
		out.println(stats);
		
//		int uniqueD = countMap.values().stream().map(map -> map.keySet().size()).reduce(0, (l,r)->l+r);
//		int dupD = countMap.values().stream().map(map -> map.values().stream().reduce(0, (l,r)->l+r)).reduce(0, (l,r)->l+r);
//		out.print("[" + label + "]\t");
//		out.println(uniqueD+" unique-dist, "+ dupD + " dup-dist");
//		out.println();
//		out.print("FirstSet\t");
//		printSetPosition(this.firstSetPositions, out);
//		out.print("SecondSet\t");
//		printSetPosition(this.secondSetPositions, out);
//		out.print("FirstCandSet\t");
//		printSetPosition(this.firstCandSetSizes, out);
//		out.print("SecondCandSet\t");
//		printSetPosition(this.secondCandSetSizes, out);
//		out.println("Total unmatched " + this.unmatched.values().stream().reduce(0, (l, r) -> l + r) + "("
//				+ this.unmatched.size() + ")" + ", Total avoidable " + this.avoidable);
	}
	// End
	
	// For debugging
//	static void printSetPosition(List<Integer> positions, PrintStream out) {
//		Collections.sort(positions);
//		if (positions.isEmpty()) {
//			out.println("position set is empty");
//		} else {
//			int max = positions.get(positions.size() - 1);
//			int min = positions.get(0);
//			double avg = positions.stream().collect(Collectors.averagingInt(i -> i));
//			int med = positions.get(positions.size() / 2);
//			int top90 = positions.get((int) Math.floor(positions.size() * 0.9));
//			int top95 = positions.get((int) Math.floor(positions.size() * 0.95));
//			int top98 = positions.get((int) Math.floor(positions.size() * 0.98));
//			int top99 = positions.get((int) Math.floor(positions.size() * 0.99));
//			out.println("max=" + max + ", min=" + min + ", avg=" + avg + ", med=" + med + ", top90=" + top90
//					+ ", top95=" + top95 + ", top98=" + top98 + ", top99=" + top99);
//		}
//
//	}
	// End
	
	protected boolean validCheck(Comparison comparison, EObject obj, Long hash) {
		if(hash != null) return true;
		else {
			// when hash is null, obj has been removed from the index (it is matched) or it has not been indexed
			// So we check if obj has a match.
			return comparison.getMatch(obj) != null;
		}
	}

	public Map<Side, EObject> fastIdenticalMatch(Comparison comparison, EObject obj2Match, Side fromSide, Match containerMatch) {
		ObjectIndex fromIndex;
		ObjectIndex aIndex;
		ObjectIndex bIndex;
		EObject aContainer;
		EObject bContainer;
		Side aSide;
		Side bSide;
		
//		timer.tic("fast-id-check");
		
		switch(fromSide) {
		case LEFT: 
			fromIndex = lefts;
			aIndex = rights;
			bIndex = origins;
			aContainer = containerMatch.getRight();
			bContainer = containerMatch.getOrigin();
			aSide = Side.RIGHT;
			bSide = Side.ORIGIN;
			break;
		case RIGHT: 
			fromIndex = rights;
			aIndex = lefts;
			bIndex = origins;
			aContainer = containerMatch.getLeft();
			bContainer = containerMatch.getOrigin();
			aSide = Side.LEFT;
			bSide = Side.ORIGIN;
			break;
		case ORIGIN: 
			fromIndex = origins;
			aIndex = lefts;
			bIndex = rights;
			aContainer = containerMatch.getLeft();
			bContainer = containerMatch.getRight();
			aSide = Side.LEFT;
			bSide = Side.RIGHT;
			break;
		default:
			return null;
		}
		
		Long fromHash = fromIndex.getHash(obj2Match);
		if(fromHash == null) return null;
		
		Iterable<EObject> aChildren = aContainer == null ? Collections.emptySet() : aIndex.query(obj2Match, aContainer, fromHash, 1, 0);
		EObject aMatch = null;
		for(EObject aChild : aChildren) {
			if(meter.areIdentic(comparison, obj2Match, aChild)) {
				aMatch = aChild;
				break;
			}
		}
		
		Iterable<EObject> bChildren = bContainer == null ? Collections.emptySet() : bIndex.query(obj2Match, bContainer, fromHash, 1, 0);
		EObject bMatch = null;
		for(EObject bChild : bChildren) {
			if(meter.areIdentic(comparison, obj2Match, bChild)) {
				bMatch = bChild;
				break;
			}
		}
//		timer.toc("fast-id-check");
		if(aMatch == null && bMatch == null) {
			/*
			 * We should record obj2Match when we can ensure that we should not
			 * perform identic check against it. If we checked all aChildren and bChildren
			 * 
			 */
			skipIdenticCheck.put(obj2Match, Boolean.TRUE);
			return null;
		} else {
			Map<Side, EObject> map = new HashMap<>();
			map.put(fromSide, obj2Match);
			map.put(aSide, aMatch);
			map.put(bSide, bMatch);
			return map;
		}
	}
}
