package edu.ustb.sei.mde.mohash.emfcompare.parallel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.BiFunction;

import org.eclipse.emf.compare.match.eobject.EObjectIndex.Side;
import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import edu.ustb.sei.mde.mohash.ObjectIndex;

public class ParallelSimpleIndex implements ObjectIndex {
	private Set<EObject> allObjects = new LinkedHashSet<>();
	private java.util.concurrent.locks.ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	private List<EObject> shadowCopy = null;
	// For debugging
	@Override
	public int getPosition(long hash, EObject selected, Iterable<EObject> candidates) {
		return allObjects.size();
	}
	// End
	
	public Iterable<EObject> filterByContainer(Set<EObject> candidates, EObject container) {
		Set<EObject> children = new LinkedHashSet<>(container.eContents());
		
		if(candidates.size() <= children.size()) {
			return Sets.filter(new LinkedHashSet<>(candidates), e -> children.contains(e));
		} else {
			children.retainAll(candidates);
			return children;
		}
	}

	@Override
	final public Iterable<EObject> query(EObject target, EObject containerMatch, long hashCode, double minSim,
			double containerDiff) {
		Iterable<EObject> res = null;
		lock.readLock().lock();
		if(containerMatch!=null) res = filterByContainer(allObjects, containerMatch);
		else {
			if(shadowCopy ==null || shadowCopy.size() != allObjects.size()) 
				shadowCopy = new ArrayList<>(allObjects);
			res = shadowCopy;
		}
		lock.readLock().unlock();
		return res;
	}

	@Override
	final public void index(EObject object, long hashCode) {
		shadowCopy = null;
		allObjects.add(object);
	}

	@Override
	final public Long remove(EObject object) {
		lock.writeLock().lock();
		allObjects.remove(object);
		lock.writeLock().unlock();
		return 0L;
	}

	@Override
	final public Long getHash(EObject object) {
		return 0L;
	}

	@Override
	final public Iterable<EObject> getRemainingObjects() {
		if(shadowCopy ==null || shadowCopy.size() != allObjects.size())
			shadowCopy = new ArrayList<>(allObjects);
		return shadowCopy;
	}

	@Override
	public void printHashCodes(BiFunction<EObject, Long, String> function) {
		allObjects.forEach(o->{
			System.out.println(function.apply(o, 0L)); 
		});
	}

	@Override
	public Iterable<EObject> getValuesToMatchAhead(Side side) {
		if(allObjects.size() > SEARCH_WINDOW) {
			return getRemainingObjects();
		}
		return Collections.emptyList();
	}

}
