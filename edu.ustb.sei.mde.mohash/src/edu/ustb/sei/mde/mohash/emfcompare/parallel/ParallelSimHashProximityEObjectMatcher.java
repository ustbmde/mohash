package edu.ustb.sei.mde.mohash.emfcompare.parallel;

import java.io.PrintStream;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.Monitor;
import org.eclipse.emf.compare.CompareFactory;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.ComparisonCanceledException;
import org.eclipse.emf.compare.EMFCompareMessages;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.match.eobject.EObjectIndex.Side;
import org.eclipse.emf.compare.match.eobject.EqualityHelperExtensionProvider.Descriptor.Registry;
import org.eclipse.emf.compare.match.eobject.IEObjectMatcher;
import org.eclipse.emf.compare.match.eobject.ScopeQuery;
import org.eclipse.emf.compare.match.eobject.WeightProvider;
import org.eclipse.emf.compare.match.eobject.internal.MatchAheadOfTime;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;
import com.google.common.collect.Maps;

import edu.ustb.sei.mde.mohash.EObjectSimHasher;
import edu.ustb.sei.mde.mohash.TypeMap;
import edu.ustb.sei.mde.mohash.emfcompare.IHashBasedMatcher;
import edu.ustb.sei.mde.mohash.factory.MoHashMatchEngineFactory;

@SuppressWarnings("restriction")
public class ParallelSimHashProximityEObjectMatcher implements IEObjectMatcher, ScopeQuery, IHashBasedMatcher {

	/**
	 * Number of elements to index before a starting a match ahead step.
	 */
	private static final int NB_ELEMENTS_BETWEEN_MATCH_AHEAD = 10000;

	/**
	 * The index which keep the EObjects.
	 */
	protected ParallelHashBasedEObjectIndex index;
	
	public ParallelHashBasedEObjectIndex getIndex() {
		return index;
	}

	/**
	 * Keeps track of which side was the EObject from.
	 */
	protected Map<EObject, Side> eObjectsToSide = Maps.newHashMap();
	
	private int numberOfThreads = MoHashMatchEngineFactory.numberOfThreads;
	ExecutorCompletionService<Integer> executor = null;
	
	public ParallelSimHashProximityEObjectMatcher(ParallelURIDistance sharedDistance,
			Registry equalityHelperExtensionProviderRegistry,
			org.eclipse.emf.compare.match.eobject.WeightProvider.Descriptor.Registry weightProviderRegistry,
			TypeMap<Double> thresholds) {
		
		final ParallelReasonableCachingDistance meter = new ParallelReasonableCachingDistance(numberOfThreads, weightProviderRegistry, equalityHelperExtensionProviderRegistry, sharedDistance);
		this.index = new ParallelHashBasedEObjectIndex(meter, this, weightProviderRegistry, thresholds, false, (t)->{
			if(needHashing(t)) return new ParallelHashIndex();
			else return new ParallelSimpleIndex();
		});
	}

	protected Set<EClass> ignoredClasses = Collections.emptySet();

	private boolean doTopDownUpdate = false;
	
	protected boolean needHashing(EClass cls) {
		return ignoredClasses.contains(cls)==false;
	}

	public void setIgnoredClasses(Set<EClass> ignoredClasses) {
		this.ignoredClasses = ignoredClasses;
		this.index.setIgnoredClasses(ignoredClasses);
	}

	/**
	 * {@inheritDoc}
	 */
	public void createMatches(Comparison comparison, Iterator<? extends EObject> leftEObjects,
			Iterator<? extends EObject> rightEObjects, Iterator<? extends EObject> originEObjects,
			Monitor monitor) {
		if (!leftEObjects.hasNext() && !rightEObjects.hasNext() && !originEObjects.hasNext()) {
			return;
		}
		
		ExecutorService threadPool = Executors.newFixedThreadPool(numberOfThreads);
		executor = new ExecutorCompletionService<>(threadPool);
		for(int i = 0; i< numberOfThreads; i++) threadPool.submit(()->{});
		

		monitor.subTask(EMFCompareMessages.getString("ProximityEObjectMatcher.monitor.indexing")); //$NON-NLS-1$
		doIndexing(comparison, leftEObjects, rightEObjects, originEObjects, monitor);
		
		long start = System.nanoTime();
		monitor.subTask(EMFCompareMessages.getString("ProximityEObjectMatcher.monitor.matching")); //$NON-NLS-1$
		matchIndexedObjects(comparison, monitor);
		long end = System.nanoTime();
		System.out.println("parhash: " + (end - start) / 1000.0);
		
		
		createUnmatchesForRemainingObjects(comparison, monitor);
		restructureMatchModel(comparison, monitor);
		
		awaitTerminationAfterShutdown(threadPool);
	}

	protected void doIndexing(Comparison comparison, Iterator<? extends EObject> leftEObjects,
			Iterator<? extends EObject> rightEObjects, Iterator<? extends EObject> originEObjects, Monitor monitor) {
		int nbElements = 0;
		int lastSegment = 0;
		
		/*
		 * We are iterating through the three sides of the scope at the same time so that index might apply
		 * pre-matching strategies elements if they wish.
		 */
		while (leftEObjects.hasNext() || rightEObjects.hasNext() || originEObjects.hasNext()) {
			if (monitor.isCanceled()) {
				throw new ComparisonCanceledException();
			}

			if (leftEObjects.hasNext()) {
				EObject next = leftEObjects.next();
				nbElements++;
				index.index(next, Side.LEFT);
				eObjectsToSide.put(next, Side.LEFT);
			}

			if (rightEObjects.hasNext()) {
				EObject next = rightEObjects.next();
				index.index(next, Side.RIGHT);
				eObjectsToSide.put(next, Side.RIGHT);
			}

			if (originEObjects.hasNext()) {
				EObject next = originEObjects.next();
				index.index(next, Side.ORIGIN);
				eObjectsToSide.put(next, Side.ORIGIN);
			}
			if (nbElements / NB_ELEMENTS_BETWEEN_MATCH_AHEAD > lastSegment) {
				matchAheadOfTime(comparison, monitor);
				lastSegment++;
			}

		}
	}

	/**
	 * If the index supports it, match element ahead of time, in case of failure the elements are kept and
	 * will be processed again later on.
	 * 
	 * @param comparison
	 *            the current comparison.
	 * @param monitor
	 *            monitor to track progress.
	 */
	private void matchAheadOfTime(Comparison comparison, Monitor monitor) {
		if (index instanceof MatchAheadOfTime) {
			matchList(comparison, ((MatchAheadOfTime)index).getValuesToMatchAhead(Side.LEFT), Side.LEFT, false, monitor);
			matchList(comparison, ((MatchAheadOfTime)index).getValuesToMatchAhead(Side.RIGHT), Side.RIGHT, false, monitor);
		}
	}

	/**
	 * Match elements for real, if no match is found for an element, an object will be created to represent
	 * this unmatch and the element will not be processed again.
	 * 
	 * @param comparison
	 *            the current comparison.
	 * @param monitor
	 *            monitor to track progress.
	 */
	private void matchIndexedObjects(Comparison comparison, Monitor monitor) {
		Iterable<EObject> todo = index.getValuesStillThere(Side.LEFT);
		while (todo.iterator().hasNext()) {
			if (monitor.isCanceled()) {
				throw new ComparisonCanceledException();
			}
			todo = matchList(comparison, todo, Side.LEFT, true, monitor);
		}
		todo = index.getValuesStillThere(Side.RIGHT);
		while (todo.iterator().hasNext()) {
			if (monitor.isCanceled()) {
				throw new ComparisonCanceledException();
			}
			todo = matchList(comparison, todo, Side.RIGHT, true, monitor);
		}

	}

	/**
	 * Create all the Match objects for the remaining EObjects.
	 * 
	 * @param comparison
	 *            the current comparison.
	 * @param monitor
	 *            a monitor to track progress.
	 */
	private void createUnmatchesForRemainingObjects(Comparison comparison, Monitor monitor) {
		for (EObject notFound : index.getValuesStillThere(Side.RIGHT)) {
			if (monitor.isCanceled()) {
				throw new ComparisonCanceledException();
			}
			areMatching(comparison, null, notFound, null);
		}
		for (EObject notFound : index.getValuesStillThere(Side.LEFT)) {
			if (monitor.isCanceled()) {
				throw new ComparisonCanceledException();
			}
			areMatching(comparison, notFound, null, null);
		}
		for (EObject notFound : index.getValuesStillThere(Side.ORIGIN)) {
			if (monitor.isCanceled()) {
				throw new ComparisonCanceledException();
			}
			areMatching(comparison, null, null, notFound);
		}
	}

	/**
	 * Process the list of objects matching them. This method might not be able to process all the EObjects if
	 * - for instance, their container has not been matched already. Every object which could not be matched
	 * is returned in the list.
	 * 
	 * @param comparison
	 *            the comparison being built.
	 * @param todoList
	 *            the list of objects to process.
	 * @param createUnmatches
	 *            whether elements which have no match should trigger the creation of a Match object (meaning
	 *            we won't try to match them afterwards) or not.
	 * @param monitor
	 *            a monitor to track progress.
	 * @return the list of EObjects which could not be processed for some reason.
	 */
	private Iterable<EObject> matchList(Comparison comparison, Iterable<EObject> todoList, Side side,
			boolean createUnmatches, Monitor monitor) {
		List<EObject> requiredContainers = new LinkedList<>();
		Iterator<EObject> todo = todoList.iterator();
		while (todo.hasNext()) {
			if (monitor.isCanceled()) {
				throw new ComparisonCanceledException();
			}
			EObject next = todo.next();
			
			EObject container = next.eContainer();
			while (container != null && isInScope(container)) {
				if (comparison.getMatch(container) == null) {
					requiredContainers.add(0, container);
				}
				container = container.eContainer();
			}
		}
		Iterator<EObject> containersAndTodo = Iterators.concat(requiredContainers.iterator(),
				todoList.iterator());
		
		
		Scheduler scheduler = new Scheduler(side, this.numberOfThreads, this.index,containersAndTodo, comparison, createUnmatches);
		
		return scheduler.start(this.executor);
		
		
		
//		while (containersAndTodo.hasNext()) {
//			if (monitor.isCanceled()) {
//				throw new ComparisonCanceledException();
//			}
//			EObject next = containersAndTodo.next();
//			/*
//			 * At this point you need to be sure the element has not been matched in any other way before.
//			 */
//			if (comparison.getMatch(next) == null) {
//				if (!tryToMatch(comparison, next, createUnmatches)) {
//					remainingResult.add(next);
//				}
//			}
//		}
//		return remainingResult;
	}
	
	public void awaitTerminationAfterShutdown(ExecutorService threadPool) {
	    threadPool.shutdown();
	    try {
	        if (!threadPool.awaitTermination(60, TimeUnit.SECONDS)) {
	            threadPool.shutdownNow();
	        }
	    } catch (InterruptedException ex) {
	        threadPool.shutdownNow();
	        Thread.currentThread().interrupt();
	    }
	}

	

	/**
	 * Process all the matches of the given comparison and re-attach them to their parent if one is found.
	 * 
	 * @param comparison
	 *            the comparison to restructure.
	 * @param monitor
	 *            a monitor to track progress.
	 */
	private void restructureMatchModel(Comparison comparison, Monitor monitor) {
		Iterator<Match> it = ImmutableList.copyOf(Iterators.filter(comparison.eAllContents(), Match.class))
				.iterator();

		while (it.hasNext()) {
			if (monitor.isCanceled()) {
				throw new ComparisonCanceledException();
			}
			Match cur = it.next();
			EObject possibleContainer = null;
			if (cur.getLeft() != null) {
				possibleContainer = cur.getLeft().eContainer();
			}
			if (possibleContainer == null && cur.getRight() != null) {
				possibleContainer = cur.getRight().eContainer();
			}
			if (possibleContainer == null && cur.getOrigin() != null) {
				possibleContainer = cur.getOrigin().eContainer();
			}
			Match possibleContainerMatch = comparison.getMatch(possibleContainer);
			if (possibleContainerMatch != null) {
				((BasicEList<Match>)possibleContainerMatch.getSubmatches()).addUnique(cur);
			}
		}
	}
	
	protected Match areMatching(Comparison comparison, EObject left, EObject right, EObject origin) {
		Match result = CompareFactory.eINSTANCE.createMatch();
		result.setLeft(left);
		result.setRight(right);
		result.setOrigin(origin);
		((BasicEList<Match>)comparison.getMatches()).addUnique(result);
		
		if (left != null) {
			index.remove(left, Side.LEFT);
		}
		if (right != null) {
			index.remove(right, Side.RIGHT);
		}
		if (origin != null) {
			index.remove(origin, Side.ORIGIN);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isInScope(EObject eContainer) {
		return eObjectsToSide.get(eContainer) != null;
	}
	
	public void printStats(PrintStream out) {
		out.println(index.timer.toString());
		// For debugging
		this.index.printStats("MoHash", out);
		// End
	}

	@Override
	public EObjectSimHasher getHasher() {
		return getIndex().getEObjectHasher();
	}
}
