package edu.ustb.sei.mde.mohash.emfcompare.treehash;

public class TreeHashValue {

	public long subtreeChecksum = 0;
	public int subtreeStructureChecksum = 0;
	
	public long localContentHash = 0;
	public long childContentHash = 0;
	public byte lchBits = 0;
	public byte cchBits = 0;
	
	public int height = 1;
	public int size = 1;
	
	public boolean isSubtreeIdentical(TreeHashValue value) {
		return height == value.height && subtreeChecksum == value.subtreeChecksum;
	}
	
	public double computeSubtreeSimilarity(TreeHashValue value) {
//		if(subtreeStructureChecksum != value.subtreeStructureChecksum) return 0;
		double localSim = jaccard(localContentHash, lchBits, value.localContentHash, value.lchBits);
		double childSim = jaccard(childContentHash, cchBits, value.childContentHash, value.cchBits);
		if(localSim < 0 && childSim < 0) return 0.0;
		else if(localSim < 0) return childSim;
		else if(childSim < 0) return localSim;
		else return localSim * 0.8 + childSim * 0.2;
	}
	
	
	static final long m1 = 0x5555555555555555L; // binary: 0101...
    static final long m2 = 0x3333333333333333L; // binary: 00110011..
    static final long m4 = 0x0F0F0F0F0F0F0F0FL; // binary: 4 zeros, 4 ones ...
    static final long m8 = 0x00FF00FF00FF00FFL; // binary: 8 zeros, 8 ones ...
    static final long m16 = 0x0000FFFF0000FFFFL; // binary: 16 zeros, 16 ones ...
    static final long m32 = 0x00000000FFFFFFFFL; // binary: 32 zeros, 32 ones
    static final long hff = 0xFFFFFFFFFFFFFFFFL; // binary: all ones
    static final long h01 = 0x0101010101010101L;

    static public byte computeBitCounts(long x) {
    	return (byte) Long.bitCount(x);
//        x = (x & m1) + ((x >> 1) & m1); // put count of each 2 bits into those 2 bits
//        x = (x & m2) + ((x >> 2) & m2); // put count of each 4 bits into those 4 bits
//        x = (x & m4) + ((x >> 4) & m4); // put count of each 8 bits into those 8 bits
//        x = (x & m8) + ((x >> 8) & m8); // put count of each 16 bits into those 16 bits
//        x = (x & m16) + ((x >> 16) & m16); // put count of each 32 bits into those 32 bits
//        x = (x & m32) + ((x >> 32) & m32); // put count of each 64 bits into those 64 bits
//        return (byte) x;
    }
    
    static public double jaccard(long a, byte aBits, long b, byte bBits) {
    	if(a==0 && b==0) return -1.0;
    	long intersect = a & b;
    	if(intersect == 0) return 0;
    	byte iBits = computeBitCounts(intersect);
    	return iBits / (double) (aBits - iBits + bBits);
    }
}
