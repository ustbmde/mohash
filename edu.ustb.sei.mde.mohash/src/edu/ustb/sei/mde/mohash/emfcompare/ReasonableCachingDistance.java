package edu.ustb.sei.mde.mohash.emfcompare;

import java.util.Map;
import java.util.function.BiPredicate;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.match.eobject.EditionDistance;
import org.eclipse.emf.compare.match.eobject.ProximityEObjectMatcher.DistanceFunction;
import org.eclipse.emf.compare.match.eobject.internal.AccessBasedLRUCache;
import org.eclipse.emf.ecore.EObject;

/**
 * This is intended to fix the bug in EMF Compare.
 * EMF Compare will cache all the distances computed at any time. 
 * But it is incorrect because the distance will not be the final
 * one.
 * 
 * The bug occurs as follows. Given aObj, when bObj is viewed as
 * the candidate of aObj, and we should perform the double check of bObj.
 * bObj will be matched and the distances will be incorrectly cached when
 * the container of bObj is not matched.
 * @author dr-he
 *
 */
@SuppressWarnings("restriction")
public class ReasonableCachingDistance implements DistanceFunction {
	/**
	 * The wrapped function.
	 */
	protected EditionDistance wrapped;
	
	/**
	 * The cache keeping the previous results.
	 */
	protected Map<TupleKey<EObject, DistanceFunction>, Double> distanceCache;
		
	/**
	 * Create a new caching distance.
	 * 
	 * @param wrapped
	 *            actual distance function to cache results from.
	 */
	public ReasonableCachingDistance(EditionDistance wrapped) {
		this.wrapped = wrapped;
		distanceCache = new AccessBasedLRUCache<TupleKey<EObject, DistanceFunction>, Double>(10000, 1000, .75F);
	}
	
//	int miss = 0;
//	int herHits = 0;
////	int hits = 0;
//	int uncached = 0;
	
	/**
	 * {@inheritDoc}
	 */
	public double distance(Comparison inProgress, EObject a, EObject b) {
		TupleKey<EObject, DistanceFunction> key = new TupleKey<EObject, DistanceFunction>(a, b, this, TupleKey.otherPred);
		Double previousResult = distanceCache.get(key);
		if (previousResult == null) {
			double dist = wrapped.distance(inProgress, a, b);
			distanceCache.put(key, Double.valueOf(dist));
			// cache it
			return dist;
		}
		return previousResult.doubleValue();
	}
	
	public double distance(Comparison inProgress, EObject a, EObject b, boolean sameContainer) {
		return distance(inProgress, a, b);
	}
	
	/**
	 * This method is used to replace {@link distance(Comparison inProgress, EObject a, EObject b)}.
	 * @param inProgress
	 * @param a
	 * @param b
	 * @param canCache
	 * @return
	 */
	public double distance(Comparison inProgress, EObject a, EObject b, boolean sameContainer, boolean canCache) {
		if(canCache) 
			return distance(inProgress, a, b, sameContainer);
		else {
//			uncached ++;
			// a and b do not have the same container!!
			return wrapped.distance(inProgress, a, b);
		}
	}
	
	public void recordDigest(EObject object) {}

	/**
	 * {@inheritDoc}
	 */
	public boolean areIdentic(Comparison inProgress, EObject a, EObject b) {
		return wrapped.areIdentic(inProgress, a, b);
	}
	
	/**
	 * A class used as a key for two EObjects. Pair(a,b) and Pair(b,a) should be equals and have the same
	 * hashcodes
	 */
	static protected class TupleKey<V, W> {
		// CHECKSTYLE:OFF
		V a;
		V b;
		W c;
		
		final public static BiPredicate<Long, Long> longPred = (l, r) -> l.equals(r);
		final public static BiPredicate<Object, Object> otherPred = (l, r) -> l == r;
		
		@SuppressWarnings("rawtypes")
		final private BiPredicate pred;

		public TupleKey(V a, V b, W c, BiPredicate<?,?> pred) {
			super();
			this.a = a;
			this.b = b;
			this.c = c;
			this.pred = pred;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + c.hashCode();
			int first = a.hashCode();
			int second = b.hashCode();
			if (first > second) {
				int tmp = first;
				first = second;
				second = tmp;
			}
			result = prime * result + first;
			result = prime * result + second;
			return result;
		}

		/**
		 * {@inheritDoc}
		 */
		@SuppressWarnings("unchecked")
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			TupleKey<?,?> other = (TupleKey<?, ?>)obj;
			if (!c.equals(other.c)) {
				return false;
			}
			return (pred.test(a, other.a) && pred.test(b, other.b)) 
					|| (pred.test(b, other.a) && pred.test(a, other.b));

		}
	}
	// CHECKSTYLE:ON
}
