package edu.ustb.sei.mde.mohash.emfcompare.treehash;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.FeatureMap;

import edu.ustb.sei.mde.mohash.functions.AccessBasedLRUCache;
import edu.ustb.sei.mde.mohash.functions.URIComputer;
import edu.ustb.sei.mde.mohash.functions.URIComputer.FragmentIterable;

public class ElementIndexAdapter extends AdapterImpl {
	static private StringHashFunction stringHash = new StringHashFunction();
	static private URIComputer uriComputer = new URIComputer();

    private TreeHashValue hash = null;
    
    public int position;

    public ElementIndexAdapter(int id) {
        this.position = id;
    }

    @Override
    final public boolean isAdapterForType(Object type) {
        return type == ElementIndexAdapter.class;
    }
    
    public void init() {
    	hash = null;
    }
    
    public TreeHashValue getTreeHash() {
    	if(this.hash == null) {
    		this.hash = computeHash((EObject) this.getTarget());
    	}
    	return this.hash;
    }
    
    
    static AccessBasedLRUCache<EClass, List<EStructuralFeature>> concernedFeaturesCache = new AccessBasedLRUCache<EClass, List<EStructuralFeature>>(500, 100, 0.75f);
    static Iterable<EStructuralFeature> getConcernedFeatures(EClass clazz) {
    	List<EStructuralFeature> results = concernedFeaturesCache.get(clazz);
    	if(results == null) {
    		results = new ArrayList<>(clazz.getEAllStructuralFeatures());
    		ListIterator<EStructuralFeature> itr = results.listIterator();
    		while(itr.hasNext()) {
    			EStructuralFeature feature = itr.next();
    			if(feature.isDerived() || feature.isTransient() || feature.isVolatile()) itr.remove();
    			else if(feature instanceof EReference) {
    				if(((EReference) feature).isContainer()) itr.remove();
    			}
    		}
    	}
    	return results;
    }
    
    static private void serializeEObject(EObject target, Checksum checksum) {
    	final EClass clazz = target.eClass();
		ChecksumUtil.update(checksum, clazz.getName());
		ChecksumUtil.update(checksum, ":{");
		
    	for(EStructuralFeature feature : getConcernedFeatures(clazz)) {
            if(target.eIsSet(feature)==false) continue;
            Object val = target.eGet(feature);
            if(feature instanceof EReference) {
                if(((EReference)feature).isContainment() == false) {
                	ChecksumUtil.update(checksum, feature.getName());
                	ChecksumUtil.update(checksum, ':');
                    serializeEValue(val,checksum);
                }
            } else {
            	ChecksumUtil.update(checksum, feature.getName());
            	ChecksumUtil.update(checksum,':');
                serializeValue(val,checksum);
            }
            ChecksumUtil.update(checksum, ',');
        }
        ChecksumUtil.update(checksum, '}');
    }
    
    static private void serializeEValue(Object value, Checksum checksum) {
        if (value instanceof EList) {
        	ChecksumUtil.update(checksum, '[');
            for (Object v : (EList<?>) value) {
                serializeReferencedEObject((EObject) v, checksum);
                ChecksumUtil.update(checksum, ',');
            }
            ChecksumUtil.update(checksum, ']');
        } else if (value instanceof EObject) {
            serializeReferencedEObject((EObject) value, checksum);
        }
    }
    
    static private void serializeValue(Object value, Checksum checksum) {
        if(value instanceof Enumerator) {
            serializeEnumValue((Enumerator) value, checksum);
        } else if(value instanceof FeatureMap) {
            serializeFeatureMap((FeatureMap) value, checksum);
        } else if(value instanceof List) {
            serializeListValue((List<?>) value, checksum);
        } else {
            serializePrimitiveValue(value, checksum);
        }
    }
    
    static private void serializePrimitiveValue(Object value, Checksum checksum) {
        if(value == null) {
            ChecksumUtil.update(checksum, "null");
        } else {
            Class<?> valueType = value.getClass();
            if (valueType == Integer.class) {
                ChecksumUtil.update(checksum, ((Integer) value).intValue());
            } else if (valueType == Boolean.class) {
                ChecksumUtil.update(checksum, ((Boolean) value).booleanValue());
            } else if (valueType == String.class) {
                ChecksumUtil.update(checksum, ((String) value));
            } else if (valueType == Long.class) {
                ChecksumUtil.update(checksum, ((Long) value).longValue());
            } else if (valueType == Double.class) {
                ChecksumUtil.update(checksum, ((Double) value).doubleValue());
            } else if (valueType == Float.class) {
                ChecksumUtil.update(checksum, ((Float) value).floatValue());
            } else if (valueType == Byte.class) {
                ChecksumUtil.update(checksum, ((Byte) value).byteValue());
            } else if (valueType == Character.class) {
                ChecksumUtil.update(checksum, ((Character) value).charValue());
            } else if (valueType == Short.class) {
                ChecksumUtil.update(checksum, ((Short) value).shortValue());
            } else {
                ChecksumUtil.update(checksum, valueType.getName());
            }
        }
    }

    static private void serializeReferencedEObject(EObject value, Checksum checksum) {
        Iterable<String> iterables = uriComputer.getOrComputeLocation(value);
        for(String frag : iterables) {
            ChecksumUtil.update(checksum, frag);
            ChecksumUtil.update(checksum, '/');
        }
    }

    static private void serializeListValue(List<?> value, Checksum checksum) {
        ChecksumUtil.update(checksum, '[');
        for(Object v : value) {
            serializeValue(v, checksum);
            ChecksumUtil.update(checksum, ',');
        }
        ChecksumUtil.update(checksum, ']');
    }

    static private void serializeFeatureMap(FeatureMap featureMap, Checksum checksum) {
        ChecksumUtil.update(checksum, "{<");
        for(FeatureMap.Entry entry : featureMap) {
            ChecksumUtil.update(checksum, entry.getEStructuralFeature().getName());
            ChecksumUtil.update(checksum, "->");
            serializeValue(entry.getValue(), checksum);
            ChecksumUtil.update(checksum, ',');
        }
        ChecksumUtil.update(checksum, ">}");
    }

    static private void serializeEnumValue(Enumerator value, Checksum checksum) {
        ChecksumUtil.update(checksum, ((Enumerator)value).getLiteral());
    }

	
    private TreeHashValue computeHash(EObject target) {
		TreeHashValue hash = new TreeHashValue();
		
		CRC64 subtreeChecksum = new CRC64();
		CRC32 subtreeStructureChecksum = new CRC32();
		
		// Step 1. Build local content checksum and local content simhash
		
		serializeEObject(target, subtreeChecksum);
		hash.subtreeChecksum = subtreeChecksum.getValue();
		hash.localContentHash = buildLocalSimhash(target);
		hash.lchBits = TreeHashValue.computeBitCounts(hash.localContentHash);
		
		
		// Step 2. Build tree structure checksum, and tree content simhash
        subtreeChecksum.reset();        

		ChecksumUtil.update(subtreeChecksum, hash.subtreeChecksum);
		ChecksumUtil.update(subtreeChecksum, '{');
		
		ChecksumUtil.update(subtreeStructureChecksum, target.eClass().getName());
		ChecksumUtil.update(subtreeStructureChecksum, '{');
		
		List<TreeHashValue> childHashes = new ArrayList<TreeHashValue>(target.eContents().size());
		
		for (EObject child : target.eContents()) {
			ElementIndexAdapter childAdapter = ElementIndexAdapterFactory.getAdapter(child);
			if (childAdapter != null) {
				TreeHashValue childTreeHash = childAdapter.getTreeHash();				
				ChecksumUtil.update(subtreeChecksum, childTreeHash.subtreeChecksum);
				ChecksumUtil.update(subtreeStructureChecksum, childTreeHash.subtreeStructureChecksum);
				
				hash.size += childTreeHash.size;
				if(childTreeHash.height + 1 > hash.height)
					hash.height = childTreeHash.height + 1;
				
				childHashes.add(childTreeHash);
			}
		}
		
		ChecksumUtil.update(subtreeChecksum, '}');
		ChecksumUtil.update(subtreeStructureChecksum, '}');
		
		hash.subtreeChecksum = subtreeChecksum.getValue();
		hash.subtreeStructureChecksum = (int) subtreeStructureChecksum.getValue();
		
		hash.childContentHash = convHashes(hash, childHashes);
		hash.cchBits = TreeHashValue.computeBitCounts(hash.childContentHash);
		
		return hash;
	}
	
    static final float[] bitarray = new float[64];
    
    private long convHashes(TreeHashValue parent, List<TreeHashValue> childHashes) {
    	
    	if(childHashes.size() == 0) {
    		return 0;
    	} else if(childHashes.size() == 1) {
    		TreeHashValue c = childHashes.get(0);
    		return c.localContentHash | c.childContentHash;
    	} else {
    		final int totalWeight = (parent.size - 1) * 64;
    		int factoredWeightSum = 0;
    		
    		// factor = \sum_i=1^N(w_i*p_i)/(\sum_i=1^N(w_i*(1-p_i)))
    		// p_i is approximated by #bits_i/64
    		// MAYBE factor_i = p_i/(1-p_i) = #bits / (64 - #bits) \in [0, 64]
    		for(TreeHashValue h : childHashes) {
    			final int bitcount = TreeHashValue.computeBitCounts(h.localContentHash);
    			final float weight = h.size;
    			factoredWeightSum += weight * bitcount;
    		}
    		
    		final float factor;
    		if(factoredWeightSum == 0) return 0;
    		
    		if(totalWeight == factoredWeightSum) factor = 0;
    		else factor = 1.0f / (((float)totalWeight)/factoredWeightSum - 1.0f);
    		
    		Arrays.fill(bitarray, 0);
    		for(TreeHashValue h : childHashes) {
    			final float weight = h.size;
    			final float nWeight = weight * factor;
				fillBitarray(bitarray, h.localContentHash, weight, nWeight);
    			fillBitarray(bitarray, h.childContentHash, weight / 10, nWeight / 10);
    		}
    		return zipBitarray(bitarray);
    	}
    	
	}

	static private void fillBitarray(final float[] bitarray, final long content, final float pWeight, final float nWeight) {
		for(int i=0; i<64; i++) {
			if((content & (1L<<i)) == 0) {
				bitarray[i] -= nWeight;
			} else {
				bitarray[i] += pWeight;
			}
		}
	}
	
	static private long zipBitarray(final float[] bitarray) {
		long value = 0;
		for(int i=0;i<64;i++) {
			if(bitarray[i] >= 0) {
				value |= (1L << i);
			}
		}
		return value;
	}
	
	protected boolean shouldHash(EStructuralFeature feature) {
		return !(feature.isDerived() || feature.isTransient() || feature.isVolatile());
	}

	private long buildLocalSimhash(EObject o) {
        long hash = 0L;
        long nHash = 0L;
        boolean flag = false;
        
        for(EStructuralFeature feature : o.eClass().getEAllAttributes()) {
        	if(shouldHash(feature)) {       		
        		Class<?> instanceClass = feature.getEType().getInstanceClass();
        		if(instanceClass==String.class) {
        			flag = true;
        			if(feature.isMany()) {
        				@SuppressWarnings("unchecked")
        				List<String> strings = (List<String>) o.eGet(feature);
        				for(String str : strings) {
        					hash |= (1L << (Math.abs(str.hashCode() % 64)));
        				}
        			} else {
        				String value = (String) o.eGet(feature);
        				if(value != null)
        					hash |= stringHash.hash(null, value);
        			}
        		}
        	}
        }
        
        if(!flag) {
        	for(EReference feature : o.eClass().getEAllReferences()) {
        		if(shouldHash(feature) && feature.isContainment()==false && feature.isContainer()==false) {
        			if(feature.isMany()) {
        				@SuppressWarnings("unchecked")
        				List<EObject> targets = (List<EObject>) o.eGet(feature);
        				for(EObject tar : targets) {
        					long hashTar = hashRef(tar);
        					nHash |= (1L << (Math.abs(hashTar % 64)));
        				}
        			} else {
        				EObject tar = (EObject) o.eGet(feature);
        				if(tar != null) {
        					long hashTar = hashRef(tar);
        					nHash |= (1L << (Math.abs(hashTar % 64)));
        				}
        			}
        		}
        	}
        }
            
        return hash | nHash;
    }

	static AccessBasedLRUCache<EObject, Long> refCache = new AccessBasedLRUCache<EObject, Long>(100,50,0.75f);
	static private long hashRef(EObject tar) {
		Long c = refCache.get(tar);
		if(c == null) {
			FragmentIterable itr = uriComputer.getOrComputeLocation(tar);
			long code = 0;
			for(String frag : itr) {
				code = code * 31 + frag.hashCode();
			}
			refCache.put(tar, code);
			return code;			
		} else return c;
	}
    
//    public long getSubtreeIdentityHash() {
//        if(treeIdentityHash == TreeHashValue.INVALID_HASH) {
//            if(localIdentityHash == TreeHashValue.INVALID_HASH) return TreeHashValue.INVALID_HASH;
//            CRC64 crc = new CRC64();
//            crc.update(localIdentityHash);
//            crc.update("{");
//            EObject object = (EObject) this.getTarget();
//            // long childHashes = 0L;
//            for(EObject child : object.eContents()) {
//                ElementIndexAdapter childAdapter = ElementIndexAdapter.getAdapter(child);
//                if(childAdapter != null) {
//                    long childTreeHash = childAdapter.getSubtreeIdentityHash();
//                    if(childTreeHash != TreeHashValue.INVALID_HASH) {
//                        crc.update(childTreeHash);
//                        // childHashes ^= childTreeHash;
//                    }
//                    if(childAdapter.height + 1 > height)
//                        height = childAdapter.height + 1;
//                }
//            }
//            // CommonUtils.update(crc32, childHashes);
//            crc.update("}");
//            treeIdentityHash = crc.getValue();
//        }
//
//        return treeIdentityHash;
//    }

    
    

    

//    private long getChildrenSimhash() {
//        return treeSimHash.high;
//    }
//
//    public long getTreeStructuralChecksum() {
//        if(treeStructuralChecksum == TreeHashValue.INVALID_HASH) {
//            EObject object = (EObject) this.getTarget();
//            long high = buildLocalSimhash(object);
//            long low = 0L;
//            CRC32 crc32 = new CRC32();
//            CommonUtils.update(crc32, object.eClass().getName());
//            CommonUtils.update(crc32, "{");
//            for(EObject child : object.eContents()) {
//                ElementIndexAdapterWithStructuralChecksum childAdapter = (ElementIndexAdapterWithStructuralChecksum) ElementIndexAdapter.getAdapter(child);
//                if(childAdapter != null) {
//                    long childTreeHash = childAdapter.getTreeStructuralChecksum();
//                    if(childTreeHash != TreeHashValue.INVALID_HASH) {
//                        CommonUtils.update(crc32, childTreeHash);
//                    }
//                    size += childAdapter.size;
//                    low |= childAdapter.getChildrenSimhash();
//                }
//            }
//            CommonUtils.update(crc32, "}");
//            treeStructuralChecksum = crc32.getValue();
//            treeSimHash = new HashLoHi(high, low);
//        }
//
//        return treeStructuralChecksum;
//    }
//
//    private long buildLocalSimhash(EObject o) {
//        long hash = 0L;
//        for(EStructuralFeature feature : o.eClass().getEAllAttributes()) {
//            if(feature.isMany()==false && feature.getEType().getInstanceClass()==String.class) {
//                String value = (String) o.eGet(feature);
//                if(value != null)
//                    hash |= stringHash.hash(null, value);
//            }
//        }
//        return hash;
//    }
}
