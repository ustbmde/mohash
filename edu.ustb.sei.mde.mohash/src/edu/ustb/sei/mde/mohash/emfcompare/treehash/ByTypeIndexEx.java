package edu.ustb.sei.mde.mohash.emfcompare.treehash;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.match.eobject.EObjectIndex;
import org.eclipse.emf.compare.match.eobject.ProximityEObjectMatcher;
import org.eclipse.emf.compare.match.eobject.ProximityEObjectMatcher.DistanceFunction;
import org.eclipse.emf.compare.match.eobject.ScopeQuery;
import org.eclipse.emf.compare.match.eobject.internal.MatchAheadOfTime;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@SuppressWarnings("restriction")
public class ByTypeIndexEx implements EObjectIndex, MatchAheadOfTime {
	/**
	 * All the type specific indexes, created on demand.
	 */
	private Map<EClass, ProximityIndexEx> allIndexes;

	/**
	 * The distance function to use to create the delegates indexes.
	 */
	private DistanceFunction meter;

	/**
	 * An instance responsible for telling whether something is in the scope or not.
	 */
	private ScopeQuery scope;

	/**
	 * Create a new instance using the given {@link DistanceFunction} to instantiate delegate indexes on
	 * demand.
	 * 
	 * @param meter
	 *            the function passed when instantiating delegate indexes.
	 * @param scope
	 *            an instance
	 */
	public ByTypeIndexEx(ProximityEObjectMatcher.DistanceFunction meter, final ScopeQuery scope) {
		this.meter = meter;
		this.scope = scope;
		this.allIndexes = Maps.newHashMap();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.compare.match.eobject.EObjectIndex#getValuesStillThere(org.eclipse.emf.compare.match.eobject.EObjectIndex.Side)
	 */
	public Iterable<EObject> getValuesStillThere(Side side) {
		List<Iterable<EObject>> allLists = Lists.newArrayList();
		for (EObjectIndex typeSpecificIndex : allIndexes.values()) {
			allLists.add(typeSpecificIndex.getValuesStillThere(side));
		}
		return Iterables.concat(allLists);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.compare.match.eobject.EObjectIndex#findClosests(org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.compare.match.eobject.EObjectIndex.Side, int)
	 */
	@Deprecated
	public Map<Side, EObject> findClosests(Comparison inProgress, EObject obj, Side side) {
		EObjectIndex typeSpecificIndex = getOrCreate(obj);
		return typeSpecificIndex.findClosests(inProgress, obj, side);
	}

	/**
	 * Get the index used to store this object, create a new one if needed.
	 * 
	 * @param obj
	 *            any EObject.
	 * @return the index used to store this object, create a new one if needed.
	 */
	private ProximityIndexEx getOrCreate(EObject obj) {
		EClass key = obj.eClass();
		ProximityIndexEx found = allIndexes.get(key);
		if (found == null) {
			found = new ProximityIndexEx(meter, scope);
			allIndexes.put(key, found);
		}
		return found;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.compare.match.eobject.EObjectIndex#remove(org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.compare.match.eobject.EObjectIndex.Side)
	 */
	public void remove(EObject obj, Side side) {
		EObjectIndex typeSpecificIndex = getOrCreate(obj);
		typeSpecificIndex.remove(obj, side);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.compare.match.eobject.EObjectIndex#index(org.eclipse.emf.ecore.EObject,
	 *      org.eclipse.emf.compare.match.eobject.EObjectIndex.Side)
	 */
	public void index(EObject eObjs, Side side) {
		EObjectIndex typeSpecificIndex = getOrCreate(eObjs);
		typeSpecificIndex.index(eObjs, side);
	}

	/**
	 * {@inheritDoc}
	 */
	public Iterable<EObject> getValuesToMatchAhead(Side side) {
		List<Iterable<EObject>> allLists = Lists.newArrayList();
		for (MatchAheadOfTime typeSpecificIndex : Iterables.filter(allIndexes.values(),
				MatchAheadOfTime.class)) {
			allLists.add(typeSpecificIndex.getValuesToMatchAhead(side));
		}
		return Iterables.concat(allLists);
	}
	
	public Map<Side, EObject> findClosests(Comparison inProgress, EObject eObj, Side side, Match partialMatchOfEObj) {
		EClass clazz = eObj.eClass();
		ProximityIndexEx subIndex = allIndexes.get(clazz);
        if(subIndex == null) return Collections.emptyMap();
        else return subIndex.findClosests(inProgress, eObj, side, partialMatchOfEObj);
	}
	
	public Match findIdenticalSubtrees(Comparison inProgress, EObject eObj, Side side, Match partialMatchOfEObj, Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots) {
		EClass clazz = eObj.eClass();
        ProximityIndexEx subIndex = allIndexes.get(clazz);
        if(subIndex == null) return null;
        else return subIndex.findIdenticalSubtrees(inProgress, eObj, side, partialMatchOfEObj, roots);
	}

}
