package edu.ustb.sei.mde.mohash.emfcompare.parallel;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.compare.CompareFactory;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.match.eobject.EObjectIndex.Side;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mohash.factory.MoHashMatchEngineFactory;

public class Scheduler {
	public Scheduler(Side side, int numberOfThreads, ParallelHashBasedEObjectIndex index,
			Iterator<EObject> objectsToMatch, Comparison comparison, boolean createUnmatches) {
		super();
		this.side = side;
		this.numberOfThreads = numberOfThreads;
		this.index = index;
		this.objectsToMatch = objectsToMatch;
		this.comparison = comparison;
		this.createUnmatches = createUnmatches;
		
		this.boardA = new Scoreboard(this, numberOfThreads);
		this.boardB = new Scoreboard(this, numberOfThreads);
	}
	
	public Collection<EObject> start(ExecutorCompletionService<Integer> executor) {
		SortedMap<Integer, EObject> remainingObjects = new TreeMap<Integer, EObject>();

		for(int i=0; i<numberOfThreads; i++) {
			executor.submit(new MatchTask(this, i, remainingObjects), i);
		}
		try {
			for(int i=0; i<numberOfThreads; i++) {
				try {
					executor.take();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return remainingObjects.values();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private Side side;
	private int numberOfThreads = MoHashMatchEngineFactory.numberOfThreads;
	private ParallelHashBasedEObjectIndex index;
	private Iterator<EObject> objectsToMatch;
	private Comparison comparison;
	
	
	private volatile int nextID = 0;
	private volatile int round = 0;
	
	private Map<EObject, Integer> objectIDMap = new ConcurrentHashMap<EObject, Integer>(2048);
	public int register(EObject object, int id) {
		return objectIDMap.computeIfAbsent(object, o -> id);
	}
	
	
	private Scoreboard boardA;
	private Scoreboard boardB;
	
	public Scoreboard getBoardA() {
		return boardA;
	}
	
	public Scoreboard getBoardB() {
		return boardB;
	}
	
	boolean createUnmatches;
	
	private void startObject(int objectID) {
		boardA.objectStarting(objectID);
		boardB.objectStarting(objectID);
	}
	
	synchronized public Pair<Integer,EObject> pickNext(int threadID) {
		if(round * numberOfThreads + threadID == nextID) {
			int objectID = nextID;
			EObject next = null;
			do{
				if(objectsToMatch.hasNext() == false) return null;
				next = objectsToMatch.next();
				objectID = register(next, nextID);
			} while(objectID != nextID);
			nextID ++;
			startObject(objectID); // whenever an element is picked, it starts
			if(nextID % numberOfThreads == 0) round ++;
			return new Pair<>(objectID, next);
		} else {
			return null;
		}
	}
	
	synchronized private boolean hasNext() {
		return objectsToMatch.hasNext();
	}
	
	class Pair<F,S> {
		public Pair(F first, S second) {
			super();
			this.first = first;
			this.second = second;
		}
		final public F first;
		final public S second;
	}
	
	class MatchTask implements Runnable {
		public MatchTask(Scheduler schedular, int threadID, SortedMap<Integer, EObject> remainingResult) {
			super();
			this.schedular = schedular;
			this.threadID = threadID;
			this.remainingResult = remainingResult;
		}

		final private Scheduler schedular;
		final private int threadID;
		final private SortedMap<Integer, EObject> remainingResult;
		
		private int objectID;
		
		@Override
		public void run() {
			try {
				while(schedular.hasNext()) {
					Pair<Integer, EObject> pair = schedular.pickNext(threadID);
					if(pair!=null) {
						this.objectID = pair.first;
						EObject objectToMatch = pair.second;
						if(comparison.getMatch(objectToMatch) == null) {
							if(!tryMatching(objectToMatch)) {
								synchronized (remainingResult) {
									remainingResult.put(objectID, objectToMatch);
								}
							}
						}
					} else Thread.yield();
				}
			} catch (Exception e) {
				System.out.println(threadID);
				e.printStackTrace();
			}
		}
		
		protected boolean tryMatching(EObject a) {
			boolean okToMatch = false;
			Side aSide = side;
			assert aSide != null;
			Side bSide = Side.LEFT;
			Side cSide = Side.RIGHT;
			if (aSide == Side.RIGHT) {
				bSide = Side.LEFT;
				cSide = Side.ORIGIN;
			} else if (aSide == Side.LEFT) {
				bSide = Side.RIGHT;
				cSide = Side.ORIGIN;
			} else if (aSide == Side.ORIGIN) {
				bSide = Side.LEFT;
				cSide = Side.RIGHT;
			}
			
			Map<Side, EObject> closests = index.findClosests(threadID, comparison, a, aSide, objectID, schedular);
			if (closests != null) {
				EObject lObj = closests.get(bSide);
				EObject aObj = closests.get(cSide);
				if (lObj != null || aObj != null) {
					// we have at least one other match
					areMatching(closests.get(Side.LEFT), closests.get(Side.RIGHT),
							closests.get(Side.ORIGIN));
					okToMatch = true;
				} else if (createUnmatches) {
					areMatching(closests.get(Side.LEFT), closests.get(Side.RIGHT),
							closests.get(Side.ORIGIN));
					okToMatch = true;
				}
			}
			schedular.finishObject(objectID);
			return okToMatch;
		}
		
		protected Match areMatching(EObject left, EObject right, EObject origin) {
			Match result = CompareFactory.eINSTANCE.createMatch();

			synchronized(comparison) {
				result.setLeft(left);
				result.setRight(right);
				result.setOrigin(origin);			
				((BasicEList<Match>)comparison.getMatches()).addUnique(result);
			}
							
			if (left != null) {
				index.remove(left, Side.LEFT);
			}
			if (right != null) {
				index.remove(right, Side.RIGHT);
			}
			if (origin != null) {
				index.remove(origin, Side.ORIGIN);
			}

			return result;
		}
	}

	final public Integer getObjectID(EObject eObj) {
		return objectIDMap.get(eObj);
	}

	public void finishObject(int objectID) {
		boardA.objectFinshed(objectID);
		boardB.objectFinshed(objectID);
	}
}
