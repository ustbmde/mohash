package edu.ustb.sei.mde.mohash.emfcompare;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.internal.utils.DiffUtil;
import org.eclipse.emf.compare.match.eobject.EditionDistance;
import org.eclipse.emf.compare.match.eobject.ProximityEObjectMatcher.DistanceFunction;
import org.eclipse.emf.compare.match.eobject.WeightProvider;
import org.eclipse.emf.compare.match.eobject.WeightProvider.Descriptor.Registry;
import org.eclipse.emf.compare.match.eobject.internal.AccessBasedLRUCache;
import org.eclipse.emf.compare.utils.ReferenceUtil;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.FeatureMap;

import edu.ustb.sei.mde.mohash.EObjectSimHasherEx;

@SuppressWarnings("restriction")
public class ReasonableCachingDistanceEx extends ReasonableCachingDistance {

	public ReasonableCachingDistanceEx(EditionDistance wrapped) {
		super(wrapped);

		try {
			Field field = EditionDistance.class.getDeclaredField("locationChangeCoef");
			field.setAccessible(true);
			this.locationChangeCoef = (Integer) field.get(this.wrapped);
			field.setAccessible(false);
		} catch (Exception e) {
		}
		
		try {
			Field field = EditionDistance.class.getDeclaredField("weightProviderRegistry");
			field.setAccessible(true);
			this.weightProviderRegistry = (Registry) field.get(this.wrapped);
			field.setAccessible(false);
		} catch (Exception e) {
		}
	}

	@SuppressWarnings("unused")
	private int locationChangeCoef;
	private WeightProvider.Descriptor.Registry weightProviderRegistry;
	
	private Map<TupleKey<Long, EClass>, HeuristicCache> heuristicCache = new AccessBasedLRUCache<TupleKey<Long, EClass>, HeuristicCache>(10000, 1000, .75F);
	private Map<EObject, Long> objectDigestMap = new HashMap<>(4096);
	
	public void recordDigest(EObject object) {
		long digest = EObjectSimHasherEx.getRecentDigest();
		objectDigestMap.put(object, digest);
	}
	
	long getDigest(EObject object) {
		return objectDigestMap.getOrDefault(object, 0L);
	}
	
	static private int indexFromFeatureMap(EObject a, EStructuralFeature feat, EObject container) {
		FeatureMap featureMap = (FeatureMap)ReferenceUtil.safeEGet(container, feat);
		for (int i = 0, size = featureMap.size(); i < size; ++i) {
			if (featureMap.getValue(i) == a) {
				EStructuralFeature entryFeature = featureMap.getEStructuralFeature(i);
				if (DiffUtil.isContainmentReference(entryFeature)) {
					return i;
				}
			}
		}
		return 0;
	}
	
	static private int getContainmentIndex(EObject a) {
		EStructuralFeature feat = a.eContainingFeature();
		EObject container = a.eContainer();
		int position = 0;
		if (container != null) {
			if (feat instanceof EAttribute) {
				position = indexFromFeatureMap(a, feat, container);
			} else if (feat != null) {
				if (feat.isMany()) {
					EList<?> eList = (EList<?>)ReferenceUtil.safeEGet(container, feat);
					position = eList.indexOf(a);
				}
			}
		}
		return position;
	}
	
	protected double containerOrderDiff(EObject a, EObject b, boolean sameContainer) {
		double changes = 0;
		
		if(sameContainer) {
			int aIndex = getContainmentIndex(a);
			int bIndex = getContainmentIndex(b);
			
			if (aIndex != bIndex) {
				changes = 5;
			} else changes = 0;
		}
		
		if (a.eContainingFeature() != b.eContainingFeature()) {
			changes += Math.max(
					weightProviderRegistry.getHighestRankingWeightProvider(a.eClass().getEPackage())
							.getContainingFeatureWeight(a),
					weightProviderRegistry.getHighestRankingWeightProvider(b.eClass().getEPackage())
							.getContainingFeatureWeight(b));
		}
		
		return changes;
	}
	
	class HeuristicCache {
		public double[] distances;
		public EObject[] belief;
		public double maxDist;
	}
	
	
//	int heuMiss = 0;
//	int heuHits = 0;
//	int heuTrys = 0;
//	int heuDer = 0;

//	public boolean areIdentic(Comparison inProgress, EObject a, EObject b) {
//		if(!((EditionDistanceEx)wrapped).haveSameContainer(inProgress, a, b)) return false;
//		else if(this.containerOrderDiff(a, b, false)!=0) return false;
//		else {
//			long da = getDigest(a);
//			long db = getDigest(b);
//			return da == db;
//		}
////		return super.areIdentic(inProgress, a, b);
//	}
	
	
	public double distance(Comparison inProgress, EObject a, EObject b, boolean sameContainer) {
		TupleKey<EObject, DistanceFunction> key = new TupleKey<EObject, DistanceFunction>(a, b, this, TupleKey.otherPred);
		Double previousResult = distanceCache.get(key);
		if (previousResult == null) {
//			heuTrys ++;
			TupleKey<Long, EClass> tuple = new TupleKey<>(getDigest(a), getDigest(b), a.eClass(), TupleKey.longPred);
			
			HeuristicCache hPreviousResults = heuristicCache.computeIfAbsent(tuple, k -> {
				HeuristicCache cache = new HeuristicCache();
				cache.distances = new double[] {-1, -1};
				cache.belief = new EObject[] {a, b};
				cache.maxDist = Math.max(wrapped.getThresholdAmount(a), wrapped.getThresholdAmount(b));
				return cache;
			});
			final int pos = sameContainer ? 0 : 1;
			double containerDiff = this.containerOrderDiff(a, b, sameContainer);
			
			double hCachedDist = hPreviousResults.distances[pos];
			if(hCachedDist!=-1) {
				double retDist = 0;
				if(hCachedDist == Double.MAX_VALUE) 
					retDist = hCachedDist;
				else {
					retDist = hCachedDist + containerDiff;
					if(retDist > hPreviousResults.maxDist) retDist = Double.MAX_VALUE;
				}
//				heuHits ++;
				return retDist;
			} else {
//				heuMiss ++;
//				if(hPreviousResults.distances[(pos + 1) % 2] != -1) heuDer ++;
			}
			
			double dist = wrapped.distance(inProgress, a, b);				
			distanceCache.put(key, Double.valueOf(dist));
			
			double cachedDist = dist;
			if(dist > 0 && dist < Double.MAX_VALUE) {
				cachedDist -= containerDiff;
				assert cachedDist >= 0;
			}
			hPreviousResults.distances[pos] = cachedDist;
			
			return dist;
		}
		return previousResult.doubleValue();
	}
	
}
