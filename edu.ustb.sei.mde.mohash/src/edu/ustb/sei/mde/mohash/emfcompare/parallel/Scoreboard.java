package edu.ustb.sei.mde.mohash.emfcompare.parallel;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.emf.ecore.EObject;

/**
 * Scoreboard is related to side
 * @author dr-he
 *
 */
public class Scoreboard {
	public Scoreboard(Scheduler schedular, int threads) {
		super();
		this.schedular = schedular;
		this.threads = threads;
	}

	static public final Integer UNHANDLED = 0;
	static public final Integer STARTING = 1;
	static public final Integer VALIDATING = 2;
	static public final Integer FINISHED = 3;
	
	
	
	private Scheduler schedular;
	
	private int threads = 8;
	
	public int getNumberOfThreads() {
		return threads;
	}

	private Map<Integer, Integer> objectStatus = new ConcurrentHashMap<>(2048);
	
	/**
	 * The waiting list of every element.
	 * If the list contains -1, then the element is occupied.
	 */
	private Map<EObject, SortedSet<Integer>> waitingList = new HashMap<>(2048);
	
	
	
	public int getObjectStatus(int id) {
		return objectStatus.getOrDefault(id, UNHANDLED);
	}
	
	public void objectStarting(int id) {
		objectStatus.put(id, STARTING);
	}
	
	public void objectValidating(int id) {
		objectStatus.put(id, VALIDATING);
	}
	
	public void objectFinshed(int id) {
		objectStatus.put(id, FINISHED);
	}
	
	/**
	 * 
	 * @param o
	 * @return null if it is free, negative number if it is locked, or zero-based natural number if it is waited by someone
	 */
	synchronized public Integer getWaitingStatus(EObject o) {
		SortedSet<Integer> s = waitingList.get(o);
		if(s==null || s.isEmpty()) return null; // o is not waited or locked by any other elements
		else {
			Iterator<Integer> it = s.iterator();
			while(it.hasNext()) {
				int waited = it.next();
				if(waited >= 0) {
					if(getObjectStatus(waited) == FINISHED) 
						it.remove();
					else return waited;
				} else {
					return waited;
				}
			}
			return -1;
//			throw new Error("This should not happen because the object asking this question should be in the list");
		}
	}
	
	synchronized public void free(EObject o, int i) {
		SortedSet<Integer> set = waitingList.getOrDefault(o, Collections.emptySortedSet());
		set.remove(i);
	}
	
	synchronized public void lock(EObject o) {
		if(o==null) return;
		SortedSet<Integer> set = waitingList.getOrDefault(o, null);
		set.add(-1);
	}
	
	synchronized public void addToWaitingList(EObject o, int i) {
		SortedSet<Integer> set = waitingList.computeIfAbsent(o, k->new TreeSet<>());
		set.add(i);
	}
	
	public boolean isPreviousSearched(int hostID) {
		for(int prev = hostID - 1; prev >=0 && (hostID - prev) < threads ; prev --) {
			int status = this.getObjectStatus(prev);
			if(status <= STARTING) return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return objectStatus.toString() + "\n" + waitingList.toString();
	}

	public int getObjectStatus(EObject eObj) {
		Integer id = schedular.getObjectID(eObj);
		if(id==null) return -1;
		return this.objectStatus.getOrDefault(id, UNHANDLED);
	}
	
	final public boolean isProcessing(EObject eObj) {
		int status = getObjectStatus(eObj);
		return status >= 0 && status != Scoreboard.FINISHED;
	}

	final public boolean isProcessing(int id) {
		int status = this.objectStatus.getOrDefault(id, UNHANDLED);
		return status >= 0 && status != Scoreboard.FINISHED;
	}
	
	final public Integer getObjectID(EObject object) {
		if(object == null) return null;
		return schedular.getObjectID(object);
	}
	
	
	final public void waitUntilFormerIsFinished(EObject former, EObject requester, String tail) {
		final Integer formerID = getObjectID(former);
		if(formerID == null) return;
		final int requesterID = getObjectID(requester); 
		waitUntilFormerIsFinished(formerID, requesterID, tail);
	}
	
	final public void waitUntilFormerIsFinished(EObject former, int requesterID) {
		final Integer formerID = getObjectID(former);
		if(formerID == null) return;
		waitUntilFormerIsFinished(formerID, requesterID, "");
	}
	
	final public void waitUntilFormerIsFinished(int formerID, int requesterID, String tail) {
		if(formerID >= requesterID) return;
		int repeated = 0;
		while(this.isProcessing(formerID)) {
//			ParallelHashBasedEObjectIndex.log(requesterID + " is waiting for "+formerID + " (" + tail +")");
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			};
			repeated ++;
//			if(repeated > 1000000000)
//				throw new RuntimeException("Too many repeated!");
		}
	}
}
