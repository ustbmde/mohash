package edu.ustb.sei.mde.mohash.emfcompare;

import java.util.HashMap;

public class Bag<T> extends HashMap<T, Integer> {
	private static final long serialVersionUID = 1L;
	
	public int add(T element) {
		return this.compute(element, (e, s) -> {
			if(s==null) return 1;
			else return s + 1;
		});
	}
	
	public Integer decrese(T element) {
		Integer v = this.compute(element, (e, s) -> {
			if(s==null || s == 1) return null;
			else return s - 1;
		});
		if(v==null) return 0;
		else return v;
	}
	
	@Override
	public Integer get(Object key) {
		return super.getOrDefault(key, 0);
	}
}
