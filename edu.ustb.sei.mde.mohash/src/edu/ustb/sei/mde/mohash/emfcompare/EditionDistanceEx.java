package edu.ustb.sei.mde.mohash.emfcompare;

import java.lang.reflect.Field;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.match.eobject.EditionDistance;
import org.eclipse.emf.compare.match.eobject.WeightProvider.Descriptor.Registry;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mohash.functions.AccessBasedLRUCache;

public class EditionDistanceEx extends EditionDistance {
	
	private AccessBasedLRUCache<EObject, Double> thresholdAmountMap = new AccessBasedLRUCache<EObject, Double>(1000, 500, 0.75f);
	private Comparison fakeComparison;
	
	protected void init() {
		try {
			Field field = EditionDistance.class.getDeclaredField("fakeComparison");
			field.setAccessible(true);
			fakeComparison = (Comparison) field.get(this);
			field.setAccessible(false);
		} catch (Exception e) {
		}
	}

	public EditionDistanceEx() {
		init();
	}

	public EditionDistanceEx(Registry weightProviderRegistry) {
		super(weightProviderRegistry);
		init();
	}

	public EditionDistanceEx(Registry weightProviderRegistry,
			org.eclipse.emf.compare.match.eobject.EqualityHelperExtensionProvider.Descriptor.Registry equalityHelperExtensionProviderRegistry) {
		super(weightProviderRegistry, equalityHelperExtensionProviderRegistry);
		init();
	}

	@Override
	public double getThresholdAmount(EObject eObj) {
		return thresholdAmountMap.computeIfAbsent(eObj, e->{
			return super.getThresholdAmount(e);
		});
	}
	
	private boolean isReferencedByTheMatch(EObject eObj, Match match) {
		return match != null
				&& (match.getRight() == eObj || match.getLeft() == eObj || match.getOrigin() == eObj);
	}
	
	public boolean haveSameContainer(Comparison inProgress, EObject a, EObject b) {
		EObject aContainer = a.eContainer();
		EObject bContainer = b.eContainer();
		if ((aContainer == null && bContainer != null) || (aContainer != null && bContainer == null)) {
			return false;
		}
		/*
		 * we consider two null containers as being the "same".
		 */
		boolean matching = aContainer == null && bContainer == null;
		if (!matching) {
			Match mA = inProgress.getMatch(aContainer);
			Match mB = inProgress.getMatch(bContainer);
			if (mA == null && mB == null) {
				/*
				 * The Objects have to be out of scope then.
				 */
				matching = fakeComparison.getEqualityHelper().matchingValues(aContainer, bContainer);
			} else {
				matching = isReferencedByTheMatch(bContainer, mA)
						|| isReferencedByTheMatch(aContainer, mB);
			}
		}
		return matching;
	}
	
	/*
	 * For profiling only
	 */
	
	public int idcheck = 0;
	public int distfun = 0;
	
	public void resetStats() {
		idcheck = 0;
		distfun = 0;
	}
	
	public int[] getExeCount() {
		return new int[] {idcheck, distfun};
	}
	
	@Override
	public boolean areIdentic(Comparison inProgress, EObject a, EObject b) {
		idcheck ++;
		return super.areIdentic(inProgress, a, b);
	}
	
	@Override
	public double distance(Comparison inProgress, EObject a, EObject b) {
		distfun ++;
		return super.distance(inProgress, a, b);
	}
}
