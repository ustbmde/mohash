package edu.ustb.sei.mde.mohash.emfcompare.parallel;

import java.lang.reflect.Field;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.match.eobject.EditionDistance;
import org.eclipse.emf.compare.match.eobject.EqualityHelperExtensionProviderDescriptorRegistryImpl;
import org.eclipse.emf.compare.match.eobject.WeightProviderDescriptorRegistryImpl;
import org.eclipse.emf.compare.match.eobject.ProximityEObjectMatcher.DistanceFunction;
import org.eclipse.emf.compare.match.eobject.URIDistance;
import org.eclipse.emf.compare.match.eobject.WeightProvider.Descriptor.Registry;
import org.eclipse.emf.ecore.EObject;

public class ParallelEditionDistance extends EditionDistance {

	public ParallelEditionDistance(URIDistance sharedURIDistance) {
		this(WeightProviderDescriptorRegistryImpl.createStandaloneInstance(),
				EqualityHelperExtensionProviderDescriptorRegistryImpl.createStandaloneInstance(), sharedURIDistance);
	}

	public ParallelEditionDistance(Registry weightProviderRegistry,
			org.eclipse.emf.compare.match.eobject.EqualityHelperExtensionProvider.Descriptor.Registry equalityHelperExtensionProviderRegistry, URIDistance sharedURIDistance) {
		super(weightProviderRegistry, equalityHelperExtensionProviderRegistry);
		
		try {
			Field field = EditionDistance.class.getDeclaredField("uriDistance");
			field.setAccessible(true);
			field.set(this, sharedURIDistance);
			field.setAccessible(false);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public ParallelEditionDistance(Registry weightProviderRegistry, URIDistance sharedURIDistance) {
		this(weightProviderRegistry,
				EqualityHelperExtensionProviderDescriptorRegistryImpl.createStandaloneInstance(), sharedURIDistance);
	}

	
}
