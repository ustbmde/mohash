package edu.ustb.sei.mde.mohash.emfcompare.treehash;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.Monitor;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.ComparisonCanceledException;
import org.eclipse.emf.compare.EMFCompareMessages;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.match.eobject.EObjectIndex.Side;
import org.eclipse.emf.compare.match.eobject.IEObjectMatcher;
import org.eclipse.emf.compare.match.eobject.ProximityEObjectMatcher.DistanceFunction;
import org.eclipse.emf.compare.match.eobject.ScopeQuery;
import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

public class TreeHashEObjectMatcher implements IEObjectMatcher, ScopeQuery {
	private ElementIndexAdapterFactory indexAdapterFactory = new ElementIndexAdapterFactory();
	
	private boolean greedyMatch = true;

	/**
	 * The index which keep the EObjects.
	 */
	private ByTypeIndexEx index;

	/**
	 * Keeps track of which side was the EObject from.
	 */
	private Map<EObject, Side> eObjectsToSide = Maps.newHashMap();

	/**
	 * Create the matcher using the given distance function.
	 * 
	 * @param meter
	 *            a function to measure the distance between two {@link EObject}s.
	 */
	public TreeHashEObjectMatcher(DistanceFunction meter) {
		this.index = new ByTypeIndexEx(meter, this);
	}
	
	private Set<EObject> buildIndex(Iterator<? extends EObject> objects, Side side) {
		// ElementIndexAdapter.reset();
		Set<EObject> roots = new LinkedHashSet<>();
		// roots
		while(objects.hasNext()) {
			EObject next = objects.next();
			doHash(next);
			index.index(next, side);
			eObjectsToSide.put(next, side);
			if(next.eContainer() == null) roots.add(next);
		}

		return roots;
	}
	
	protected ElementIndexAdapter doHash(EObject next) {
		ElementIndexAdapter adapter = this.indexAdapterFactory.equip(next);
		return adapter;
	}

	/**
	 * {@inheritDoc}
	 */

	public void createMatches(Comparison comparison, Iterator<? extends EObject> leftEObjects,
			Iterator<? extends EObject> rightEObjects, Iterator<? extends EObject> originEObjects,
			Monitor monitor) {
		if (!leftEObjects.hasNext() && !rightEObjects.hasNext() && !originEObjects.hasNext()) {
			return;
		}

		monitor.subTask(EMFCompareMessages.getString("ProximityEObjectMatcher.monitor.indexing")); //$NON-NLS-1$
		
		Set<EObject> leftRoots = buildIndex(leftEObjects, Side.LEFT);
		Set<EObject> rightRoots = buildIndex(rightEObjects, Side.RIGHT);
		Set<EObject> originRoots = buildIndex(originEObjects, Side.ORIGIN);

		monitor.subTask(EMFCompareMessages.getString("ProximityEObjectMatcher.monitor.matching")); //$NON-NLS-1$
		
		
//		matchIndexedObjects(comparison, monitor);
//
//		createUnmatchesForRemainingObjects(comparison, monitor);
//		restructureMatchModel(comparison, monitor);
		
		Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots = Triple.make(leftRoots, rightRoots, originRoots);

		matchIndexedObjects(comparison, roots, monitor);
		createUnmatchesForRemainingObjects(comparison, monitor);
		restructureMatchModel(comparison, monitor);
		
//		System.out.println(String.format("#elements found by eq: %d, #elements found by sim: %d, totalRemoved: %d", nElementsFoundByEq, nElementsFoundBySim, nElementsRemoved));

		ElementIndexAdapterFactory.removeAdapters(leftRoots);
		ElementIndexAdapterFactory.removeAdapters(rightRoots);
		ElementIndexAdapterFactory.removeAdapters(originRoots);
	}
	
	protected void matchIndexedObjects(Comparison comparison,
            Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots, 
            Monitor monitor) {
        matchList(comparison, roots.left, true, roots, monitor);
        matchList(comparison, roots.right, true, roots, monitor);
    }
	
	private void createUnmatchesForRemainingObjects(Comparison comparison, Monitor monitor) {
		for (EObject notFound : index.getValuesStillThere(Side.RIGHT)) {
			areMatching(comparison, null, notFound, null, null);
		}
		for (EObject notFound : index.getValuesStillThere(Side.LEFT)) {
			areMatching(comparison, notFound, null, null, null);
		}
		for (EObject notFound : index.getValuesStillThere(Side.ORIGIN)) {
			areMatching(comparison, null, null, notFound, null);
		}
	}
	
	protected Iterable<EObject> matchList(Comparison comparison, Iterable<EObject> todoList, boolean createUnmatches,
            Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots, Monitor monitor) {

        List<EObject> notFullyMatched = new ArrayList<>(32);
        List<EObject> nextLevel = new ArrayList<>(32);

        while(true) {
            nextLevel.clear();
            notFullyMatched.clear();
    
			// coarse match
			for (EObject eObj : todoList) {
				ElementIndexAdapter adapter = ElementIndexAdapterFactory.getAdapter(eObj);
				if (adapter != null) {					
					Match partialMatch = comparison.getMatch(eObj);
					if (!MatchUtil.isFullMatch(partialMatch)) {
						partialMatch = trySubtreeEqMatch(comparison, eObj, partialMatch, createUnmatches, roots);
						if (!MatchUtil.isFullMatch(partialMatch)) {
							notFullyMatched.add(eObj);
						}
					}
				}

				// This is a FIX for unsuccessful tree match
				nextLevel.addAll(eObj.eContents());
			}

			// structural iso match
			createSubtreeSimMatches(comparison, createUnmatches, roots, notFullyMatched);
  
			// fine match
            for(EObject eObj : notFullyMatched) {
                Match partialMatch = comparison.getMatch(eObj);
                tryFineMatch(comparison, eObj, partialMatch, createUnmatches);
            }
    
            if(nextLevel.isEmpty()) break;
            todoList = new ArrayList<>(nextLevel);
        }
        
        return Collections.emptyList();
    }


	/**
	 * Process all the matches of the given comparison and re-attach them to their parent if one is found.
	 * 
	 * @param comparison
	 *            the comparison to restructure.
	 * @param monitor
	 *            a monitor to track progress.
	 */
	private void restructureMatchModel(Comparison comparison, Monitor monitor) {
		Iterator<Match> it = ImmutableList.copyOf(Iterators.filter(comparison.eAllContents(), Match.class))
				.iterator();

		while (it.hasNext()) {
			if (monitor.isCanceled()) {
				throw new ComparisonCanceledException();
			}
			Match cur = it.next();
			EObject possibleContainer = null;
			if (cur.getLeft() != null) {
				possibleContainer = cur.getLeft().eContainer();
			}
			if (possibleContainer == null && cur.getRight() != null) {
				possibleContainer = cur.getRight().eContainer();
			}
			if (possibleContainer == null && cur.getOrigin() != null) {
				possibleContainer = cur.getOrigin().eContainer();
			}
			Match possibleContainerMatch = comparison.getMatch(possibleContainer);
			if (possibleContainerMatch != null) {
				((BasicEList<Match>)possibleContainerMatch.getSubmatches()).addUnique(cur);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isInScope(EObject eContainer) {
		return eObjectsToSide.get(eContainer) != null;
	}
	
	private Match trySubtreeEqMatch(Comparison comparison, EObject a, Match partialMatchOfA, boolean createUnmatches,
			Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots) {
		Side aSide = eObjectsToSide.get(a);
		assert aSide != null;
		Side bSide = Side.LEFT;
		Side cSide = Side.RIGHT;
		if (aSide == Side.RIGHT) {
			bSide = Side.LEFT;
			cSide = Side.ORIGIN;
		} else if (aSide == Side.LEFT) {
			bSide = Side.RIGHT;
			cSide = Side.ORIGIN;
		} else if (aSide == Side.ORIGIN) {
			bSide = Side.LEFT;
			cSide = Side.RIGHT;
		}
		assert aSide != bSide;
		assert bSide != cSide;
		assert cSide != aSide;

		// sub-tree match
		partialMatchOfA = doSubtreeEqMatch(comparison, partialMatchOfA, a, aSide, bSide, cSide, roots);
		// if(partialMatchOfA != null) {
		// counter.hit(a.eClass());
		// }
		assert MatchUtil.isValidPartialMatch(partialMatchOfA);
		return partialMatchOfA;
	}
	
//	long nElementsFoundByEq = 0;
//	long nElementsFoundBySim = 0;
//	long nElementsRemoved = 0;
	
	protected Match doSubtreeEqMatch(Comparison comparison, Match partialMatchOfA, EObject a, Side aSide, Side bSide,
			Side cSide, Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots) {
		// find subtrees in bSide and cSide
		// if bSubtree == null && cSubtree == null, then do nothing
		// if bSubtree != null && cSubtree == null, then Match.a = aSubtree, Match.b =
		// bSubtree, Match.c = PSEUDO
		// if bSubtree == null && cSubtree != null, then Match.a = aSubtree, Match.b =
		// PSEUDO, Match.c = cSubtree
		// if bSubtree != null && cSubtree != null, then Match.a = aSubtree, Match.b =
		// bSubtree, Match.c = cSubtree, remove all from index
		// if(configure.isUsingSubtreeHash()) {
		// when partialMatchOfA != null, we should match objects with partial matches
		// otherwise, we can match objects with partial matches
		// if findIdenticalSubtrees returns a match
		// if partialMatchOfA == null && result !=null
		// if result == partialMatchOfA, fill from aSide
		// if partialMatchOfA == null && result != partialMatchOfA, it is a new match or
		// a partial match from other sides
		// for a new match, createNewMatch
		// otherwise, fill from the side of result (either bSide or cSide)
		boolean unmatchedB = !MatchUtil.isMatched(partialMatchOfA, bSide);

		Match result = index.findIdenticalSubtrees(comparison, a, aSide, partialMatchOfA, roots);

		if (partialMatchOfA == null) {
			if (result == null) {
				// no subtree match is found
				return null;
			} else {
				if (result.eContainer() == null) {
					// this is a new match
//					nElementsFoundByEq += ElementIndexAdapterFactory.getAdapter(result.getLeft()).getTreeHash().size;
					createNewSubtreeMatches(comparison, result, roots);
				} else {
					// this is a match from other sides
					// fill a
					// fillSubtreeMatch(result, aSide)
					fillSubtreeMatches(comparison, result, aSide, roots);
				}
				return result;
			}
		} else {
			// in this case, result will not be null
			// we must check if the partial match is changed
			// if changed, fill b or c
			// fillSubtreeMatch(result, bSide) or fillSubtreeMatch(result, cSide)
			assert result != null;
			Side sideToFill = null;
			if (unmatchedB) {
				if (MatchUtil.isMatched(result, bSide))
					sideToFill = bSide;
			} else {
				if (MatchUtil.isMatched(result, cSide))
					sideToFill = cSide;
			}

			if (sideToFill != null) {
				assert MatchUtil.isFullMatch(result);
				fillSubtreeMatches(comparison, result, sideToFill, roots);
				return result;
			} else
				return result;
		}
	}
	
	protected void createNewSubtreeMatches(Comparison comparison, Match match,
			Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots) {

		if (roots.origin.isEmpty() && match.getOrigin() == MatchUtil.PSEUDO_MATCHED_OBJECT) {
			match.setOrigin(null); // finalize this match
		}

		((BasicEList<Match>) comparison.getMatches()).addUnique(match);

		// if match is a full match, remove objects
		if (MatchUtil.isFullMatch(match)) {
			index.remove(match.getLeft(), Side.LEFT);
			index.remove(match.getRight(), Side.RIGHT);
			if (match.getOrigin() != null) {
				index.remove(match.getOrigin(), Side.ORIGIN);
			}
//			nElementsRemoved ++;
		}

		List<EObject> lefts = getChildren(match.getLeft());
		List<EObject> rights = getChildren(match.getRight());
		List<EObject> origins = getChildren(match.getOrigin());

		boolean lrEqualSize = lefts.size() == rights.size();
		boolean loEqualSize = lefts.size() == origins.size();

		if (lrEqualSize && loEqualSize) {
			int size = lefts.size();
			for (int i = 0; i < size; i++) {
				EObject lc = lefts.get(i);
				EObject rc = rights.get(i);
				EObject oc = origins.get(i);

				boolean lrEqType = lc.eClass() == rc.eClass();
				boolean loEqType = lc.eClass() == oc.eClass();

				if (lrEqType && loEqType) {
					Match childMatch = MatchUtil.createMatch();
					childMatch.setLeft(lc);
					childMatch.setRight(rc);
					childMatch.setOrigin(oc);
					createNewSubtreeMatches(comparison, childMatch, roots);
				} else if (lrEqType) {
					Match childMatch = MatchUtil.createMatch();
					childMatch.setLeft(lc);
					childMatch.setRight(rc);
					childMatch.setOrigin(MatchUtil.PSEUDO_MATCHED_OBJECT);
					createNewSubtreeMatches(comparison, childMatch, roots);
				} else if (loEqType) {
					Match childMatch = MatchUtil.createMatch();
					childMatch.setLeft(lc);
					childMatch.setRight(MatchUtil.PSEUDO_MATCHED_OBJECT);
					childMatch.setOrigin(oc);
					createNewSubtreeMatches(comparison, childMatch, roots);
				} else if (rc.eClass() == oc.eClass()) {
					Match childMatch = MatchUtil.createMatch();
					childMatch.setLeft(MatchUtil.PSEUDO_MATCHED_OBJECT);
					childMatch.setRight(rc);
					childMatch.setOrigin(oc);
					createNewSubtreeMatches(comparison, childMatch, roots);
				}
			}
		} else if (lrEqualSize) {
			int size = lefts.size();
			for (int i = 0; i < size; i++) {
				EObject lc = lefts.get(i);
				EObject rc = rights.get(i);
				if (lc.eClass() == rc.eClass()) {
					Match childMatch = MatchUtil.createMatch();
					childMatch.setLeft(lc);
					childMatch.setRight(rc);
					childMatch.setOrigin(MatchUtil.PSEUDO_MATCHED_OBJECT);
					createNewSubtreeMatches(comparison, childMatch, roots);
				}
			}
		} else if (loEqualSize) {
			int size = lefts.size();
			for (int i = 0; i < size; i++) {
				EObject lc = lefts.get(i);
				EObject oc = origins.get(i);
				if (lc.eClass() == oc.eClass()) {
					Match childMatch = MatchUtil.createMatch();
					childMatch.setLeft(lc);
					childMatch.setRight(MatchUtil.PSEUDO_MATCHED_OBJECT);
					childMatch.setOrigin(oc);
					createNewSubtreeMatches(comparison, childMatch, roots);
				}
			}
		} else if (rights.size() == origins.size()) {
			int size = rights.size();
			for (int i = 0; i < size; i++) {
				EObject rc = rights.get(i);
				EObject oc = origins.get(i);
				if (rc.eClass() == oc.eClass()) {
					Match childMatch = MatchUtil.createMatch();
					childMatch.setLeft(MatchUtil.PSEUDO_MATCHED_OBJECT);
					childMatch.setRight(rc);
					childMatch.setOrigin(oc);
					createNewSubtreeMatches(comparison, childMatch, roots);
				}
			}
		}
	}
	
//	long notFullMatch = 0;
	protected void createNewSubtreeSimMatches(Comparison comparison, Match match,
			Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots) {
		if (roots.origin.isEmpty() && match.getOrigin() == MatchUtil.PSEUDO_MATCHED_OBJECT) {
			match.setOrigin(null); // finalize this match
		}

		((BasicEList<Match>) comparison.getMatches()).addUnique(match);

		// if match is a full match, remove objects
		if (MatchUtil.isFullMatch(match)) {
			index.remove(match.getLeft(), Side.LEFT);
			index.remove(match.getRight(), Side.RIGHT);
			if (match.getOrigin() != null) {
				index.remove(match.getOrigin(), Side.ORIGIN);
			}
//			nElementsRemoved ++;
		} else {
//			notFullMatch++;
		}
		
//		nElementsFoundBySim += 1;
		
		List<EObject> lefts = getChildren(match.getLeft());
		List<EObject> rights = getChildren(match.getRight());
		List<EObject> origins = getChildren(match.getOrigin());
		
		Map<EObject, EObject> matchesLR = createSimMatches(lefts, rights);
		Map<EObject, EObject> matchesLO = createSimMatches(lefts, origins);
		
		for(EObject l : lefts) {
			EObject r = matchesLR.get(l);
			EObject o = matchesLO.get(l);
			
			if(r==null && o==null) continue;
			
			Match childMatch = MatchUtil.createMatch();
			
			childMatch.setLeft(l);
			childMatch.setRight(r == null ? MatchUtil.PSEUDO_MATCHED_OBJECT : r);
			childMatch.setOrigin(o == null ? MatchUtil.PSEUDO_MATCHED_OBJECT : o);
			
			createNewSubtreeSimMatches(comparison, childMatch, roots);
		}
	}
	
	protected Map<EObject, EObject> createSimMatches(List<EObject> alist, List<EObject> blist) {
		if(alist.isEmpty() || blist.isEmpty()) return Collections.emptyMap();
		else {
			alist = getCopy(alist);
			blist = getCopy(blist);
			
			Multimap<Long, EObject> checksumMap = LinkedHashMultimap.create();
			blist.forEach(e->{
				ElementIndexAdapter adapter = ElementIndexAdapterFactory.getAdapter(e);
				if(adapter!=null) {
					checksumMap.put(adapter.getTreeHash().subtreeChecksum, e);
				}
			});
			
			Map<EObject, EObject> matches = new HashMap<EObject, EObject>();
			List<EObject> structuralCandidates = new ArrayList<>();
			
			Iterator<EObject> aItr = alist.iterator();
			while(aItr.hasNext()) {
				EObject e = aItr.next();
				ElementIndexAdapter adapter = ElementIndexAdapterFactory.getAdapter(e);
				if(adapter != null) {
					final TreeHashValue treeHashOfE = adapter.getTreeHash();
					final long checksum = treeHashOfE.subtreeChecksum;
					Collection<EObject> cand = checksumMap.get(checksum);
					if(cand==null || cand.isEmpty()) {
						if(greedyMatch) {
							structuralCandidates.clear();
							
							for(EObject b : blist) {
								ElementIndexAdapter bAdapter = ElementIndexAdapterFactory.getAdapter(b);
								if(bAdapter!=null) {
									TreeHashValue treeHashOfB = bAdapter.getTreeHash();
									if(treeHashOfB.subtreeStructureChecksum == treeHashOfE.subtreeStructureChecksum) {
										double sim = treeHashOfB.computeSubtreeSimilarity(treeHashOfE);
										if(sim > TREE_VERY_SIM_THREASHOLD) {
											structuralCandidates.add(b);
										}
									}
								}
							}
							
							if(structuralCandidates.size() == 1) {
								matches.put(e, structuralCandidates.get(0));
							}
						}
					} else {
						Iterator<EObject> candItr = cand.iterator();
						
						while(candItr.hasNext()) {
							EObject match = candItr.next();
							if(match.eClass() == e.eClass()) {							
								aItr.remove();
								candItr.remove();
								matches.put(e, match);
								break;
							}
						}
						
					}
				}
			}
			
			return matches;
		}
	}
	
	
	protected List<EObject> getCopy(List<EObject> list) {
		if(list.isEmpty()) return list;
		else return new ArrayList<>(list);
	}
	protected List<EObject> getUnmatchedChildren(Comparison comparison, List<EObject> children, Side sideToFill) {
		if(children.isEmpty()) return Collections.emptyList();
		else {
			return children.stream().filter(e->{
				Match m = comparison.getMatch(e);
				return !(MatchUtil.isMatched(m, sideToFill));
			}).toList();
		}
	}
	
	protected void fillSubtreeSimMatches(Comparison comparison, Match match, Side sideToFill,
			Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots) {

		if (MatchUtil.isFullMatch(match)) {
			index.remove(match.getLeft(), Side.LEFT);
			index.remove(match.getRight(), Side.RIGHT);
			if (match.getOrigin() != null) {
				index.remove(match.getOrigin(), Side.ORIGIN);
			}
		}

		List<EObject> lefts = getUnmatchedChildren(match.getLeft(), comparison, sideToFill);
		List<EObject> rights = getUnmatchedChildren(match.getRight(), comparison, sideToFill);
		List<EObject> origins = getUnmatchedChildren(match.getOrigin(), comparison, sideToFill);

		List<EObject> colA = null;
		List<EObject> colB = null;
		List<EObject> colToFill = null;
		
		switch (sideToFill) {
		case LEFT:
			colA = rights;
			colB = origins;
			colToFill = lefts;
			break;
		case RIGHT:
			colA = lefts;
			colB = origins;
			colToFill = rights;
			break;
		case ORIGIN:
			colA = lefts;
			colB = rights;
			colToFill = origins;
			break;
		}
		
		Map<EObject, EObject> matchesLR = createSimMatches(colToFill, colA);
		Map<EObject, EObject> matchesLO = createSimMatches(colToFill, colB);
		
		for(EObject e : colToFill) {
			EObject m = matchesLR.get(e);
			if(m!=null && e.eClass() == m.eClass()) {
				Match childMatch = comparison.getMatch(m);
				if (childMatch != null) {
					if (MatchUtil.tryFillMatched(childMatch, e, sideToFill)) {
						fillSubtreeSimMatches(comparison, childMatch, sideToFill, roots);
						continue;
					} else {
						System.err.println(childMatch);
					}
				} else {
					System.err.println("null child match [A]!");
				}
			} else {
				m = matchesLO.get(e);
				if(m!=null && e.eClass() == m.eClass()) {
					Match childMatch = comparison.getMatch(m);
					if (childMatch != null) {
						if (MatchUtil.tryFillMatched(childMatch, e, sideToFill)) {
							fillSubtreeSimMatches(comparison, childMatch, sideToFill, roots);
							continue;
						} else {
							System.err.println(childMatch);
						}
					} else {
						System.err.println("null child match [B]!");
					}
				}
			}
		}
	}
	
	private List<EObject> getUnmatchedChildren(EObject obj, Comparison c, Side side) {
		return getChildren(obj).stream().filter(e->!MatchUtil.hasMatchFor(e, c, side)).toList();
	}

	private List<EObject> getChildren(EObject obj) {
		if(obj == null) return Collections.emptyList();
		else return obj.eContents();
	}
	
	
	protected void fillSubtreeMatches(Comparison comparison, Match match, Side sideToFill,
			Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots) {

		if (MatchUtil.isFullMatch(match)) {
			index.remove(match.getLeft(), Side.LEFT);
			index.remove(match.getRight(), Side.RIGHT);
			if (match.getOrigin() != null) {
				index.remove(match.getOrigin(), Side.ORIGIN);
			}
		}

		List<EObject> lefts = getChildren(match.getLeft());
		List<EObject> rights = getChildren(match.getRight());
		List<EObject> origins = getChildren(match.getOrigin());

		List<EObject> colA = null;
		List<EObject> colB = null;
		List<EObject> colToFill = null;
		@SuppressWarnings("unused")
		Side sideA = null;
		Side sideB = null;

		switch (sideToFill) {
		case LEFT:
			colA = rights;
			colB = origins;
			colToFill = lefts;
			sideA = Side.RIGHT;
			sideB = Side.ORIGIN;
			break;
		case RIGHT:
			colA = lefts;
			colB = origins;
			colToFill = rights;
			sideA = Side.LEFT;
			sideB = Side.ORIGIN;
			break;
		case ORIGIN:
			colA = lefts;
			colB = rights;
			colToFill = origins;
			sideA = Side.LEFT;
			sideB = Side.RIGHT;
			break;
		}

		if (lefts.size() == rights.size() && lefts.size() == origins.size()) {
			int size = lefts.size();
			for (int i = 0; i < size; i++) {
				EObject ac = colA.get(i);
				EObject bc = colB.get(i);
				EObject fc = colToFill.get(i);

				if (ac.eClass() == fc.eClass()) {
					Match childMatch = comparison.getMatch(ac);
					if (childMatch != null) {
						if (MatchUtil.tryFillMatched(childMatch, fc, sideToFill)) {
							fillSubtreeMatches(comparison, childMatch, sideToFill, roots);
							continue;
						} else {
							System.err.println(childMatch);
						}

						if (MatchUtil.getMatchedObject(childMatch, sideB) == bc)
							continue;
					} else {
						System.err.println("null child match [A]!");
					}
				}

				if (bc.eClass() == fc.eClass()) {
					Match childMatch = comparison.getMatch(bc);
					if (childMatch != null) {
						if (MatchUtil.tryFillMatched(childMatch, fc, sideToFill)) {
							fillSubtreeMatches(comparison, childMatch, sideToFill, roots);
						} else {
							System.err.println(childMatch);
						}
					} else {
						System.err.println("null child match [B]!");
					}
				}
			}

		} else {
			// we stop here because we cannot ensure subtree equal
			// and this branch should rarely be triggered
			throw new RuntimeException(
					"An unexpected branch is triggered. We hope the partial match should be sub-tree-equal. Please check if this case is reasonable.");
		}

	}
	
	protected void createSubtreeSimMatches(Comparison comparison, boolean createUnmatches,
            Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots,
            List<EObject> notFullyMatched) {
        
        Map<EObject, ChangeTrackingMatch> results = trySubtreeSimMatches(comparison, notFullyMatched, createUnmatches, roots);
		
		Iterator<EObject> notMatched = notFullyMatched.iterator();
		while(notMatched.hasNext()) {
			EObject eObj = notMatched.next();
			ChangeTrackingMatch partialMatchPair = results.get(eObj);
			if(partialMatchPair != null && MatchUtil.isFullMatch(partialMatchPair.match)) {
				notMatched.remove();
			}
		}
    }
	
	class ChangeTrackingMatch {
		public Match match = null;
		public Side changedSide = null;

		public ChangeTrackingMatch(Match m) {
			this.match = m;
			this.changedSide = null;
		}

		public ChangeTrackingMatch(Match m, Side changedSide) {
			this.match = m;
			this.changedSide = changedSide;
		}
	}
	
	private Map<EObject, ChangeTrackingMatch> trySubtreeSimMatches(Comparison comparison, List<EObject> from, boolean createUnmatches,
			Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots) {
		if (from.isEmpty())
			return Collections.emptyMap();
		else {
			Side aSide = eObjectsToSide.get(from.get(0));
			assert aSide != null;
			Side bSide = Side.LEFT;
			Side cSide = Side.RIGHT;
			if (aSide == Side.RIGHT) {
				bSide = Side.LEFT;
				cSide = Side.ORIGIN;
			} else if (aSide == Side.LEFT) {
				bSide = Side.RIGHT;
				cSide = Side.ORIGIN;
			} else if (aSide == Side.ORIGIN) {
				bSide = Side.LEFT;
				cSide = Side.RIGHT;
			}
			assert aSide != bSide;
			assert bSide != cSide;
			assert cSide != aSide;

			Map<EObject, ChangeTrackingMatch> currentMatches = new HashMap<>();
			from.forEach(e->{
				Match m = comparison.getMatch(e);
				if(m != null) currentMatches.put(e, new ChangeTrackingMatch(m));
			});

			doSubtreeSimMatches(comparison, currentMatches, from, aSide, bSide, cSide, roots);

			return currentMatches;
		}
	}
	
	private void doSubtreeSimMatches(Comparison comparison, Map<EObject, ChangeTrackingMatch> currentMatches,
			List<EObject> from, Side aSide, Side bSide, Side cSide,
			Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots) {

		findUniqueSimSubtrees(comparison, from, aSide, bSide, cSide, currentMatches, roots);

		for (int i = 0; i < from.size(); i++) {
			EObject a = from.get(i);
			ChangeTrackingMatch result = currentMatches.get(a);

			if (result != null) {
				if (result.match.eContainer() == null) {
					createNewSubtreeSimMatches(comparison, result.match, roots);
				} else {
					if (result.changedSide != null)
						fillSubtreeSimMatches(comparison, result.match, result.changedSide, roots);
				}
			}
		}
	}
	
	public static final double TREE_SIM_THREASHOLD = 0.65;
	public static final double TREE_VERY_SIM_THREASHOLD = 0.9;
	
	class CandidateMatchGroup {
		List<CandidateMatch> bCandidates = new ArrayList<>();
		List<CandidateMatch> cCandidates = new ArrayList<>();

		private void add(EObject from, int iFrom, EObject cand, int iCand, List<CandidateMatch> candidates) {
			double sim = computePartialSimilarity(from, cand);
			if (sim == 0.0)
				return;
//			if (sim > 0.96)
//				sim = 1.0;

			CandidateMatch cm = new CandidateMatch();
			cm.from = from;
			cm.cand = cand;
			cm.treeSim = sim;
			cm.posDiff = Math.abs(iFrom - iCand);

			candidates.add(cm);
		}

		public void addCandidateB(EObject a, int ia, EObject b, int ib) {
			add(a, ia, b, ib, bCandidates);
		}

		public void addCandidateC(EObject a, int ia, EObject c, int ic) {
			add(a, ia, c, ic, cCandidates);
		}
		
		protected double computePartialSimilarity(EObject from, EObject cand) {
			ElementIndexAdapter adapter = ElementIndexAdapterFactory.getAdapter(from);
			if (adapter == null) return 0.0;
			if (cand.eClass() != from.eClass()) return 0.0;

			final TreeHashValue treeHash = adapter.getTreeHash();

			final int size = treeHash.size;
			final int height = treeHash.height;

			if (size < 2 && height < 2) return 0.0;

			ElementIndexAdapter cAdapter = ElementIndexAdapterFactory.getAdapter(cand);
			
			if (cAdapter != null) {
				final TreeHashValue childHash = cAdapter.getTreeHash();
//				if (size == childHash.size && height == childHash.height) 
				{
					final double sim = treeHash.computeSubtreeSimilarity(childHash);
					if (sim > TREE_SIM_THREASHOLD) {
						return sim;
					}
				}
			} 
			
			return 0.0;
		}
	}

	class CandidateMatch implements Comparable<CandidateMatch> {
		EObject from;
		EObject cand;
		double treeSim;
		int posDiff;

		@Override
		public int compareTo(CandidateMatch right) {
			if (treeSim == right.treeSim)
				return posDiff - right.posDiff;
			else {
				if (treeSim > right.treeSim)
					return -1;
				else
					return 1;
			}
		}
	}
	
	private void findUniqueSimSubtrees(Comparison comparison, List<EObject> from, Side passedObjectSide,
			Side bSide, Side cSide, Map<EObject, ChangeTrackingMatch> currentMatches,
			Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots) {

		EObject parent = null;

		// make candiate matches
		List<CandidateMatchGroup> groups = new ArrayList<>();

		CandidateMatchGroup group = null;
		List<EObject> bCandidates = null;
		List<EObject> cCandidates = null;

		int size = from.size();
		for (int i = 0; i < size; i++) {
			final EObject cur = from.get(i);
			final EObject curPar = cur.eContainer();

			final ChangeTrackingMatch matchOfCurPair = currentMatches.get(cur);
			final Match matchOfCur = matchOfCurPair == null ? null : matchOfCurPair.match;

			if (readyForThisTest(comparison, cur) == false)
				continue;

			if (parent != curPar || (curPar == null && parent == null)) {
				parent = curPar;

				bCandidates = getCandidatesWithFilter(comparison, parent, passedObjectSide, bSide, roots);
				cCandidates = getCandidatesWithFilter(comparison, parent, passedObjectSide, cSide, roots);

				if (bCandidates.isEmpty() && cCandidates.isEmpty()) {
					group = null;
				} else {
					// add group
					group = new CandidateMatchGroup();
					groups.add(group);
				}
			}

			if (group == null)
				continue;

			if (!MatchUtil.isMatched(matchOfCur, bSide)) {
				final int bCandSize = bCandidates.size();
				for (int j = 0; j < bCandSize; j++) {
					EObject b = bCandidates.get(j);
					group.addCandidateB(cur, i, b, j);
				}
			}

			if (!MatchUtil.isMatched(matchOfCur, cSide)) {
				final int cCandSize = cCandidates.size();
				for (int j = 0; j < cCandSize; j++) {
					EObject c = cCandidates.get(j);
					group.addCandidateC(cur, i, c, j);
				}
			}
		}

		// pick matches
		groups.forEach(g -> {
			pickMatches(comparison, passedObjectSide, bSide, cSide, currentMatches, g.bCandidates);
			pickMatches(comparison, passedObjectSide, cSide, bSide, currentMatches, g.cCandidates);
		});
	}
	
	protected boolean readyForThisTest(Comparison inProgress, EObject fastCheck) {
		EObject eContainer = fastCheck.eContainer();
		if (eContainer != null && isInScope(eContainer)) {
			Match match = MatchUtil.getMatch(eContainer, inProgress);
			return match != null;
		}
		return true;
	}
	
	private void pickMatches(Comparison comparison, Side passedObjectSide, Side bSide, Side cSide,
			Map<EObject, ChangeTrackingMatch> currentMatches, List<CandidateMatch> candidateMatches) {
		Collections.sort(candidateMatches);

		candidateMatches.forEach(cm -> {
			ChangeTrackingMatch aMatch = currentMatches.get(cm.from);
			ChangeTrackingMatch bMatch = currentMatches.get(cm.cand);

			if (hasMatchFor(aMatch, bSide) || hasMatchFor(bMatch, passedObjectSide))
				return;

			if (aMatch == null && bMatch == null) {
				// check if cm.cand has a match in comparison.
				// if so, reuse the match
				Match oldBMatch = comparison.getMatch(cm.cand);
				ChangeTrackingMatch ctm;
				if (oldBMatch == null) {
					Match newMatch = MatchUtil.createMatch();
					MatchUtil.setMatch(newMatch, cm.from, passedObjectSide);
					MatchUtil.setMatch(newMatch, cm.cand, bSide);
					ctm = new ChangeTrackingMatch(newMatch, passedObjectSide);
				} else {
					MatchUtil.setMatch(oldBMatch, cm.from, passedObjectSide);
					ctm = new ChangeTrackingMatch(oldBMatch, passedObjectSide);
				}
				currentMatches.put(cm.from, ctm);
				currentMatches.put(cm.cand, ctm);
			} else if (bMatch == null) {// aMatch != null
				Match oldBMatch = comparison.getMatch(cm.cand);
				if (oldBMatch != null) {
					return; // skip now, but we may further try to merge the two matches
				}

				if (aMatch.changedSide != null) {
					if (aMatch.changedSide != passedObjectSide)
						throw new RuntimeException();
				}
				aMatch.changedSide = bSide;
				MatchUtil.setMatch(aMatch.match, cm.cand, bSide);
				currentMatches.put(cm.cand, aMatch);
			} else if (aMatch == null) { // bMatch != null
				MatchUtil.setMatch(bMatch.match, cm.from, passedObjectSide);
				bMatch.changedSide = passedObjectSide;
				currentMatches.put(cm.from, bMatch);
			} else {
				throw new RuntimeException();
			}
		});
	}
	
	private boolean hasMatchFor(ChangeTrackingMatch ctm, Side sideToCheck) {
		if(ctm == null) return false;
		else return MatchUtil.isMatched(ctm.match, sideToCheck);
	}
	
	private List<EObject> getCandidatesWithFilter(Comparison comparison, EObject parent, Side aSide, Side sideToFind, Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots) {
		
		Collection<EObject> unfiltered = getCandidates(comparison, parent, sideToFind, roots);
		List<EObject> filtered = new ArrayList<>(unfiltered.size());
		
		for(EObject cand : unfiltered) {
			if(!MatchUtil.hasMatchFor(cand, comparison, aSide)) {
				filtered.add(cand);
			}
		}

		return filtered;
	}

	private Collection<EObject> getCandidates(Comparison comparison, EObject parent, Side sideToFind, Triple<Collection<EObject>, Collection<EObject>, Collection<EObject>> roots) {
		if(parent != null) {
			final Match containerMatch = MatchUtil.getMatch(parent, comparison);
			final EObject matchedContainer = MatchUtil.getMatchedObject(containerMatch, sideToFind);
	
			if (matchedContainer == null) {
				return Collections.emptyList();
			} else {
				return matchedContainer.eContents();
			}
		} else {
			switch(sideToFind) {
				case LEFT: return roots.left;
				case RIGHT: return roots.right;
				case ORIGIN: return roots.origin;
				default:
					return Collections.emptyList();
			}
		}
	}
	
	private boolean tryFineMatch(Comparison comparison, EObject a, Match partialMatchOfA, boolean createUnmatches) {
		Side aSide = eObjectsToSide.get(a);
		assert aSide != null;
		Side bSide = Side.LEFT;
		Side cSide = Side.RIGHT;
		if (aSide == Side.RIGHT) {
			bSide = Side.LEFT;
			cSide = Side.ORIGIN;
		} else if (aSide == Side.LEFT) {
			bSide = Side.RIGHT;
			cSide = Side.ORIGIN;
		} else if (aSide == Side.ORIGIN) {
			bSide = Side.LEFT;
			cSide = Side.RIGHT;
		}
		assert aSide != bSide;
		assert bSide != cSide;
		assert cSide != aSide;

		// sub-tree match
		return fineGainedMatch(comparison, partialMatchOfA, a, aSide, bSide, cSide, createUnmatches);
    }
	
	protected boolean fineGainedMatch(Comparison comparison, Match partialMatch, EObject a, Side aSide, Side bSide, Side cSide, boolean createUnmatches) {
		boolean okToMatch = false;
		Map<Side, EObject> closests = index.findClosests(comparison, a, aSide, partialMatch);
		if (closests != null) {
			EObject bObj = closests.get(bSide);
			EObject cObj = closests.get(cSide);
			if (bObj != null || cObj != null) {
				// we have at least one other match
				areMatching(comparison, closests.get(Side.LEFT), closests.get(Side.RIGHT), closests.get(Side.ORIGIN), partialMatch);
				okToMatch = true;
			} else if (createUnmatches) {
				areMatching(comparison, closests.get(Side.LEFT), closests.get(Side.RIGHT), closests.get(Side.ORIGIN), partialMatch);
				okToMatch = true;
			}
		}
		return okToMatch;
	}
	
	private Match areMatching(Comparison comparison, EObject left, EObject right, EObject origin, Match partialMatch) {
		Match result;
		if(partialMatch == null) {
			result = MatchUtil.createMatch();
			result.setLeft(left);
			result.setRight(right);
			result.setOrigin(origin);
			((BasicEList<Match>)comparison.getMatches()).addUnique(result);
		} else {
			result = partialMatch;
			if(result.getLeft() == MatchUtil.PSEUDO_MATCHED_OBJECT) result.setLeft(left);
			if(result.getRight() == MatchUtil.PSEUDO_MATCHED_OBJECT) result.setRight(right);
			if(result.getOrigin() == MatchUtil.PSEUDO_MATCHED_OBJECT) result.setOrigin(origin);
		}

		if (left != null) {
			index.remove(left, Side.LEFT);
		}
		if (right != null) {
			index.remove(right, Side.RIGHT);
		}
		if (origin != null) {
			index.remove(origin, Side.ORIGIN);
		}
		return result;
	}
}
