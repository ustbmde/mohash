package edu.ustb.sei.mde.mohash.emfcompare.parallel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.Map.Entry;
import java.util.function.BiFunction;

import org.eclipse.emf.compare.match.eobject.EObjectIndex.Side;
import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import edu.ustb.sei.mde.mohash.HashValue64;
import edu.ustb.sei.mde.mohash.ObjectIndex;
import edu.ustb.sei.mde.mohash.functions.Hash64;

public class ParallelHashIndex implements ObjectIndex {
	private java.util.concurrent.locks.ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	protected Map<EObject, HashValue64> obj2codeMap = new LinkedHashMap<>();
	protected Map<Long, Set<EObject>> code2objMap = new LinkedHashMap<>();
	
	// For debugging
	public int getPosition(long anchor, EObject selected, Iterable<EObject> candidates) {
		List<Double> ahead = new ArrayList<>();
		HashValue64 anchorHash = new HashValue64(anchor);
		HashValue64 selectedHash = obj2codeMap.get(selected);

		double baseSim = Hash64.cosineSimilarity(anchorHash, selectedHash);

		for (EObject cand : candidates) {
			HashValue64 candHash = obj2codeMap.get(cand);
			double candSim = Hash64.cosineSimilarity(anchorHash, candHash);
			if (baseSim <= candSim)
				ahead.add(candSim);
		}

		return ahead.size();
	}
	// End
	
	public Iterable<EObject> filterByContainer(Set<EObject> candidates, EObject container) {
		Set<EObject> children = new LinkedHashSet<>(container.eContents());
		
		if(candidates.size() <= children.size()) {
			return Sets.filter(new LinkedHashSet<>(candidates), e -> children.contains(e));
		} else {
			children.retainAll(candidates);
			return children;
		}
	}
	
	@Override
	public Iterable<EObject> query(EObject target, EObject containerMatch, long hashCode, double minSim, double containerDiff) {
		if(minSim==1 || hashCode==0) {
			Iterable<EObject> res = null;
			lock.readLock().lock();
			Set<EObject> o = code2objMap.getOrDefault(hashCode, Collections.emptySet());
			if(containerMatch!=null) res = filterByContainer(o, containerMatch);
			else res = new ArrayList<>(o);
			lock.readLock().unlock();
			return res;
		}
		
		HashValue64 hv = new HashValue64(hashCode);
		
		LinkedList<EObject> result = new LinkedList<>();
		lock.readLock().lock();
		for(Entry<EObject, HashValue64> entry : obj2codeMap.entrySet()) {
			HashValue64 value = entry.getValue();
			double containerSim = containerDiff;
			if(containerMatch==entry.getKey().eContainer()) {
				containerSim = 0;
			}
			double sim = value.code==hashCode ? 1.0 : ObjectIndex.similarity(value, hv);
			if(sim >= (minSim+containerSim)) { // Similarity propagation
				result.add(entry.getKey());
			}
		}
		lock.readLock().unlock();
		return result;
	}
	
	@Override
	public void index(EObject object, long hashCode) {
		HashValue64 hv = new HashValue64(hashCode);
		obj2codeMap.put(object, hv);
		code2objMap.computeIfAbsent(hashCode, ParallelHashIndex::orderedSetCreator).add(object);
	}
	
	static private Set<EObject> orderedSetCreator(Long key) {
		return new LinkedHashSet<>();
	}
	
	@Override
	public Long remove(EObject object) {
		Long ret = null;
		
		lock.writeLock().lock();
		HashValue64 hv = obj2codeMap.remove(object);
		if(hv!=null) {
			Set<EObject> set = code2objMap.get(hv.code);
			if(set!=null) {
				set.remove(object);
				if(set.isEmpty()) {
					code2objMap.remove(hv.code);
				}
			}
			ret = hv.code;
		}
		else ret = null;
		lock.writeLock().unlock();
		return ret;
	}
	
	@Override
	public Long getHash(EObject object) {
		lock.readLock().lock();
		HashValue64 hv = obj2codeMap.get(object);
		lock.readLock().unlock();
		if(hv!=null) return hv.code;
		else return null;
	}
	
	@Override
	public Iterable<EObject> getRemainingObjects() {
		Iterable<EObject> res =  new ArrayList<>(obj2codeMap.keySet());
		return res;
	}

	@Override
	public void printHashCodes(BiFunction<EObject, Long, String> function) {
		code2objMap.forEach((h,list)->{
			Object hashString = Hash64.toString(h)+":"+Long.bitCount(h);
			list.forEach(e->{				
				System.out.println(String.format("%s\t%s", hashString, function.apply(e, h)));
			});
		});
	}
	
	static class DiffPair {
		public DiffPair(double sim, EObject eObj) {
			super();
			this.diff = 1.0 - sim;
			this.eObj = eObj;
		}

		public double diff;
		public EObject eObj;
		
		public EObject getEObject() {
			return eObj;
		}
		
		static public int compare(DiffPair a, DiffPair b) {
			double delta = a.diff - b.diff;
			if(delta<0) return -1;
			else if(delta==0) return a.hashCode() - b.hashCode();
			else return 1;
		}
	}
	
	@Override
	public Iterable<EObject> getValuesToMatchAhead(Side side) {
		if(obj2codeMap.size() > SEARCH_WINDOW) {
			return getRemainingObjects();
		}
		return Collections.emptyList();
	}
}
