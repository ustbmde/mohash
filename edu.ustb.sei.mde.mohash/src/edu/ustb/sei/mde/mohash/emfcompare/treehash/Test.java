package edu.ustb.sei.mde.mohash.emfcompare.treehash;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class Test {

	public static void main(String[] args) {
		ResourceSet resourceSet = new ResourceSetImpl();
				// Register the default resource factory -- only needed for stand-alone!
		resourceSet.getPackageRegistry().put(EcorePackage.eINSTANCE.getNsURI(), EcorePackage.eINSTANCE);
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		
		Resource resource = resourceSet.getResource(URI.createFileURI("C:/JavaProjects/git/mohash/edu.ustb.sei.mde.mohash.evaluation/model/ecore/data_top_20/Ecore_12.ecore"), true);
		
		testResource(resource);
	}

	private static void testResource(Resource resource) {
		ElementIndexAdapterFactory factory = new ElementIndexAdapterFactory();
		
		List<EObject> list = new ArrayList<>();
		
		long start = System.currentTimeMillis();
		resource.getAllContents().forEachRemaining(e->{
			list.add(e);
			factory.equip(e);
		});
		
		resource.getContents().forEach(e->{
			ElementIndexAdapter adapter = ElementIndexAdapterFactory.getAdapter(e);
			adapter.getTreeHash();
		});
		long end = System.currentTimeMillis();
		System.out.println("Indexing time: "+(end-start));
		
		list.forEach(l->{
			ElementIndexAdapter la = ElementIndexAdapterFactory.getAdapter(l);
			TreeHashValue lh = la.getTreeHash();
			list.forEach(r->{
				if(l.eContainer() == r.eContainer()) {					
					ElementIndexAdapter ra = ElementIndexAdapterFactory.getAdapter(r);
					TreeHashValue rh = ra.getTreeHash();
					double computeSubtreeSimilarity = lh.computeSubtreeSimilarity(rh);
					
					if(computeSubtreeSimilarity > 0.65) {
						System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
						dump(l, lh);
						System.out.println("===============================");
						dump(r, rh);
						System.out.println("===============================");
						System.out.println("Local: "+TreeHashValue.jaccard(lh.localContentHash, lh.lchBits, rh.localContentHash, rh.lchBits));
						System.out.println("Child: "+TreeHashValue.jaccard(lh.childContentHash, lh.cchBits, rh.childContentHash, rh.cchBits));
						System.out.println("Total: "+computeSubtreeSimilarity);
						System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");					
					}
				}
				
				
			});
		});
	}

	private static void dump(EObject l, TreeHashValue lh) {
		System.out.println(l);
//		System.out.println("subtreeChecksum: " + lh.subtreeChecksum);
//		System.out.println("subtreeStructureChecksum: " + lh.subtreeStructureChecksum);
//		System.out.println("localContentHash: " + lh.localContentHash + ", " + Long.toBinaryString((lh.localContentHash))+" ("+lh.lchBits+","+Long.bitCount(lh.localContentHash)+")");
//		System.out.println("childContentHash: " + lh.childContentHash + ", " + Long.toBinaryString((lh.childContentHash))+" ("+lh.cchBits+","+Long.bitCount(lh.childContentHash)+")");
	}

}
