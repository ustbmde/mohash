package edu.ustb.sei.mde.mohash.emfcompare.parallel;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.match.eobject.EObjectIndex;
import org.eclipse.emf.compare.match.eobject.ProximityEObjectMatcher.DistanceFunction;
import org.eclipse.emf.compare.match.eobject.ScopeQuery;
import org.eclipse.emf.compare.match.eobject.WeightProvider.Descriptor.Registry;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mohash.ObjectIndex;
import edu.ustb.sei.mde.mohash.TypeMap;
import edu.ustb.sei.mde.mohash.emfcompare.HashBasedEObjectIndex;
import edu.ustb.sei.mde.mohash.emfcompare.ReasonableCachingDistance;

public class ParallelHashBasedEObjectIndex extends HashBasedEObjectIndex {
	protected ParallelReasonableCachingDistance meter;

//	public ParallelHashBasedEObjectIndex(ParallelReasonableCachingDistance meter, ScopeQuery matcher) {
//		super(null, matcher);
//		this.meter = meter;
//	}
//
//	public ParallelHashBasedEObjectIndex(ParallelReasonableCachingDistance meter, ScopeQuery matcher, Registry weightProviderRegistry) {
//		super(null, matcher, weightProviderRegistry);
//		this.meter = meter;
//	}
//
	public ParallelHashBasedEObjectIndex(ParallelReasonableCachingDistance meter, ScopeQuery matcher, Registry weightProviderRegistry,
			Double threshold, boolean extendedHasher) {
		super(null, matcher, weightProviderRegistry, threshold, extendedHasher);
		this.meter = meter;
	}

	public ParallelHashBasedEObjectIndex(ParallelReasonableCachingDistance meter, ScopeQuery matcher, Registry weightProviderRegistry,
			TypeMap<Double> threshold, boolean extendedHasher, Function<EClass, ObjectIndex> objectIndexBuilder) {
		super(null, matcher, weightProviderRegistry, threshold, extendedHasher, objectIndexBuilder);
		this.meter = meter;
	}
	
	@Override
	public Map<Side, EObject> findClosests(Comparison inProgress, EObject eObj, Side passedObjectSide) {
		throw new RuntimeException("");
	}
	
	// test container
	protected boolean readyForThisTestOrWaiting(Comparison inProgress, EObject eObj, EObject target, Scoreboard board) {
		if(!readyForThisTest(inProgress, eObj)) {
			Integer id = board.getObjectID(eObj.eContainer());
			if(id == null) return false;
			else {
				int requestID = board.getObjectID(target);
				board.waitUntilFormerIsFinished(id, requestID, "ready-for-test 2");
				return readyForThisTest(inProgress, eObj);
			}
		}
		return true;
	}
	
	public Map<Side, EObject> findClosests(int threadID, Comparison inProgress, EObject eObj, Side passedObjectSide, int objectID, Scheduler scheduler) {
		if (!readyForThisTestOrWaiting(inProgress, eObj, objectID, scheduler)) {
			return null;
		}
		
		Map<Side, EObject> result = new HashMap<EObjectIndex.Side, EObject>(3);
		result.put(passedObjectSide, eObj);
		if (passedObjectSide == Side.LEFT) {
			EObject closestRight = findTheClosest(threadID, inProgress, eObj, Side.LEFT, Side.RIGHT, objectID, scheduler.getBoardA());
			scheduler.getBoardA().lock(closestRight);
			EObject closestOrigin = findTheClosest(threadID, inProgress, eObj, Side.LEFT, Side.ORIGIN, objectID, scheduler.getBoardB());
			scheduler.getBoardB().lock(closestOrigin);
			result.put(Side.RIGHT, closestRight);
			result.put(Side.ORIGIN, closestOrigin);
		} else if (passedObjectSide == Side.RIGHT) {
			EObject closestLeft = findTheClosest(threadID, inProgress, eObj, Side.RIGHT, Side.LEFT, objectID, scheduler.getBoardA());
			scheduler.getBoardA().lock(closestLeft);
			EObject closestOrigin = findTheClosest(threadID, inProgress, eObj, Side.RIGHT, Side.ORIGIN, objectID, scheduler.getBoardB());
			scheduler.getBoardB().lock(closestOrigin);
			result.put(Side.LEFT, closestLeft);
			result.put(Side.ORIGIN, closestOrigin);

		} else if (passedObjectSide == Side.ORIGIN) {
			EObject closestLeft = findTheClosest(threadID, inProgress, eObj, Side.ORIGIN, Side.LEFT, objectID, scheduler.getBoardA());
			scheduler.getBoardA().lock(closestLeft);
			EObject closestRight = findTheClosest(threadID, inProgress, eObj, Side.ORIGIN, Side.RIGHT, objectID, scheduler.getBoardB());
			scheduler.getBoardB().lock(closestRight);
			result.put(Side.LEFT, closestLeft);
			result.put(Side.RIGHT, closestRight);
		}
		
		return result;
	}
	
	// test own container
	protected boolean readyForThisTestOrWaiting(Comparison inProgress, EObject eObj, int eObjID, Scheduler scheduler) {
		EObject eContainer = eObj.eContainer();
		if(!readyForThisTest(inProgress, eObj)) {
			Integer containerID = eContainer == null ? null : scheduler.getObjectID(eContainer);
			if(containerID==null) return false;
			else {
				scheduler.getBoardA().waitUntilFormerIsFinished(containerID, eObjID, "ready-for-test 1-1");
				scheduler.getBoardB().waitUntilFormerIsFinished(containerID, eObjID, "ready-for-test 1-2");
				return readyForThisTest(inProgress, eObj);
			}
		} else 
			return true;
	}
	
	static public void log(String msg) {
		String name = Thread.currentThread().getName();
		System.out.println("["+name+"]\t"+msg);
	}

	private EObject findTheClosest(int threadID, Comparison inProgress, final EObject eObj, final Side originalSide,
			final Side sideToFind, int objectID, Scoreboard board) {
		ObjectIndex originStorage = lefts;
		switch (originalSide) {
			case RIGHT:
				originStorage = rights;
				break;
			case LEFT:
				originStorage = lefts;
				break;
			case ORIGIN:
				originStorage = origins;
				break;

			default:
				break;
		}
		
		ObjectIndex storageToSearchFor = lefts;
		switch (sideToFind) {
			case RIGHT:
				storageToSearchFor = rights;
				break;
			case LEFT:
				storageToSearchFor = lefts;
				break;
			case ORIGIN:
				storageToSearchFor = origins;
				break;

			default:
				break;
		}
		
//		log("starting matching "+objectID);
		
		// find identical by hash code
		Long hash = originStorage.getHash(eObj);
		
		Iterable<EObject> cand = storageToSearchFor.query(eObj, null, hash, 1.0, 0.0);
		for(EObject fastCheck : cand) {
			if (!readyForThisTest(inProgress, fastCheck)) {
			} else {
				if (meter.areIdentic(threadID, inProgress, eObj, fastCheck)) {
					board.addToWaitingList(fastCheck, objectID);
//					log("id matching successfull for "+objectID);
					return fastCheck;
				}
			}
		}
		
//		log("searching candidates "+objectID);

		Match containerMatch = getPreviousMatch(eObj.eContainer(), inProgress);
		
		double minSim = getMinSim(eObj);
		double containerDiff = getContainerSimilarityRatio(eObj);
		
		EObject matchedContainer = null;
		if(containerMatch!=null) {			
			switch (sideToFind) {
			case RIGHT:
				matchedContainer = containerMatch.getRight();
				break;
			case LEFT:
				matchedContainer = containerMatch.getLeft();
				break;
			case ORIGIN:
				matchedContainer = containerMatch.getOrigin();
				break;
			default:
				break;
			}
		}
		Iterable<EObject> cand2 = storageToSearchFor.query(eObj, matchedContainer, hash, minSim, containerDiff);
		double bestDistance = Double.MAX_VALUE;
		
		SortedStackMap candidates = new SortedStackMap(board, objectID);
		for(EObject potentialClosest : cand2) {
			double dist = meter.distance(threadID, inProgress, eObj, potentialClosest);
			if(dist < bestDistance) {
				candidates.put(Double.valueOf(dist), potentialClosest);
			}
		}
		
		board.objectValidating(objectID);
		
//		log("searching candidates end "+objectID);
		
		for (EObject entry : candidates) {
			if(entry == null) break;
			EObject doubleCheck = findTheClosest(threadID, inProgress, entry, sideToFind, originalSide, eObj, board);
			if (doubleCheck == eObj) {
//				log("matching successful for "+objectID);
				return entry;
			}
		}
//		log("matching failed for "+objectID);
		return null;
	}
	
	protected boolean readyForThisTest(Comparison inProgress, EObject fastCheck) {
		EObject eContainer = fastCheck.eContainer();
		if (eContainer != null && scope.isInScope(eContainer)) {
			synchronized(inProgress) {
				return inProgress.getMatch(eContainer) != null;
			}
		}
		return true;
	}
	
	/**
	 * used for double check
	 * @param inProgress
	 * @param eObj
	 * @param originalSide
	 * @param sideToFind
	 * @param board
	 * @return
	 */
	private EObject findTheClosest(int threadID, Comparison inProgress, final EObject eObj, final Side originalSide,
			final Side sideToFind, final EObject target, Scoreboard board) {
		ObjectIndex originStorage = lefts;
		switch (originalSide) {
			case RIGHT:
				originStorage = rights;
				break;
			case LEFT:
				originStorage = lefts;
				break;
			case ORIGIN:
				originStorage = origins;
				break;

			default:
				break;
		}
		
		ObjectIndex storageToSearchFor = lefts;
		switch (sideToFind) {
			case RIGHT:
				storageToSearchFor = rights;
				break;
			case LEFT:
				storageToSearchFor = lefts;
				break;
			case ORIGIN:
				storageToSearchFor = origins;
				break;

			default:
				break;
		}
//		log("starting db-check matching");
		
		
		// find identical by hash code
		Long hash = originStorage.getHash(eObj);
		if(hash==null) return null;
		
		Iterable<EObject> cand = storageToSearchFor.query(eObj, null, hash, 1.0, 0.0);
		for(EObject fastCheck : cand) {
			if (!readyForThisTestOrWaiting(inProgress, fastCheck, target, board)) {
			} else {
				if (meter.areIdentic(threadID, inProgress, eObj, fastCheck)) {
//					log("finishing db-check");
					return fastCheck;
				}
			}
		}

		Match containerMatch = getPreviousMatch(eObj.eContainer(), inProgress);
		
		
		double minSim = getMinSim(eObj);
		double containerDiff = getContainerSimilarityRatio(eObj);
		
		boolean canCache = true;
		
		EObject matchedContainer = null;
		if(containerMatch!=null) {			
			switch (sideToFind) {
			case RIGHT:
				matchedContainer = containerMatch.getRight();
				break;
			case LEFT:
				matchedContainer = containerMatch.getLeft();
				break;
			case ORIGIN:
				matchedContainer = containerMatch.getOrigin();
				break;
			default:
				break;
			}
		} else {
			canCache = false;
		}
		Iterable<EObject> cand2 = storageToSearchFor.query(eObj, matchedContainer, hash, minSim, containerDiff);
		double bestDistance = Double.MAX_VALUE;
		EObject bestObject = null;
		
		for(EObject potentialClosest : cand2) {
//			board.waitUntilFormerIsFinished(potentialClosest, target, "db-check searching");
			double dist = meter.distance(threadID, inProgress, eObj, potentialClosest, canCache);
			if (dist < bestDistance) {
				bestDistance = dist;
				bestObject = potentialClosest;
			}
		}
		
//		log("finishing db-check");
		return bestObject;
	}

}
