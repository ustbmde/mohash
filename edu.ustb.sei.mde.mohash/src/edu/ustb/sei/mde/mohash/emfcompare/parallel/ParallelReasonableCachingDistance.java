package edu.ustb.sei.mde.mohash.emfcompare.parallel;

import java.util.Map;

import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.match.eobject.URIDistance;
import org.eclipse.emf.compare.match.eobject.ProximityEObjectMatcher.DistanceFunction;
import org.eclipse.emf.compare.match.eobject.WeightProvider.Descriptor.Registry;
import org.eclipse.emf.ecore.EObject;
import edu.ustb.sei.mde.mohash.functions.AccessBasedLRUCache;

public class ParallelReasonableCachingDistance implements DistanceFunction {

	/**
	 * The wrapped function.
	 */
	private DistanceFunction[] wrapped;

	/**
	 * The cache keeping the previous results.
	 */
	private Map<Pair, Double> distanceCache;

	/**
	 * Create a new caching distance.
	 * 
	 * @param wrapped
	 *            actual distance function to cache results from.
	 */
	public ParallelReasonableCachingDistance(int numberOfThreads, 
			Registry weightProviderRegistry,
			org.eclipse.emf.compare.match.eobject.EqualityHelperExtensionProvider.Descriptor.Registry equalityHelperExtensionProviderRegistry, 
			URIDistance sharedURIDistance) {
		this.wrapped = new DistanceFunction[numberOfThreads];
		for(int i=0; i<numberOfThreads; i++) this.wrapped[i] = new ParallelEditionDistance(weightProviderRegistry, equalityHelperExtensionProviderRegistry, sharedURIDistance);
		distanceCache = new AccessBasedLRUCache<Pair, Double>(10000, 1000, .75F);
	}

	/**
	 * {@inheritDoc}
	 */
	synchronized public double distance(int threadID, Comparison inProgress, EObject a, EObject b) {
		Pair key = new Pair(a, b);
		Double previousResult = distanceCache.get(key);
		if (previousResult == null) {			
			double dist = wrapped[threadID].distance(inProgress, a, b);
			distanceCache.put(key, Double.valueOf(dist));
			// cache it
			return dist;
		}
		return previousResult.doubleValue();
	}
	
	/**
	 * This method is used to replace {@link distance(Comparison inProgress, EObject a, EObject b)}.
	 * @param inProgress
	 * @param a
	 * @param b
	 * @param canCache
	 * @return
	 */
	public double distance(int threadID, Comparison inProgress, EObject a, EObject b, boolean canCache) {
		if(canCache) return distance(threadID, inProgress, a, b);
		else return wrapped[threadID].distance(inProgress, a, b);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean areIdentic(int threadID, Comparison inProgress, EObject a, EObject b) {
		return wrapped[threadID].areIdentic(inProgress, a, b);
	}

	/**
	 * A class used as a key for two EObjects. Pair(a,b) and Pair(b,a) should be equals and have the same
	 * hashcodes
	 */
	class Pair {
		// CHECKSTYLE:OFF
		EObject a;

		EObject b;

		public Pair(EObject a, EObject b) {
			super();
			this.a = a;
			this.b = b;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			int first = a.hashCode();
			int second = b.hashCode();
			if (first > second) {
				int tmp = first;
				first = second;
				second = tmp;
			}
			result = prime * result + first;
			result = prime * result + second;
			return result;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			Pair other = (Pair)obj;
			if (!getOuterType().equals(other.getOuterType())) {
				return false;
			}
			return (a == other.a && b == other.b) || (b == other.a && a == other.b);

		}

		private ParallelReasonableCachingDistance getOuterType() {
			return ParallelReasonableCachingDistance.this;
		}

	}
	// CHECKSTYLE:ON

	@Deprecated
	@Override
	public double distance(Comparison inProgress, EObject a, EObject b) {
		return 0;
	}

	@Deprecated
	@Override
	public boolean areIdentic(Comparison inProgress, EObject a, EObject b) {
		// TODO Auto-generated method stub
		return false;
	}

}
