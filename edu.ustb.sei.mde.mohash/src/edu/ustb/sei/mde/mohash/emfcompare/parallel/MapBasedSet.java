package edu.ustb.sei.mde.mohash.emfcompare.parallel;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MapBasedSet<V> implements Set<V> {
	private Map<V,Boolean> internalMap;
	
	public MapBasedSet(Map<V,Boolean> m) {
		internalMap = m;
	}

	@Override
	public int size() {
		return internalMap.size();
	}

	@Override
	public boolean isEmpty() {
		return internalMap.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return internalMap.containsKey(o);
	}

	@Override
	public Iterator<V> iterator() {
		return internalMap.keySet().iterator();
	}

	@Override
	public Object[] toArray() {
		return internalMap.keySet().toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return internalMap.keySet().toArray(a);
	}

	@Override
	public boolean add(V e) {
		return internalMap.put(e, Boolean.TRUE) == null;
	}

	@Override
	public boolean remove(Object o) {
		return internalMap.remove(o) != null;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return internalMap.keySet().containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends V> c) {
		for(V e : c) add(e);
		return true;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return internalMap.keySet().retainAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return internalMap.keySet().removeAll(c);
	}

	@Override
	public void clear() {
		internalMap.clear();
	}
}
