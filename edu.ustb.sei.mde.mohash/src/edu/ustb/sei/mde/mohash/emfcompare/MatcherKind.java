package edu.ustb.sei.mde.mohash.emfcompare;

public enum MatcherKind {
	simpleHash,
	dualHash,
	convHash,
	parallelHash
}
