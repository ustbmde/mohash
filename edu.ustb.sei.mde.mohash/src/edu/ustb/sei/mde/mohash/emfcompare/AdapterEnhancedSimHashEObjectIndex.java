package edu.ustb.sei.mde.mohash.emfcompare;

import org.eclipse.emf.compare.match.eobject.ProximityEObjectMatcher.DistanceFunction;
import org.eclipse.emf.compare.match.eobject.ScopeQuery;
import org.eclipse.emf.compare.match.eobject.WeightProvider.Descriptor.Registry;

import edu.ustb.sei.mde.mohash.AdapterEnhancedHashIndex;
import edu.ustb.sei.mde.mohash.ByTypeIndex;
import edu.ustb.sei.mde.mohash.TypeMap;

public class AdapterEnhancedSimHashEObjectIndex extends HashBasedEObjectIndex {

//	public AdapterEnhancedSimHashEObjectIndex(ReasonableCachingDistance meter, ScopeQuery matcher) {
//		super(meter, matcher);
//	}
//
//	public AdapterEnhancedSimHashEObjectIndex(ReasonableCachingDistance meter, ScopeQuery matcher,
//			Registry weightProviderRegistry) {
//		super(meter, matcher, weightProviderRegistry);
//	}
//
	public AdapterEnhancedSimHashEObjectIndex(ReasonableCachingDistance meter, ScopeQuery matcher,
			Registry weightProviderRegistry, Double threshold, boolean extendedHasher) {
		super(meter, matcher, weightProviderRegistry, threshold, extendedHasher);
	}
	
	public AdapterEnhancedSimHashEObjectIndex(ReasonableCachingDistance meter, ScopeQuery matcher,
			Registry weightProviderRegistry, TypeMap<Double> threshold, boolean extendedHasher) {
		super(meter, matcher, weightProviderRegistry, threshold, extendedHasher, null);
	}
	
	@Override
	protected void initIndex() {
		this.lefts = new ByTypeIndex(t->new AdapterEnhancedHashIndex());
		this.rights = new ByTypeIndex(t->new AdapterEnhancedHashIndex());
		this.origins = new ByTypeIndex(t->new AdapterEnhancedHashIndex());
	}

}
