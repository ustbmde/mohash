package edu.ustb.sei.mde.mohash.emfcompare.parallel;

import org.eclipse.emf.compare.match.eobject.URIDistance;
import org.eclipse.emf.ecore.EObject;

public class ParallelURIDistance extends URIDistance {

	public ParallelURIDistance() {
	}

	synchronized public Iterable<String> apply(EObject input) {
		return super.apply(input);
	}
	
	@Override
	synchronized public String retrieveFragment(EObject input) {
		return super.retrieveFragment(input);
	}
	
}
