package edu.ustb.sei.mde.mohash.emfcompare.treehash;

import java.util.zip.Checksum;

public class ChecksumUtil {
	static public void update(Checksum checksum, long value) {
        int l = (int) (value & 0xFFFFFFFFL);
        int h = (int) (value >>> 32);
        checksum.update(l);
        checksum.update(h);
    }

    static public void update(Checksum checksum, int value) {
        checksum.update(value);
    }

    static public void update(Checksum checksum, double value) {
        update(checksum, Double.doubleToLongBits(value));
    }
    
    static public void update(Checksum checksum, float value) {
        update(checksum, Float.floatToIntBits(value));
    }
    
    static public void update(Checksum checksum, boolean value) {
    	if(value) checksum.update("true".getBytes());
        else  checksum.update("false".getBytes());
    }

    static public void update(Checksum checksum, String str) {
        checksum.update(str.getBytes());
    }
    
    static private final byte[] byteCache = new byte[8];
    
    static public void update(Checksum checksum, byte value) {
        byteCache[0] = value;
        checksum.update(byteCache, 0, 1);
    }

    static public void update(Checksum checksum, char value) {
        byteCache[0] = (byte) (value & 0xFF);
        byteCache[1] = (byte) ((value >>> 8) & 0xFF);
        checksum.update(byteCache, 0, 2);
    }

    static public void update(Checksum checksum, short value) {
        byteCache[0] = (byte) (value & 0xFF);
        byteCache[1] = (byte) ((value >>> 8) & 0xFF);
        checksum.update(byteCache, 0, 2);
    }
    
    
}
