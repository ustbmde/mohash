package edu.ustb.sei.mde.mohash.factory;

import org.eclipse.emf.compare.match.DefaultComparisonFactory;
import org.eclipse.emf.compare.match.DefaultEqualityHelperFactory;
import org.eclipse.emf.compare.match.DefaultMatchEngine;
import org.eclipse.emf.compare.match.IComparisonFactory;
import org.eclipse.emf.compare.match.IMatchEngine;
import org.eclipse.emf.compare.match.IMatchEngine.Factory;
import org.eclipse.emf.compare.match.eobject.CachingDistance;
import org.eclipse.emf.compare.match.eobject.EditionDistance;
import org.eclipse.emf.compare.match.eobject.EqualityHelperExtensionProvider;
import org.eclipse.emf.compare.match.eobject.EqualityHelperExtensionProviderDescriptorRegistryImpl;
import org.eclipse.emf.compare.match.eobject.WeightProvider;
import org.eclipse.emf.compare.match.eobject.WeightProviderDescriptorRegistryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryRegistryImpl;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.compare.utils.UseIdentifiers;

import edu.ustb.sei.mde.mohash.EObjectSimHasher;
import edu.ustb.sei.mde.mohash.emfcompare.EditionDistanceEx;
import edu.ustb.sei.mde.mohash.emfcompare.IHashBasedMatcher;
import edu.ustb.sei.mde.mohash.emfcompare.treehash.TreeHashEObjectMatcher;

public class TreeHashMatchEngineFactory implements Factory {
	/** The match engine created by this factory. */
	protected IMatchEngine matchEngine;

	/** Ranking of this match engine. */
	private int ranking;

	/** A match engine needs a WeightProvider in case of this match engine do not use identifiers. */
	private WeightProvider.Descriptor.Registry weightProviderRegistry;

	public WeightProvider.Descriptor.Registry getWeightProviderRegistry() {
		return weightProviderRegistry;
	}

	/** A match engine may need a specific equality helper extension provider. */
	private EqualityHelperExtensionProvider.Descriptor.Registry equalityHelperExtensionProviderRegistry;
	
	static public final int numberOfThreads = 8;
	
	public TreeHashMatchEngineFactory() {
		this(WeightProviderDescriptorRegistryImpl.createStandaloneInstance(),
				EqualityHelperExtensionProviderDescriptorRegistryImpl.createStandaloneInstance());
	}
	
	public TreeHashMatchEngineFactory(WeightProvider.Descriptor.Registry weightProviderRegistry) {
		this(weightProviderRegistry,
				EqualityHelperExtensionProviderDescriptorRegistryImpl.createStandaloneInstance());
	}
	
	public TreeHashMatchEngineFactory(WeightProvider.Descriptor.Registry weightProviderRegistry,
			EqualityHelperExtensionProvider.Descriptor.Registry equalityHelperExtensionProviderRegistry) {
		this.weightProviderRegistry = weightProviderRegistry;
		this.equalityHelperExtensionProviderRegistry = equalityHelperExtensionProviderRegistry;
	}
	
//	private Set<EClass> ignoredClasses = Collections.emptySet();
	
//	public void setIgnoredClasses(Set<EClass> ignoredClasses) {
//		this.ignoredClasses = ignoredClasses;
//	}

	protected EditionDistance distance;
	protected TreeHashEObjectMatcher matcher ;

	public EditionDistance getDistance() {
		if(distance==null) {
			distance = createDistance();
		}
		return distance;
	}
	
	private EditionDistance createDistance() {
		return new EditionDistanceEx(weightProviderRegistry, equalityHelperExtensionProviderRegistry);
	}
	
	public EqualityHelperExtensionProvider.Descriptor.Registry getEqualityHelperExtensionProviderRegistry() {
		return equalityHelperExtensionProviderRegistry;
	}

	@Override
	public IMatchEngine getMatchEngine() {
		if (matchEngine == null) {
			final IComparisonFactory comparisonFactory = new DefaultComparisonFactory(
					new DefaultEqualityHelperFactory());
			CachingDistance cachedDistance = new CachingDistance(getDistance());
			
			matcher = new TreeHashEObjectMatcher(cachedDistance);
			
//			matcher.setIgnoredClasses(ignoredClasses);
			
			matchEngine = new DefaultMatchEngine(matcher, comparisonFactory);
		}
		return matchEngine;
	}
	
	public TreeHashEObjectMatcher getMatcher() {
		return matcher;
	}
	
	public void reset() {
		matchEngine = null;
	}
	
//	private double[] thresholds = null;
//
//	public double[] getThresholds() {
//		return thresholds;
//	}
//
//	public void setThresholds(double[] thresholds) {
//		this.thresholds = thresholds;
//	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.compare.match.IMatchEngine.Factory#getRanking()
	 */
	public int getRanking() {
		return ranking;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.compare.match.IMatchEngine.Factory#setRanking(int)
	 */
	public void setRanking(int r) {
		ranking = r;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.emf.compare.match.IMatchEngine.Factory#isMatchEngineFactoryFor(org.eclipse.emf.compare.scope.IComparisonScope)
	 */
	public boolean isMatchEngineFactoryFor(IComparisonScope scope) {
		return true;
	}

	/**
	 * The match engine needs a WeightProvider in case of this match engine do not use identifiers.
	 * 
	 * @param registry
	 *            the registry to associate with the match engine.
	 */
	void setWeightProviderRegistry(WeightProvider.Descriptor.Registry registry) {
		this.weightProviderRegistry = registry;
	}

	/**
	 * The match engine may need a Equality Helper Extension.
	 * 
	 * @param equalityHelperExtensionProviderRegistry
	 *            the registry to associate with the match engine.
	 */
	public void setEqualityHelperExtensionProviderRegistry(
			EqualityHelperExtensionProvider.Descriptor.Registry equalityHelperExtensionProviderRegistry) {
		this.equalityHelperExtensionProviderRegistry = equalityHelperExtensionProviderRegistry;
	}
	
	static public TreeHashMatchEngineFactory matchEngineFactory;
	
	
	static public IMatchEngine.Factory.Registry createFactoryRegistry( WeightProvider.Descriptor.Registry weightProviderRegistry) {
		IMatchEngine.Factory.Registry reg = MatchEngineFactoryRegistryImpl.createStandaloneInstance();
		
		if(weightProviderRegistry!=null) matchEngineFactory = new TreeHashMatchEngineFactory(weightProviderRegistry);
		else matchEngineFactory = new TreeHashMatchEngineFactory();
		
		matchEngineFactory.setRanking(20);
		reg.add(matchEngineFactory);
		return reg;
	}
	
	static public IMatchEngine.Factory.Registry createFactoryRegistry() {
		return createFactoryRegistry(null);
	}
	
	static public IMatchEngine.Factory.Registry createEMFCompareFactoryRegistry() {
		final IMatchEngine.Factory.Registry registry = new MatchEngineFactoryRegistryImpl();
		org.eclipse.emf.compare.match.eobject.WeightProvider.Descriptor.Registry weightInstance = WeightProviderDescriptorRegistryImpl.createStandaloneInstance();
		final MatchEngineFactoryImpl matchEngineFactory = new MatchEngineFactoryImpl(UseIdentifiers.NEVER, weightInstance);
		matchEngineFactory.setRanking(10);
		registry.add(matchEngineFactory);
		return registry;
	}
	
	static public IMatchEngine.Factory createEMFCompareFactory() {
		org.eclipse.emf.compare.match.eobject.WeightProvider.Descriptor.Registry weightInstance = WeightProviderDescriptorRegistryImpl.createStandaloneInstance();
		final MatchEngineFactoryImpl matchEngineFactory = new MatchEngineFactoryImpl(UseIdentifiers.NEVER, weightInstance);
		return matchEngineFactory;
	}
	
//	static public IMatchEngine.Factory createEMFCompareFactoryForProfiling() {
//		org.eclipse.emf.compare.match.eobject.WeightProvider.Descriptor.Registry weightInstance = WeightProviderDescriptorRegistryImpl.createStandaloneInstance();
//		final MatchEngineFactoryImpl matchEngineFactory = new MatchEngineFactoryImpl(UseIdentifiers.NEVER, weightInstance) {
//			@Override
//			public IMatchEngine getMatchEngine() {
//				if (matchEngine == null) {
//					final IComparisonFactory comparisonFactory = new DefaultComparisonFactory(new DefaultEqualityHelperFactory());
//					final IEObjectMatcher matcher;
//					final EditionDistance editionDistance = new EditionDistanceEx(weightInstance,
//							EqualityHelperExtensionProviderDescriptorRegistryImpl.createStandaloneInstance());
//					final CachingDistance cachedDistance = new CachingDistance(editionDistance);
//					matcher = new ProximityEObjectMatcher(cachedDistance);
//					matchEngine = new DefaultMatchEngine(matcher, comparisonFactory);
//				}
//				return matchEngine;
//			}
//		};
//		return matchEngineFactory;
//	}

}
