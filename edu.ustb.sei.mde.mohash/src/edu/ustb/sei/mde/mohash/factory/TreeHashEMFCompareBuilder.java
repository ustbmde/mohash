package edu.ustb.sei.mde.mohash.factory;

import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.match.eobject.WeightProvider;

public class TreeHashEMFCompareBuilder {
	static public EMFCompare build() {
		return build(null);
	}
	
	static public EMFCompare build(WeightProvider.Descriptor.Registry weightProviderRegistry) {
		return org.eclipse.emf.compare.EMFCompare.builder()
				.setMatchEngineFactoryRegistry(TreeHashMatchEngineFactory.createFactoryRegistry(weightProviderRegistry))
				.build();
	}
}
