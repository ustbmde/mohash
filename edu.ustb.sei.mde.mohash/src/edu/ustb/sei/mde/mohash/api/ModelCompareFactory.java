package edu.ustb.sei.mde.mohash.api;

import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.match.eobject.WeightProvider.Descriptor.Registry;

import edu.ustb.sei.mde.mohash.TypeMap;
import edu.ustb.sei.mde.mohash.emfcompare.MatcherKind;
import edu.ustb.sei.mde.mohash.factory.MoHashMatchEngineFactory;
import edu.ustb.sei.mde.mohash.interfaces.IImporter;

public class ModelCompareFactory {
	final private IImporter modelImporter;
	private MatcherKind matcherKind;
	
	public ModelCompareFactory(IImporter modelImporter, MatcherKind matcherKind) {
		this.modelImporter = modelImporter;
		this.matcherKind = matcherKind;
	}
	
	public ModelCompareFactory(IImporter modelImporter) {
		this(modelImporter, MatcherKind.simpleHash);
	}
	
	public void setMatcherKind(MatcherKind matcherKind) {
		this.matcherKind = matcherKind;
	}
	
	public EMFCompare createInstance() {
		Registry registry = modelImporter.getWeightRegistry();
		TypeMap<Double> thresholds = modelImporter.getThresholds();
		
		return org.eclipse.emf.compare.EMFCompare.builder()
				.setMatchEngineFactoryRegistry(MoHashMatchEngineFactory.createFactoryRegistry(
						this.matcherKind, 
						registry, 
						thresholds))
				// TODO: add diff engine factory here
				.build();
	}
}
