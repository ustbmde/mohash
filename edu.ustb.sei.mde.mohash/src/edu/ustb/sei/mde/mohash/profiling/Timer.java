package edu.ustb.sei.mde.mohash.profiling;

import java.util.HashMap;
import java.util.Map;

final public class Timer {
	
	final class TimerPair{
		public int count;
		public long start;
		
		public TimerPair(int count, long start) {
			this.count = count;
			this.start = start;
		}
	}
	
	private Map<Object, Long> totalTime = new HashMap<>();
	
	private Map<Object, TimerPair> startTime = new HashMap<>();
	
	public String toString() {
		return totalTime.toString();
	}
	
	public void tic(Object cat) {
		long now = System.nanoTime();
		startTime.compute(cat, (k,v)->{
			if(v==null) {
				return new TimerPair(1, now);
			} else {
				v.count ++;
				return v;
			}
		});
	}
	
	public void toc(Object cat) {
		long now = System.nanoTime();
		startTime.compute(cat, (k,v)->{
			if(v==null) {
				return null;
			} else {
				v.count --;
				if(v.count==0) {
					long span = now - v.start;
					long t = this.totalTime.getOrDefault(cat, 0L);
					this.totalTime.put(cat, t + span);
					return null;
				} else return v;
			}
		});
	}
	
	public void toggle(Object cat) {
		totalTime.compute(cat, (k,v)->{
			if(v==null) return 1L;
			else return v + 1;
		});
	}
}
