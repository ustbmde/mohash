package edu.ustb.sei.mde.mohash;

import org.eclipse.emf.ecore.EObject;

/**
 * Used with {@link HashAdapter}.
 * @author dr-he
 *
 */
public class AdapterEnhancedHashIndex extends HashIndex {

	public AdapterEnhancedHashIndex() {
		super();
	}
	
	/**
	 * {@inheritDoc}
	 * Note that if {@link object} is removed from the index, we still get the hash.
	 */
	@Override
	public Long getHash(EObject object) {
		return HashAdapter.getHash(object);
	}

}
