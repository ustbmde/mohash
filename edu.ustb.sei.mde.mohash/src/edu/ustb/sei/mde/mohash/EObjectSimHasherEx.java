package edu.ustb.sei.mde.mohash;

import java.util.List;
import java.util.zip.CRC32;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import edu.ustb.sei.mde.mohash.functions.Hash64;
import edu.ustb.sei.mde.mohash.functions.OnehotURIHash64;
import edu.ustb.sei.mde.mohash.functions.PrimitiveValueHash64;

public class EObjectSimHasherEx extends EObjectSimHasher {
	static public CRC32 crc32 = new CRC32();
	static public StringBuilder stringBuilder = new StringBuilder(4096);
	
	public static long getRecentDigest() {
		crc32.update(stringBuilder.toString().getBytes());
		return crc32.getValue();
	}
	
	public EObjectSimHasherEx() {
	}

	public EObjectSimHasherEx(EHasherTable table) {
		super(table);
	}

	@Override
	protected void beforeHash() {
		EObjectSimHasherEx.crc32.reset();
		EObjectSimHasherEx.stringBuilder.setLength(0);
	}
	
	@Override
	protected void onHash(EStructuralFeature feature, Hash64<?> hasher, Object value) {
		EObjectSimHasherEx.stringBuilder.append(feature.getName());
		EObjectSimHasherEx.stringBuilder.append(":");
		EObjectSimHasherEx.stringBuilder.append(value.toString());
	}
	
	final static void putValue(EStructuralFeature feature, Object value) {
		Class<?> type = value.getClass();
		
		if(type == Integer.class) stringBuilder.append(((Integer)value).intValue());
		else if(type == Boolean.class) stringBuilder.append(((Boolean)value).booleanValue());
		else if(type == String.class) stringBuilder.append(((String)value));
		else if(value instanceof EObject) {
			// cast without checking
			Iterable<String> uris = ((OnehotURIHash64)EHasherTable.globalURIHasher).getFragments((EObject) value);
			for(String s : uris) stringBuilder.append(s);
		}
		else if(value instanceof List) {
			stringBuilder.append("[");
			for(Object v : (List<?>) value) {
				putValue(feature, v);
			}
			stringBuilder.append("]");
		} else {
			stringBuilder.append(value.toString());
		}
	}
	
	@Override
	protected void endHash() {
	}
	
	public long pseudoHash(EObject data) {
		EClass clazz = data.eClass();
		beforeHash();
		Iterable<FeatureHasherTuple> pairs = getFeatureHasherTuples(clazz);
		for (FeatureHasherTuple pair : pairs) {
			Object value = data.eGet(pair.feature);
			if (value != null) {
				onHash(pair.feature, pair.hasher, value);
			}
		}
		endHash();
		return 0L;
	}
}
