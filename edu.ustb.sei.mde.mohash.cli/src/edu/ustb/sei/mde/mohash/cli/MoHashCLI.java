package edu.ustb.sei.mde.mohash.cli;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.ComparePackage;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;

import edu.ustb.sei.mde.mohash.api.ModelCompareFactory;
import edu.ustb.sei.mde.mohash.emfcompare.MatcherKind;
import edu.ustb.sei.mde.mohash.importer.ecore.EcoreImporter;
import edu.ustb.sei.mde.mohash.importer.json.JsonImporter;
import edu.ustb.sei.mde.mohash.importer.uml.UMLImporter;
import edu.ustb.sei.mde.mohash.interfaces.IImporter;

public class MoHashCLI {
	
	static Options options = new Options();
	static {
		options.addRequiredOption("mm", "metamodel", true, "The identifier of the metamodel");
		options.addRequiredOption("l", "left", true, "The path to the left model");
		options.addRequiredOption("r", "right", true, "The path to the right model");
		options.addOption("o", "origin", true, "the path to the origin model");
		options.addOption("out", "out", true, "the path to the comparison output");
	}
	

	public static void main(String[] args) {
		CommandLineParser parser = new DefaultParser();
		try {
			CommandLine cmd = parser.parse(options, args);
			
			ResourceSet resourceSet = new ResourceSetImpl();
			resourceSet.getPackageRegistry().put(ComparePackage.eNS_URI, ComparePackage.eINSTANCE);
			
			Comparison comparison = doCompare(resourceSet, cmd);
			
			saveResult(resourceSet, cmd, comparison);
			
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}
	}


	protected static void saveResult(ResourceSet resourceSet, CommandLine cmd, Comparison comparison)
			throws IOException {
		String outFile = cmd.getOptionValue("out");
		URI uri = null;
		
		if(outFile == null) {
			uri = URI.createURI("memory:/comparison.xmi");
			
		} else {
			uri = URI.createFileURI(outFile);
		}
		
		Resource resource = resourceSet.createResource(uri);
		resource.getContents().add(comparison);
		
		Map<String, Object> saveOptions = new HashMap<>();
		saveOptions.put(XMIResource.OPTION_FORMATTED, true);
		saveOptions.put(XMIResource.OPTION_ENCODING, "utf-8");
		
		if(outFile == null) {
			resource.save(System.out, saveOptions);
		} else {
			resource.save(saveOptions);
		}
	}


	protected static Comparison doCompare(ResourceSet resourceSet, CommandLine cmd) {
		String metamodel = cmd.getOptionValue("mm");
		String leftPath = cmd.getOptionValue("l");
		String rightPath = cmd.getOptionValue("r");
		String originPath = cmd.getOptionValue("o");
		
		IImporter modelImporter = getModelImporter(metamodel);
		modelImporter.initResourceSet(resourceSet);
		
		Resource left = modelImporter.loadFromFile(resourceSet, leftPath);
		Resource right = modelImporter.loadFromFile(resourceSet, rightPath);
		Resource origin = originPath == null ? null : modelImporter.loadFromFile(resourceSet, originPath);
		
		MatcherKind kind = MatcherKind.simpleHash;
		
		ModelCompareFactory factory = new ModelCompareFactory(modelImporter, kind);
		
		EMFCompare compare = factory.createInstance();
		IComparisonScope mohashScope = new DefaultComparisonScope(left, right, origin);
		
		Comparison comparison = compare.compare(mohashScope);
		return comparison;
	}


	protected static IImporter getModelImporter(String metamodel) throws UnsupportedOperationException {
		IImporter modelImporter = null;
		switch(metamodel) {
		case "ecore":
			modelImporter = new EcoreImporter();
			break;
		case "uml":
			modelImporter = new UMLImporter();
			break;
		case "json":
			modelImporter = new JsonImporter();
		default:
			throw new UnsupportedOperationException("Cannot load "+metamodel+" models");
		}
		return modelImporter;
	}

}
