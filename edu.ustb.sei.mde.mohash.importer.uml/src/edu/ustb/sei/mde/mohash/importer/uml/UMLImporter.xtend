package edu.ustb.sei.mde.mohash.importer.uml

import edu.ustb.sei.mde.mohash.interfaces.IImporter
import org.eclipse.emf.ecore.resource.ResourceSet
import edu.ustb.sei.mde.mohash.TypeMap
import org.eclipse.emf.ecore.EcorePackage
import org.eclipse.uml2.uml.UMLPackage
import org.eclipse.uml2.uml.resource.XMI2UMLResource
import org.eclipse.emf.compare.match.eobject.WeightProviderDescriptorRegistryImpl

class UMLImporter implements IImporter {
	
	override getThresholds() {
		val thresholds = new TypeMap<Double>(0.5)
		thresholds.put(EcorePackage.eINSTANCE.EPackage, 0.15)
		thresholds.put(EcorePackage.eINSTANCE.EClass, 0.55)
		thresholds.put(EcorePackage.eINSTANCE.EReference, 0.65)
		thresholds.put(EcorePackage.eINSTANCE.EOperation, 0.55)
		thresholds.put(EcorePackage.eINSTANCE.EAttribute, 0.68)
		thresholds.put(EcorePackage.eINSTANCE.EStringToStringMapEntry, 0.4)
		thresholds.put(EcorePackage.eINSTANCE.EEnum, 0.5)
		thresholds.put(EcorePackage.eINSTANCE.EEnumLiteral, 0.45)
		thresholds.put(EcorePackage.eINSTANCE.EParameter, 0.5)
		
		thresholds.put(UMLPackage.eINSTANCE.activity, 0.76);
		thresholds.put(UMLPackage.eINSTANCE.actor, 0.63);
		thresholds.put(UMLPackage.eINSTANCE.class_, 0.67);
		thresholds.put(UMLPackage.eINSTANCE.component, 0.77);
		thresholds.put(UMLPackage.eINSTANCE.association, 0.5);
		thresholds.put(UMLPackage.eINSTANCE.behaviorExecutionSpecification, 0.77);
		thresholds.put(UMLPackage.eINSTANCE.collaboration, 0.67);
		thresholds.put(UMLPackage.eINSTANCE.dataType, 0.61);
		thresholds.put(UMLPackage.eINSTANCE.dependency, 0.60);
		thresholds.put(UMLPackage.eINSTANCE.elementImport, 0.65);
		thresholds.put(UMLPackage.eINSTANCE.enumeration, 0.59);
		thresholds.put(UMLPackage.eINSTANCE.enumerationLiteral, 0.51);
		thresholds.put(UMLPackage.eINSTANCE.executionOccurrenceSpecification, 0.71);
		thresholds.put(UMLPackage.eINSTANCE.generalOrdering, 0.78);
		thresholds.put(UMLPackage.eINSTANCE.interaction, 0.74);
		thresholds.put(UMLPackage.eINSTANCE.instanceSpecification, 0.5);
		thresholds.put(UMLPackage.eINSTANCE.lifeline, 0.57);
		thresholds.put(UMLPackage.eINSTANCE.message, 0.67);
		thresholds.put(UMLPackage.eINSTANCE.messageOccurrenceSpecification, 0.73);
		thresholds.put(UMLPackage.eINSTANCE.model, 0.44);
		thresholds.put(UMLPackage.eINSTANCE.occurrenceSpecification, 0.72);
		thresholds.put(UMLPackage.eINSTANCE.operation, 0.66);
		thresholds.put(UMLPackage.eINSTANCE.package, 0.41);
		thresholds.put(UMLPackage.eINSTANCE.parameter, 0.67);
		thresholds.put(UMLPackage.eINSTANCE.property, 0.76);
		thresholds.put(UMLPackage.eINSTANCE.stateMachine, 0.55);
		thresholds.put(UMLPackage.eINSTANCE.useCase, 0.72);
		thresholds.put(UMLPackage.eINSTANCE.usage, 0.47);
		thresholds.put(UMLPackage.eINSTANCE.abstraction, 0.67);
		thresholds.put(UMLPackage.eINSTANCE.activityFinalNode, 0.73);
		thresholds.put(UMLPackage.eINSTANCE.decisionNode, 0.68);
		thresholds.put(UMLPackage.eINSTANCE.flowFinalNode, 0.68);
		thresholds.put(UMLPackage.eINSTANCE.forkNode, 0.67);
		thresholds.put(UMLPackage.eINSTANCE.joinNode, 0.72);
		thresholds.put(UMLPackage.eINSTANCE.include, 0.74);
		thresholds.put(UMLPackage.eINSTANCE.initialNode, 0.69);
		thresholds.put(UMLPackage.eINSTANCE.interface, 0.67);
		thresholds.put(UMLPackage.eINSTANCE.literalUnlimitedNatural, 0.50);
		thresholds.put(UMLPackage.eINSTANCE.literalInteger, 0.50);
		thresholds.put(UMLPackage.eINSTANCE.literalString, 0.20);
		
		thresholds
	}
	
	override initResourceSet(ResourceSet set) {
		set.packageRegistry.put(UMLPackage.eINSTANCE.nsURI, UMLPackage.eINSTANCE)
		set.getResourceFactoryRegistry().getExtensionToFactoryMap().put(XMI2UMLResource.FILE_EXTENSION, XMI2UMLResource.Factory.INSTANCE) 
	}
	
	override unhashableTypes() {
		#[EcorePackage.eINSTANCE.EAnnotation, EcorePackage.eINSTANCE.EGenericType, EcorePackage.eINSTANCE.EFactory, 
			EcorePackage.eINSTANCE.EStringToStringMapEntry,
			UMLPackage.eINSTANCE.packageImport, UMLPackage.eINSTANCE.literalBoolean, UMLPackage.eINSTANCE.literalInteger, UMLPackage.eINSTANCE.literalString]
	}
	
	override getWeightRegistry() {
		WeightProviderDescriptorRegistryImpl.createStandaloneInstance()
	}
	
}